<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
/** ADMIN Routes  */
Route::get('/admin-login','admin\AdminController@adminLogin');
Route::post('/verify','admin\AdminController@authorised');
Route::get('/adminlogout','admin\AdminController@logout');
Route::any('/forgot-password','admin\AdminController@forgotPassword');
Route::any('/reset-password','admin\AdminController@resetPassword');

Route::group(['middleware' => ['AdminMiddleware','role:ROLE_ADMIN']], function () {

Route::get('/dashboard','admin\AdminController@dashboard');
Route::any('/profile','admin\AdminController@profile');
Route::get('/user-draft-submission','admin\FormsubmissionController@draftSubmission');
Route::get('/user-draft-submission-detail/{id}','admin\FormsubmissionController@draftSubmissionDetail');
Route::get('/update-status/{id}/{id1}','admin\AdminController@updateStatus');

/** Draft Submission Fee*/
Route::any('/draft-amount','admin\AdminController@draftFee');
// Packages 
Route::get('/add-package','admin\PackagesController@addPackage');
Route::post('/package-submit-detail','admin\PackagesController@submitDetail');
Route::post('/package-submit-detail/{id}','admin\PackagesController@submitDetail');
Route::get('/packages-list','admin\PackagesController@viewPackages');
Route::get('/edit-package/{id}','admin\PackagesController@editPackage');
Route::post('/update-package/{id}','admin\PackagesController@updatePackage');
Route::get('/delete-package/{id}','admin\PackagesController@deleteData');
Route::get('/modify-status/{id}/{id1}','admin\PackagesController@modifyStatus');
// Add User by Admin
Route::get('add-user','admin\UsersController@addUser');
Route::post('/submit-detail','admin\UsersController@submitDetail');
Route::post('/submit-detail/{id}','admin\UsersController@submitDetail');
Route::get('/users-list','admin\UsersController@viewallUser');
Route::get('/edit-user/{id}','admin\UsersController@editUser');
Route::get('/delete-user/{id}','admin\UsersController@deleteData');

// Film makers 
Route::get('add-film-makers','admin\FilmMakersController@addUser');
Route::post('/filmmaker-submit-detail','admin\FilmMakersController@submitDetail');
Route::post('/filmmaker-submit-detail/{id}','admin\FilmMakersController@submitDetail');
Route::get('/film-makers-list','admin\FilmMakersController@viewallUser');
Route::get('/edit-film-maker/{id}','admin\FilmMakersController@editUser');
Route::get('/delete-user/{id}','admin\FilmMakersController@deleteData');

// Home page Banners 
Route::get('add-banner','admin\BannersController@addBanner');
Route::post('submit-banner-detail','admin\BannersController@submitDetail');
Route::post('submit-banner-detail/{id}','admin\BannersController@submitDetail');
Route::get('edit-banner-detail/{id}','admin\BannersController@editbanner');
Route::get('banners','admin\BannersController@viewBanners');
Route::get('delete-banner/{id}','admin\BannersController@deleteBanner');
Route::get('/admin-modify-banner-status/{id}/{id1}/{id2}','admin\BannersController@modifyStatus');


/**  CMS  */
Route::get('cms/add-page','admin\CMSController@addPage');
Route::post('submit-page-detail','admin\CMSController@submitDetail');
Route::post('submit-page-detail/{id}','admin\CMSController@submitDetail');
Route::get('cms/view-page','admin\CMSController@viewPage');
Route::get('cms/edit-page/{id}','admin\CMSController@EditPage');
Route::get('cms/delete-page/{id}','admin\CMSController@EditPage');
Route::get('/admin-modify-cms-status/{id}/{id1}/{id2}','admin\CMSController@modifyStatus');

// Blog Section//
Route::get('blog/add-blog','admin\BlogController@addBlog');
Route::post('submit-blog-detail','admin\BlogController@submitDetail');
Route::post('submit-blog-detail/{id}','admin\BlogController@submitDetail');
Route::get('blog/blog-list','admin\BlogController@viewBloglist');
Route::get('blog/edit-blog/{id}','admin\BlogController@Editblog');
Route::get('delete-blog/{id}','admin\BlogController@deleteBlog');
Route::get('/admin-modify-blog-status/{id}/{id1}/{id2}','admin\BlogController@modifyStatus');

/* Gallery Section  */
Route::get('add-gallery','admin\GalleryController@addGallery');
Route::post('save-images','admin\GalleryController@saveImage');
Route::get('gallery-list','admin\GalleryController@viewGallery');

//Categories 
Route::get('admin-add-categories','admin\CategoriesController@addCategories');
Route::post('/admin-submit-categories-detail','admin\CategoriesController@submitDetail');
Route::post('/admin-submit-categories-detail/{id}','admin\CategoriesController@submitDetail');
Route::get('/admin-categories-list','admin\CategoriesController@viewallCategories');
Route::get('/admin-edit-categories/{id}','admin\CategoriesController@editCategories');
Route::get('/admin-delete-categories/{id}','admin\CategoriesController@deleteData');
Route::get('/admin-modify-categories-status/{id}/{id1}/{id2}','admin\CategoriesController@modifyStatus');

//Events
Route::get('admin-manage-events','admin\EventController@addEvents');
Route::post('/admin-submit-events-detail','admin\EventController@submitDetail');
Route::post('/admin-submit-events-detail/{id}','admin\EventController@submitDetail');
Route::get('/admin-view-all-events-list','admin\EventController@viewallEvents');
Route::get('/admin-view-event/{id}','admin\EventController@viewEvents');
Route::get('/admin-edit-events/{id}','admin\EventController@editEvents');
Route::get('/admin-delete-events/{id}','admin\EventController@deleteData');
Route::get('/admin-modify-events-status/{id}/{id1}/{id2}','admin\EventController@modifyStatus');

//Filming Services
Route::get('admin-manage-filming-services-list','admin\FilmingServicesController@addFilmingServices');
Route::post('/admin-submit-filming-services-detail','admin\FilmingServicesController@submitDetail');
Route::post('/admin-submit-filming-services-detail/{id}','admin\FilmingServicesController@submitDetail');
Route::get('/admin-filming-services-list','admin\FilmingServicesController@viewallFilmingServices');
Route::get('/admin-edit-filming-services/{id}','admin\FilmingServicesController@editFilmingServices');
Route::get('/admin-delete-filming-services/{id}','admin\FilmingServicesController@deleteData');
Route::get('/admin-modify-filming-services-status/{id}/{id1}/{id2}','admin\FilmingServicesController@modifyStatus');

//Countries
Route::get('/admin-view-all-countries-list','admin\CountriesController@viewallCountries');
Route::get('/admin-delete-countries/{id}','admin\CountriesController@deleteData');
Route::get('/admin-modify-countries-status/{id}/{id1}/{id2}','admin\CountriesController@modifyStatus');


//FAQ
Route::get('admin-manage-faq','admin\FaqController@addFaq');
Route::post('/admin-submit-faq-detail','admin\FaqController@submitDetail');
Route::post('/admin-submit-faq-detail/{id}','admin\FaqController@submitDetail');
Route::get('/admin-view-all-faq-list','admin\FaqController@viewallFaq');
Route::get('/admin-view-event/{id}','admin\FaqController@viewFaq');
Route::get('/admin-edit-faq/{id}','admin\FaqController@editFaq');
Route::get('/admin-delete-faq/{id}','admin\FaqController@deleteData');
Route::get('/admin-modify-faq-status/{id}/{id1}/{id2}','admin\FaqController@modifyStatus');


Route::get('downloadExcel/{type}', 'admin\EventController@downloadExcel');

});

/**
 * FRONT END SECTION 
 * ALL ROUTES
 */
Route::get('/','front\HomesController@index');
Route::get('/submission-form','front\HomesController@draftSubmission');
Route::post('submit-draft-from-data','front\HomesController@submitDraftFormData');
Route::any('/paypal','PaymentController@payWithpaypal');
Route::get('status', 'PaymentController@getPaymentStatus');


Route::any('/event-price','front\HomesController@eventPrice');

//PAGES
Route::any('/faq','front\FaqController@viewFaq');
Route::get('/event-detail/{any}','front\HomesController@eventBySlug');
Route::get('/thankyou','front\HomesController@thankyou');
//NEWS
Route::get('/news','front\HomesController@allnews');
Route::any('/news-deatail/{any}','front\HomesController@newsDeatails');

//USERS
Route::get('/user-register','front\UserController@userRegister');
Route::get('/filmaker-megister','front\UserController@filmakerRegister');

Route::get('/user-login','front\UserController@userLogin');
Route::post('/user-verify','front\UserController@authorised');
Route::get('/user-logout','front\UserController@logout');
Route::any('/user-forgot-password','front\UserController@forgotPassword');
Route::any('/user-reset-password','front\UserController@resetPassword');

//STRIPE
Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripesubmit', 'StripePaymentController@stripePost');
//Route::post('stripesubmit', 'StripePaymentController@stripePost')->name('stripe.post');