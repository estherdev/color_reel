@extends('admin.mainlayout')
@section('title', 'Admin| users')
@section('content')
@section('heading','Users')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','users')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href= "{{url('add-user')}}" class = "btn btn-primary float-right mento" >Add User</a>
      <h5 class="card-title">Users List</h5>
     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered">
         <?php //echo "<pre>";print_r($packages);exit;?>
            <thead>
               <tr>
                  <th>Sr No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Package</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
            @forelse($users as $row)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$row['name']}}</td>
              <td>{{$row['email']}}</td>
              <td>{{$row['phone_number']}} </td>
             <td>{{$row['userpackage']['package_name']}}</td>
              <td><a href = "{{url('modify-status/user_info/'.$row['user_id'])}}">{{$row['status'] == 1 ? 'Active':'Inactive'}}</a></td>
              <td><a href = "{{url('edit-user/'.$row['user_id'])}}"><i class = "fa fa-edit"></i></a> | 
                  <a href = "{{url('delete-user/'.$row['user_id'])}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr><td colspan = "7" class = "text-center">No data found</td><tr>
            @endforelse 

            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection