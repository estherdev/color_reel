@extends('admin.mainlayout')
@section('title', 'Admin | add Blog')
@section('content')
@section('heading','Blog')  
@section('breadcrumb_menu','Blog')  
@section('breadcrumb_submenu','add-Blog')  
<div class="row">
   <div class="col-md-12">
      <div class="card">
      <?php //print_r($user)?>
         @if(@$blog !='')
         <form class="form-horizontal" method = "post" action = "{{url('/submit-blog-detail/'.$blog->blog_id)}}" enctype = "multipart/form-data">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/submit-blog-detail')}}" enctype = "multipart/form-data">
         @endif
         
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Blog Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title</label>
                  <div class="col-sm-9">
                     <input type="text" name = "title" class="form-control" id="fname" placeholder="Enter Blog Title" required value = "{{old('title') != '' ? old('title') : @$blog->title }}">
                     <span style = "color:red">{{ $errors->first('title') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Description</label>
                  <div class="col-sm-9">
                  <textarea class = "form-control" id="editor1" name = "description" placeholder = "Enter Blog Description" required>{{old('description') != '' ? old('description') : @$blog->description }}</textarea>
                  <span style = "color:red">{{ $errors->first('description') }}</span> 
                  </div>
               </div>
               
               <div class="form-group row">
                  <label class="col-md-3 text-right control-label col-form-label">Image</label>
                  <div class="col-md-9">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name = "image" id="validatedCustomFile">
                           <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                           <span style = "color:red">{{ $errors->first('image') }}</span>
                        </div>
                  </div>
               </div>
               
              
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
      
      
   </div>
</div>
@endsection