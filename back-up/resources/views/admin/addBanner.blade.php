@extends('admin.mainlayout')
@section('title', 'Admin | add Banner')
@section('content')
@section('heading','Banner')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','add-banner')  
<div class="row">
   <div class="col-md-12">
      <div class="card">
      <?php //print_r($user)?>
         @if(@$banner !='')
         <form class="form-horizontal" method = "post" action = "{{url('/submit-banner-detail/'.$banner->banner_id)}}" enctype = "multipart/form-data">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/submit-banner-detail')}}" enctype = "multipart/form-data">
         @endif
         
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Heading</label>
                  <div class="col-sm-9">
                     <input type="text" name = "heading" class="form-control" id="fname" placeholder="Enter Display Heading" required value = "{{old('heading') != '' ? old('heading') : @$banner->heading }}">
                     <span style = "color:red">{{ $errors->first('name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Sub Heading</label>
                  <div class="col-sm-9">
                     <input type="text" name  = "sub_heading" class="form-control" id="" placeholder="Enter Sub Heading"  value = "{{old('sub_heading') != '' ? old('sub_heading') : @$banner->sub_heading }}">
                     
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">URL</label>
                  <div class="col-sm-9">
                     <input type="text" name  = "url" class="form-control" id="" placeholder="Enter Page URL"  value = "{{old('url') != '' ? old('url') : @$banner->url}}">
                     
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-md-3 text-right control-label col-form-label">Banner Image</label>
                  <div class="col-md-9">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name = "banner_image" id="validatedCustomFile">
                           <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                           <span style = "color:red">{{ $errors->first('banner_image') }}</span>
                        </div>
                  </div>
               </div>
               
              
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
      
      
   </div>
</div>
@endsection