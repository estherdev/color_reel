<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('adminAssets/assets/images/favicon.png') }}">
    <title>@yield('title')</title>
    <!-- Custom CSS -->
    <link href="{{ asset('adminAssets/assets/libs/flot/css/float-chart.css') }}" rel="stylesheet">
    <link href="{{ asset('adminAssets/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('adminAssets/assets/libs/magnific-popup/dist/magnific-popup.css') }}" rel="stylesheet">
    <link href="{{ asset('adminAssets/dist/css/style.min.css') }}" rel="stylesheet">
    <script src="//cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
    <style>
   .logo-text img.light-logo {max-height: 60px;width: 117;object-fit: cover;}
   .mento { margin-bottom: 20px;}
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar" data-navbarbg="skin5">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- Logo -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon -->
                        <b class="logo-icon p-l-10">
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="{{ asset('adminAssets/assets/images/logo-icon.png') }}" alt="homepage" class="light-logo" /> -->
                           
                        </b>
                        <!--End Logo icon -->
                         <!-- Logo text -->
                        
                        <span class="logo-text">
                        <?php //print_r(sitelogo(1)->sitelogo);?>
                             <!-- dark Logo text -->
                             <img src="{{ asset('adminAssets/assets/images/'.sitelogo(session('admin'))->sitelogo) }}" alt="homepage" class="light-logo" />
                            
                        </span>
                        <!-- Logo icon -->
                        <!-- <b class="logo-icon"> -->
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <!-- <img src="../../assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->
                            
                        <!-- </b> -->
                        <!--End Logo icon -->
                    </a>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Toggle which is visible on mobile only -->
                    <!-- ============================================================== -->
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                       
                    </ul>
                    <!-- ============================================================== -->
                    <!-- Right side toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav float-right">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-bell font-24"></i>
                            </a>
                             <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="font-24 mdi mdi-comment-processing"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="2">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                             <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-success btn-circle"><i class="ti-calendar"></i></span>
                                                    <div class="m-l-10">
                                                        <h5 class="m-b-0">Event today</h5> 
                                                        <span class="mail-desc">Just a reminder that event</span> 
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-info btn-circle"><i class="ti-settings"></i></span>
                                                    <div class="m-l-10">
                                                        <h5 class="m-b-0">Settings</h5> 
                                                        <span class="mail-desc">You can customize this template</span> 
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-primary btn-circle"><i class="ti-user"></i></span>
                                                    <div class="m-l-10">
                                                        <h5 class="m-b-0">Pavan kumar</h5> 
                                                        <span class="mail-desc">Just see the my admin!</span> 
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)" class="link border-top">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i class="fa fa-link"></i></span>
                                                    <div class="m-l-10">
                                                        <h5 class="m-b-0">Luanch Admin</h5> 
                                                        <span class="mail-desc">Just see the my new admin!</span> 
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->

                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{  asset('adminAssets/assets/images/users/1.jpg') }}" alt="user" class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <a class="dropdown-item" href="{{url('/profile')}}"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                               
                                <div class="dropdown-divider"></div>
                               <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#Modal2"><i class="ti-settings m-r-5 m-l-5"></i> Draft Form Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{url('/adminlogout')}}"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                <div class="dropdown-divider"></div>
                                
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
          <div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Draft Submission Price</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method = "post" action = "{{url('/draft-amount')}}">
                           {{csrf_field()}}
                            <label>Enter Draft Submission Fee</label>
                            <input type = "number" min = 0 class = "form-control" name = "draft_amount" Placeholder = "Enter Draft Submission Fee" required value = "{{draftprice()->draft_amount}}"><br>
                            <input type = "submit" value = "submit" name = "submit" class = "btn btn-primary">

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item <?php if(Request::segment(1) == 'dashboard') echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'dashboard') echo "active"?>" href="{{URL::to('dashboard')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item <?php if((Request::segment(1) == 'user-draft-submission') || (Request::segment(1) == 'user-draft-submission-detail')) echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'user-draft-submission') echo "active"?>" href="{{URL::to('user-draft-submission')}}" aria-expanded="false"><i class="mdi mdi-film"></i><span class="hide-menu">Draft Submission</span></a></li>
                        <li class="sidebar-item <?php if(Request::segment(1) == 'packages-list') echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'packages-list') echo "active"?>" href="{{URL::to('packages-list')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Package</span></a></li>
                        <li class="sidebar-item <?php if(Request::segment(1) == 'users-list') echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'users-list') echo "active"?>" href="{{URL::to('users-list')}}" aria-expanded="false"><i class="mdi mdi-account-multiple-plus"></i><span class="hide-menu">Users</span></a></li>
                        <li class="sidebar-item <?php if(Request::segment(1) == 'film-makers-list') echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'film-makers-list') echo "active"?>" href="{{URL::to('film-makers-list')}}" aria-expanded="false"><i class="mdi mdi-film"></i><span class="hide-menu">Film Makers</span></a></li>
                        <li class="sidebar-item <?php if(Request::segment(1) == 'banners') echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'banners') echo "active"?>" href="{{URL::to('banners')}}" aria-expanded="false"><i class="mdi mdi-file-image"></i><span class="hide-menu">Home Page Banners</span></a></li>
                        <li class="sidebar-item <?php if(Request::segment(1) == 'cms/view-page') echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'cms/view-page') echo "active"?>" href="{{URL::to('cms/view-page')}}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">CMS</span></a></li>
                         <li class="sidebar-item <?php if(Request::segment(1) == 'blog/blog-list') echo "selected"?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if(Request::segment(1) == 'blog/blog-list') echo "active"?>" href="{{URL::to('blog/blog-list')}}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Blog</span></a></li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
             <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">@yield('heading')</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">@yield('breadcrumb_menu')</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">@yield('breadcrumb_submenu')</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
    
              <!-- ############## Main content ###############-->
           
				@yield('content')
                 @show                     
    
    <!-- #################/End content ######################-->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Color Reel.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{  asset('adminAssets/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{  asset('adminAssets/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{  asset('adminAssets/dist/js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{  asset('adminAssets/dist/js/sidebarmenu.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{  asset('adminAssets/dist/js/custom.min.js') }}"></script>
    <!--This page JavaScript -->
    <!-- <script src="../../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="{{  asset('adminAssets/assets/libs/flot/excanvas.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/flot/jquery.flot.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/flot/jquery.flot.time.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/flot/jquery.flot.stack.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/flot/jquery.flot.crosshair.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{  asset('adminAssets/dist/js/pages/chart/chart-page-init.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{  asset('adminAssets/assets/libs/magnific-popup/meg.init.js') }}"></script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#zero_config').DataTable();
    </script>
    <script>
    setTimeout(function(){ $('.alert').fadeOut(); }, 1000);
     CKEDITOR.replace( 'editor1' );
    </script>
    <script>
$('#uploadtype').change(function(){
   var uploadtype =  $( 'input[name=file_type]:checked' ).val()
   if(uploadtype == 'image'){
       $('#imagetag').show();
       $('#videotag').hide();
       $('#urltag').hide();
       $('#customvideo').hide();
       $('#validatedCustomFile').attr('required','required');
       $('#validatedCustomvideoFile').removeAttr('required');
       $('#validatedCustomvideoFile').val('');
       $('#urls').val('');
       
   }else{
       $('#imagetag').hide();
       $('#videotag').show();
       $('#blah').hide();
       $('#blah').attr('src','');
       $('#validatedCustomFile').removeAttr('required');
       $('#validatedCustomFile').val('');
      
   }
   
});
$('#browsetype').change(function(){
       var browsetype =  $( 'input[name=upload_type]:checked' ).val()
        if(browsetype == 'url'){
            $('#customvideo').hide();
            $('#urltag').show();
            $('#urls').attr('required','required');
            $('#validatedCustomvideoFile').removeAttr('required');
            $('#validatedCustomvideoFile').val('');
        }
        else{
            $('#customvideo').show();
            $('#urltag').hide();
            $('#urls').removeAttr('required');
            $('#urls').val('');
            $('#validatedCustomvideoFile').attr('required','required');
        }
        });
        
</script>
<!-- Display image before upload  -->
<script>
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
        $('#blah').show();
      $('#blah').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]);
  }
}

$("#validatedCustomFile").change(function() {
  readURL(this);
});

</script>
<!-- AJAX iMAGE UPLOAD -->
<script type="text/javascript">
var APP_URL = {!! json_encode(url('/')) !!}
</script>

<script>
$(document).ready(function(){

 $('#upload_form').on('submit', function(event){
  event.preventDefault();
  $('.preloader').css('display','show');
  $.ajax({
   url: APP_URL+"/save-images",
   method:"POST",
   data:new FormData(this),
   dataType:'JSON',
   contentType: false,
   cache: false,
   processData: false,
   success:function(data)
   {
       //console.log(data);
       $('.preloader').css('display','none');
    if(data.status = 200){
        $("#upload_form")[0].reset();
    }
    $('#message').css('display', 'block');
    $('#message').html(data.message);
    $('#message').addClass(data.class_name);
    $('#uploaded_image').html(data.uploaded_image);
   }
  })
 });

});
</script>
</body>

</html>