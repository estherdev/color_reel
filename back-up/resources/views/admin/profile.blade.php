@extends('admin.mainlayout')
@section('title', 'Admin Dashboard')
@section('content')
@section('heading','Profile')  
@section('breadcrumb_menu','profile')  
@section('breadcrumb_submenu','setting') 
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
      <div class="card">
         <form class="form-horizontal" method = "post" action = "" enctype = "multipart/form-data">
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Personal Info</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="fname" placeholder="First Name Here"  value = "{{$detail['name']}}" name="name">
                     <span style = "color:red">{{ $errors->first('name') }}</span>
                  </div>

               </div>
               
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Password</label>
                  <div class="col-sm-9">
                     <input type="password" class="form-control" id="password" placeholder="Leave blank, if not need to change" name="password">
                     <span style = "color:red">{{ $errors->first('password') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="email1" class="col-sm-3 text-right control-label col-form-label">Email</label>
                  <div class="col-sm-9">
                     <input type="text" class="form-control" id="email1" placeholder="Company Name Here" value = "{{$detail['email']}}" name="email">
                     <span style = "color:red">{{ $errors->first('email') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-md-3 text-right control-label col-form-label">Site Logo</label>
                  <div class="col-md-9">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name = "sitelogo" id="validatedCustomFile">
                           <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                        
                        </div>
                  </div>
               </div>
              <!--  <div class="form-group row">
                  <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Message</label>
                  <div class="col-sm-9">
                     <textarea class="form-control"></textarea>
                  </div>
               </div> -->
            </div>
            <div class="border-top">
               <div class="card-body">
                  <input type="submit" class="btn btn-primary" name = "submit" value = "Submit">
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- /.col -->
@endsection