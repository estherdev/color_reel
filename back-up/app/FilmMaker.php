<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmMaker extends Model
{
    //
    public $table = "submitter_info"; protected $primaryKey = 'user_id';
    protected $fillable = ['name','email','phone_number','user_id'];
}
