<?php 
if (! function_exists('sitelogo')) {
  function sitelogo($user_id = null) {
    $data = \DB::table('users')
        ->select('*')
        ->where('id', $user_id)
        ->first();

    return $data;
  }
}

if (! function_exists('draftprice')) {
  function draftprice() {
    $data = \DB::table('site_setting')
        ->select('*')
        ->first();

    return $data;
  }
}
