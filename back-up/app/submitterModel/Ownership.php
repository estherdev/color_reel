<?php

namespace App\submitterModel;

use Illuminate\Database\Eloquent\Model;

class Ownership extends Model
{
    public $table = "ownership"; protected $primaryKey = 'id';
    protected $fillable = ['flim_id','owner_name','mailing_address','phone','email'];
    
    public function filmdetail()
    {
        return $this
            ->belongsTo('App\FilmInfo');
           
    }
}
