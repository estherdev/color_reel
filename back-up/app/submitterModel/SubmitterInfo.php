<?php

namespace App\submitterModel;

use Illuminate\Database\Eloquent\Model;

class SubmitterInfo extends Model
{
    public $table = "submitter_info"; protected $primaryKey = 'submitter_id';
    protected $fillable = ['name','email','phone_number','mailing_address','role_on_film','country_origin','dob','race','ethnicity','gender_identity','gender_expression','sexual_identity','disability','LGBTQ','user_id','film_id_fk'];
    public function filmdetail()
    {
        return $this
            ->belongsTo('App\FilmInfo');
           
    }
}
