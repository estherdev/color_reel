<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FilmInfo;
use App\submitterModel\Episode;
use App\submitterModel\FilmLanguage;
use App\submitterModel\Subtitle;
use App\submitterModel\SocialLinks;
use App\submitterModel\SubmitterInfo;
use App\submitterModel\Ownership;
use App\submitterModel\Distributors;
use App\User;
use Session;
class HomesController extends Controller
{
    //
    /**
     * load landing page of site
     * @method index 
     * @param null
     */
    public function index(Request $request)
    {
        return view('frontend.pages.index');
    }
    /**
     * Form submission page to make film maker
     * @method draftSubmission
     * @param null
     */
    public function draftSubmission(Request $request)
    {   
        
        return view('frontend.pages.draft_submissionform');
    }
    /**
     * Submit draft data 
     * @method submitDraftFormData
     * @param null
     */
    public function submitDraftFormData(Request $request)
    {
        $filminfo = $request->all();
        //print_r($filminfo);exit;
        $filminfo['film_type'] = implode(',',array_filter($request->post('film_type')));
        $filminfo['total_run_time'] = $request->post('hour').'hr :'.$request->post('minute').'min :' .$request->post('second').'sec';
        $filmid = FilmInfo::create($filminfo)->flim_id;
        //Episode
        $episode_title = array_filter($request->post('episode_title'));
        if(!empty($episode_title)){
            for($i=0 ; $i<count($episode_title);$i++){
                $episodes = array('episode_title' => $episode_title[$i],
                                  'flim_id_fk' =>$filmid,
                                  'episode_length' => $request->post('episode_hour')[$i].'hr :'.$request->post('episode_minute')[$i].'min :' .$request->post('episode_sec')[$i].'sec');
                Episode::create($episodes);                 
            }
        }
        // End Episode
        // movie Language
        $film_language = array_filter($request->post('language'));
        if(COUNT($film_language)>0){
            for($i=0 ; $i<count($film_language);$i++){
                $film_languages =array('language' => $film_language[$i],
                                       'film_id_fk' =>$filmid,
                                      );
                             
                FilmLanguage::create($film_languages);                 
            }
        }
        // End language
        // SubTitle  
        $subtitle  = array_filter($request->post('subtitle_lang'));
       
        if(COUNT($subtitle)>1){
            for($i=0 ; $i<count($subtitle);$i++){
                $subtitles = array('name' => $subtitle[$i],
                                  'flim_id_fk' =>$filmid,
                                );
               
                Subtitle::create($subtitles);                 
            }
        }
        //End Subtitle
        // social links
        $Slinks = $request->post('social_links');
        if(COUNT($Slinks)>0){
            for($i=0 ; $i<count($Slinks);$i++){
                $social_links =array('social_links' => $Slinks[$i],
                                       'film_id_fk' =>$filmid,
                                       'social_media_name'=>$request->post('social_media_name')[$i]
                                      );
                       
                SocialLinks::create($social_links);                 
            }
        }
        // end social
        // Submitter info
         $submitter = $request->all();
         $submitter['password'] = '123456';
         $user = User::create($submitter);
         $submitter['film_id_fk'] = $filmid;
         $submitter['user_id'] = $user->id;
         $submitter['role_on_film'] = implode(',',array_filter($request->post('role_on_film')));
      
         SubmitterInfo::create($submitter);
        // End Submitter
        //owener
        $ownership = array_filter($request->post('owner_name'));
        //print_r(count($ownership));exit;
        if(count($ownership)>0){
            for($i = 0 ;$i<count($ownership);$i++){
            $owner = array('owner_name'=>$ownership[$i],
                           'mailing_address' =>$request->post('owner_mailing_address')[$i],
                           'phone' => $request->post('owner_phone')[$i],
                           'email'=> $request->post('owner_email')[$i],
                           'flim_id' => $filmid );
            Ownership::create($owner);               
            }
        }
        //end owner
        //Distributer
        $distributor = array_filter($request->post('distributor_name'));
        if(count($distributor)>0){
            for($i = 0 ;$i<count($distributor);$i++){
            $distributor_detail = array('distributor_name'=>$distributor[$i],
                                        'mailing_address' =>$request->post('distributor_mailing_address')[$i],
                                        'phone' => $request->post('phone')[$i],
                                        'email'=> $request->post('distributor_email')[$i],
                                        'film_id' => $filmid );
            Distributors::create($distributor_detail);                              
            }
        }
        //End distributer

        try {
            
            if($filmid){
                //return redirect('/paypal');
                //stripe
                Session::put('filmid',$filmid);
                return redirect('/stripe');
            }
        } catch(\Illuminate\Database\QueryException $e){
            return redirect('/draft-submission-form')->with('error','something went wrong');
        }
    } 
    /**
     * payment success page  
     * @method thankyou
     **/  
    public function thankyou(Request $request)
    {    
        Session::forget('filmid');
        return view('frontend.pages.thankyou');
    }

}
