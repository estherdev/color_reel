<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Redirect;
use Hash;
Use Session;
use App\User;
use Mail;
class AdminController extends Controller
{
    public function __construct()
    {
      $this->User = new User;
      
      //$this->middleware('role:ROLE_ADMIN');
    }
    /**
     * load dashboard page 
     * @method dashboard
     * @param null
     */
    public function dashboard(Request $request)
    {
        return view('admin.dashboard');
    }
    /**
     * Make admin Login
     * @method adminLogin
     * @param null 
     */
    public function adminLogin(Request $request)
    {
        return view('admin.login');
    }
    /**
     * authorised user data
     * @method authorised
     * @param null
     */
    function authorised(Request $request)
    {
      
        $validatedData = $request->validate([
            'email_id'  => 'required',
            'password'  => 'required',
            
        ]);
        $password = Hash::make($request->post('password'));
        if (Auth::attempt ( array (
            'email' => $request->post ('email_id' ),
            'password' => $request->post ('password' ) 
        ) )) {
            
            
            $id = Auth::user()->id;
            session ( ['admin' => $id ]);
            return redirect()->intended('dashboard');
        }
        else{
            return redirect()->intended('admin-login')->with('error','Unauthorized access');
        }
        
    }
    /**
     * Profile section
     * @method profile
     * @param user id
     */
    public function profile(Request $request)
    {  
        $id =  $request->session()->get('admin');
        $detail = User::find($id);
        $submit = $request->post('submit');
        if(isset($submit)){
           
            $validatedData = $request->validate([
                'name'   => 'required',
                'email'  => 'required',
            ]); 
            $sitelogo ='';
            $sitelogo_file = $request->file('sitelogo');
           
            if(!empty($sitelogo_file)){
            $sitelogo = 'colorReel.'.$sitelogo_file->getClientOriginalExtension();
            $sitelogo_file->move('adminAssets/assets/images',$sitelogo);
            }

            $data = array_filter($request->only(['name', 'password','email']));
            $data['sitelogo']= $sitelogo != '' ? $sitelogo : $detail['sitelogo'] ;
            User::where('id',$id)->update($data);
            return redirect()->intended('profile')->with('success','Profile updated successfully!');
        }
       
        return view('admin.profile',compact('detail'));
    }
    /**
     * Forgot Password
     * @method forgotPassword
     * @param null
     */
    public function forgotPassword(Request $request)
    {
        $verify = $this->User->verifyEmail($request->post('email'));
        
        if($verify)
        {
            //print_r($verify);exit;
            $random =  str_random(6); 
            session( ['otp' => $random]);
            session( ['email' => $request->post('email')]);
             Mail::send('email-template.blank', ['otp' => $random], function ($m) use ($verify) {
                $m->from('hello@app.com', 'Color Reel');
    
                $m->to($verify->email, $verify->name)->subject('OTP Request');
            });
            return redirect('/reset-password')->with('success','OTP has been sent on email');
        }
        else{
            $request->session()->flash('error', 'invalid email id');
            return redirect()->back()->with('error_code','5');
        }
    }
    /**
     * Reset Password
     * @method resetPassword
     * @param null
     */
    public function resetPassword(Request $request)
    {   
        if(session('otp') ==''){
            return redirect('/admin-login') ; 
        }
        $confirm = $request->post('confirm');
        if($confirm){

             $validatedData = $request->validate([
            'otp'  => 'required',
            'password'  => 'required|confirmed',
        ]);
         if($request->post('otp') == session('otp')){
            $this->User->updatePassword(bcrypt($request->post('password')));
            Session::flush ();
            return redirect()->intended('/admin-login')->with('success','Password has been changed');

         }
            
        }
        return view('admin.reset_password');
    }
   /**
     * Logout
     * @method logout
     * @param null
     */
    public function logout() {
		Session::flush ();
		Auth::logout ();
		return redirect('/admin-login');
    }
    /**
     * Update Status 
     * @method updateStatus
     * @param primary_key , table_name
     * 
     */
    public function updateStatus(Request $request ,$table_name = null,$primary_key = null)
    {
      $updated =  $this->User->update($table_name,$primary_key);
      if($updated)
      {
          return redirect::back()->with('success','Status updated successfully');
      }
    }
    /**
     * Add Draft Submission form fee
     * @method draftFee
     * @param null
     */
    public function draftFee(Request $request)
    {   
        $detail = array('draft_amount' => $request->post('draft_amount'),
                         'updated_at' => date('Y-m-d h:i:s'));
         DB::table('site_setting')->delete();                
         DB::table('site_setting')->insert($detail);
         return redirect::back()->with('success','Payment updated successfully');
    }
}
