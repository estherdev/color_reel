<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserModel;
use App\Package;
use App\Role;
Use App\FilmMaker;
use App\FilmInfo;
use Redirect;
class FilmMakersController extends Controller
{
    /**
     * Add User and role by admin
     * @method addUser
     * @param null
     */
    public function addUser(Request $request)
    {  
        $package = Package::all();
        return view('admin.addfilmmaker',compact('package'));
    }

    /**
     * Save user detail 
     * @method submitDetail
     * @param null
     */
    public function submitDetail(Request $request ,$id = null )
    {
        $validatedData = $request->validate([
            'name'   => 'required|string',
            'email'  => 'required|unique:users,email,'.$id.',id',
            'phone_number'  =>'required|numeric|regex:/[0-9]{10}/',
            'password'  =>'required|confirmed',
           
        ]); 
        
       $password = bcrypt($request->post('password'));
       $basic = $request->all();
       $basic['password'] = $password;
      
       $user = User::updateOrCreate(['id' => $id,],$basic);
       
       $data = $request->except(['_token']);
       $data['user_id'] = $user->id;
       FilmMaker::updateOrCreate(['user_id' => $id],$data);
       $role = Role::where('name','ROLE_FILM')->first();
       $user->roles()->attach($role->id);
        
       return redirect('film-makers-list')->with('success','User added successfully');
        
    }

    /**
     * fetch all user detail
     * @method viewallUser
     * @param null
     */
    public function viewallUser(Request $request)
    {
        $users = FilmMaker::all();
        return view('admin.viewallfilmmakers',compact('users'));
    }
    /**
     * Edit Package list
     * @method editPackage
     * @param package id 
     */
    public function editUser(Request $request,$id = null)
    {  
        $package = Package::all();
        $user = FilmMaker::find($id);
        return view('admin.addfilmmaker',compact('package','user'));
    }
    /**
     * Delete user 
     * @method deleteData
     * @param id
     */
    public function deleteData(Request $request,$id = null)
    {
       
        $film_id = FilmMaker::find($id)->film_id_fk;
     
        FilmInfo::destroy($film_id);
        User::destroy($id);
        return redirect::back()->with('success','Delete Successfully');
        
       
       
    }
}
