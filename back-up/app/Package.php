<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Package extends Model
{   
    protected $guarded = ['token'];
    public $table = "package_detail"; protected $primaryKey = 'package_id';

    /**
     * Update Status 
     * @method updatemoduleStatus
     * @param $table_name,$primary_key
     */
    public function updatemoduleStatus($table_name = null,$primary_key = null)
    {
        $column_name=  substr($table_name,0,strpos($table_name,"_")).'_id';
       
        $status = DB::table($table_name)->where($column_name,$primary_key)->first('status');
        if($status->status == 1 )
      {
        return DB::table($table_name)->where($column_name,$primary_key)->update(array('status'=>0));
      }else{
       return  DB::table($table_name)->where($column_name,$primary_key)->update(array('status'=>1));
      }
    }

   
}

