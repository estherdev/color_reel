<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
/** ADMIN Routes  */
Route::get('/admin-login','admin\AdminController@adminLogin');
Route::post('/verify','admin\AdminController@authorised');
Route::get('/adminlogout','admin\AdminController@logout');
Route::any('/forgot-password','admin\AdminController@forgotPassword');
Route::any('/reset-password','admin\AdminController@resetPassword');
Route::group(['middleware' => ['AdminMiddleware','role:ROLE_ADMIN']], function () {

Route::get('/dashboard','admin\AdminController@dashboard');
Route::any('/profile','admin\AdminController@profile');
Route::get('/user-draft-submission','admin\FormsubmissionController@draftSubmission');
Route::get('/user-draft-submission-detail/{id}','admin\FormsubmissionController@draftSubmissionDetail');
Route::get('/update-status/{id}/{id1}','admin\AdminController@updateStatus');

/** Draft Submission Fee*/
Route::any('/draft-amount','admin\AdminController@draftFee');
// Packages 
Route::get('/add-package','admin\PackagesController@addPackage');
Route::post('/package-submit-detail','admin\PackagesController@submitDetail');
Route::post('/package-submit-detail/{id}','admin\PackagesController@submitDetail');
Route::get('/packages-list','admin\PackagesController@viewPackages');
Route::get('/edit-package/{id}','admin\PackagesController@editPackage');
Route::post('/update-package/{id}','admin\PackagesController@updatePackage');
Route::get('/delete-package/{id}','admin\PackagesController@deleteData');
Route::get('/modify-status/{id}/{id1}','admin\PackagesController@modifyStatus');
// Add User by Admin
Route::get('add-user','admin\UsersController@addUser');
Route::post('/submit-detail','admin\UsersController@submitDetail');
Route::post('/submit-detail/{id}','admin\UsersController@submitDetail');
Route::get('/users-list','admin\UsersController@viewallUser');
Route::get('/edit-user/{id}','admin\UsersController@editUser');
Route::get('/delete-user/{id}','admin\UsersController@deleteData');

// Film makers 
Route::get('add-film-makers','admin\FilmMakersController@addUser');
Route::post('/filmmaker-submit-detail','admin\FilmMakersController@submitDetail');
Route::post('/filmmaker-submit-detail/{id}','admin\FilmMakersController@submitDetail');
Route::get('/film-makers-list','admin\FilmMakersController@viewallUser');
Route::get('/edit-film-maker/{id}','admin\FilmMakersController@editUser');
Route::get('/delete-user/{id}','admin\FilmMakersController@deleteData');

// Home page Banners 
Route::get('add-banner','admin\BannersController@addBanner');
Route::post('submit-banner-detail','admin\BannersController@submitDetail');
Route::post('submit-banner-detail/{id}','admin\BannersController@submitDetail');
Route::get('edit-banner-detail/{id}','admin\BannersController@editbanner');
Route::get('banners','admin\BannersController@viewBanners');
Route::get('delete-banner/{id}','admin\BannersController@deleteBanner');

/**  CMS  */
Route::get('cms/add-page','admin\CMSController@addPage');
Route::post('submit-page-detail','admin\CMSController@submitDetail');
Route::post('submit-page-detail/{id}','admin\CMSController@submitDetail');
Route::get('cms/view-page','admin\CMSController@viewPage');
Route::get('cms/edit-page/{id}','admin\CMSController@EditPage');
Route::get('cms/delete-page/{id}','admin\CMSController@EditPage');
// Blog Section//
Route::get('blog/add-blog','admin\BlogController@addBlog');
Route::post('submit-blog-detail','admin\BlogController@submitDetail');
Route::post('submit-blog-detail/{id}','admin\BlogController@submitDetail');
Route::get('blog/blog-list','admin\BlogController@viewBloglist');
Route::get('blog/edit-blog/{id}','admin\BlogController@Editblog');
Route::get('delete-blog/{id}','admin\BlogController@deleteBlog');
/* Gallery Section  */
Route::get('add-gallery','admin\GalleryController@addGallery');
Route::post('save-images','admin\GalleryController@saveImage');
Route::get('gallery-list','admin\GalleryController@viewGallery');
});

/**
 * FRONT END SECTION 
 * ALL ROUTES
 */
Route::get('/','front\HomesController@index');
Route::get('/draft-submission-form','front\HomesController@draftSubmission');
Route::post('submit-draft-from-data','front\HomesController@submitDraftFormData');
Route::any('/paypal','PaymentController@payWithpaypal');
Route::get('status', 'PaymentController@getPaymentStatus');
Route::get('/thankyou','front\HomesController@thankyou');


//STRIPE
Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripesubmit', 'StripePaymentController@stripePost');
//Route::post('stripesubmit', 'StripePaymentController@stripePost')->name('stripe.post');