@extends('admin.mainlayout')
@section('title', 'Admin Draft Submission detail')
@section('content')
@section('heading','Draft Submission Detail')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Draft Submission')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
      <?php //echo "<pre>"; print_r($detail);exit;?>
      <div class="accordion" id="accordionExample">
         <div class="card m-b-0">
            <div class="card-header" id="headingOne">
               <h5 class="mb-0">
                  <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <i class="m-r-5 fa fa-film" aria-hidden="true"></i>
                  <span>Project Basic Info</span>
                  </a>
               </h5>
            </div>
            @if(!empty($detail))
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
               <div class="card-body">
                  <h6 class = "card-title"> <b>Project Title : {{$detail->project_title}}</b>({{$detail->film_type}})</h6>
                  <hr>
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Total Run Time :</b> {{$detail->total_run_time}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Country of Origin :</b> {{$detail->country_origin}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Country of Filming :</b> {{$detail->country_filming}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Trailer Link :</b> <a href = "{{$detail->trailer_link}}" target = "_blank" id = "trailer">{{$detail->trailer_link}}</a>
                        <button onclick="copyToClipboard('#trailer')" >Copy</button>
                        </h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Full Movie Link :</b> <a href = "{{$detail->full_movie_link}}" target = "_blank" id = "fullmovie">{{$detail->full_movie_link}}</a>
                        <button onclick="copyToClipboard('#fullmovie')" >Copy</button>
                        </h6>
                        
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Website :</b> {{$detail->website != '' ? $detail->website : 'Not Found'}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-8">
                        <h6 class = "card-title"><b>Awards :</b> {{$detail->screenings_and_awards}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Completion date :</b> {{$detail->flim_complition_date}}</h6>
                     </div>
                  </div>
                  <hr>
                  <h6 class = "card-title"><b>Description :</b> {{$detail->project_synopsis}}</h6>
               </div>
            </div>
         </div>
         <div class="card m-b-0 border-top">
            <div class="card-header" id="headingTwo">
               <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <i class="m-r-5 fa fa-play" aria-hidden="true"></i>
                  <span>Episodes</span>
                  </a>
               </h5>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
               <div class="card-body">
                  @foreach($detail->episodes as $list)
                  <div class = "row">
                     <div class = "col-md-8">
                        <h6 class = "card-title"><b>Title :</b> {{$list->episode_title}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Length :</b> {{$list->episode_length}}</h6>
                     </div>
                  </div>
                  @endforeach()
               </div>
            </div>
         </div>
         <div class="card m-b-0 border-top">
            <div class="card-header" id="headingThree">
               <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                  <i class="m-r-5 fa fa-language" aria-hidden="true"></i>
                  <span>Film Language</span>
                  </a>
               </h5>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
               <div class="card-body">
                  @foreach($detail->language as $list)
                  <div class = "row">
                     <div class = "col-md-8">
                        <h6 class = "card-title"><b>Language :</b> {{$list->language}}</h6>
                     </div>
                  </div>
                  @endforeach
               </div>
            </div>
         </div>
         <div class="card m-b-0 border-top">
            <div class="card-header" id="headingFour">
               <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                  <i class="m-r-5 fa fa-file" aria-hidden="true"></i>
                  <span>Subtitle</span>
                  </a>
               </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
               <div class="card-body">
                  @forelse($detail->subtitle as $list)
                  <div class = "row">
                     <div class = "col-md-8">
                        <h6 class = "card-title"><b>Subtitle :</b> {{$list->name}}</h6>
                     </div>
                  </div>
                  @empty
                  <h6 class = "card-title">No data found</h6>
                  @endforelse
               </div>
            </div>
         </div>
         <div class="card m-b-0 border-top">
            <div class="card-header" id="headingFive">
               <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                  <i class="m-r-5 fa fa-link" aria-hidden="true"></i>
                  <span>Social Links</span>
                  </a>
               </h5>
            </div>
            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
               <div class="card-body">
                  @forelse($detail->sociallink as $list)
                  <div class = "row">
                     <div class = "col-md-2">
                        <h6 class = "card-title">{{$list->social_media_name}}</h6>
                     </div>
                     <div class = "col-md-3">
                        <h6 class = "card-title"><a href = "{{$list->social_links}}">{{$list->social_links}}</a></h6>
                     </div>
                  </div>
                  @empty
                  <h6 class = "card-title">No data found</h6>
                  @endforelse
               </div>
            </div>
         </div>
         <div class="card m-b-0 border-top">
            <div class="card-header" id="headingSix">
               <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                  <i class="m-r-5 fa fa-paste" aria-hidden="true"></i>
                  <span>Distributors</span>
                  </a>
               </h5>
            </div>
            <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionExample">
               <div class="card-body">
                  @forelse($detail->distributer as $list)
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Name :</b> {{$list->distributor_name}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Email :</b> {{$list->distributor_email}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Phone :</b> {{$list->phone}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-12">
                        <h6 class = "card-title"><b>Mailing Address :</b> {{$list->mailing_address}}</h6>
                     </div>
                     
                  </div>
                  @empty
                  <h6 class = "card-title">No data found</h6>
                  @endforelse
               </div>
            </div>
         </div>
         <div class="card m-b-0 border-top">
            <div class="card-header" id="headingSeven">
               <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                  <i class="m-r-5 fa fa-th" aria-hidden="true"></i>
                  <span>Ownership</span>
                  </a>
               </h5>
            </div>
            <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordionExample">
               <div class="card-body">
                  @forelse($detail->ownerships as $list)
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Name :</b> {{$list->owner_name}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Email :</b> {{$list->email}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Phone :</b> {{$list->phone}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-12">
                        <h6 class = "card-title"><b>Mailing Address :</b> {{$list->mailing_address}}</h6>
                     </div>
                     
                  </div>
                  @empty
                  <h6 class = "card-title">No data found</h6>
                  @endforelse
               </div>
            </div>
         </div>
         <div class="card m-b-0 border-top">
            <div class="card-header" id="headingEight">
               <h5 class="mb-0">
                  <a class="collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                  <i class="m-r-5 fas fa-address-card" aria-hidden="true"></i>
                  <span>Submitter Info</span>
                  </a>
               </h5>
            </div>
            <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordionExample">
               <div class="card-body">
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Name :</b> {{$detail->submitter->name}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Email :</b> {{$detail->submitter->email}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Phone :</b> {{$detail->submitter->phone_number}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-12">
                        <h6 class = "card-title"><b>Mailing Address :</b> {{$detail->submitter->mailing_address}}</h6>
                     </div>
                    
                  </div>
                  <div class = "row">
                    
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Role on Film :</b> {{$detail->submitter->role_on_film}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Country Origin :</b> {{$detail->submitter->country_origin}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>DOB :</b> {{$detail->submitter->dob}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Race :</b> {{$detail->submitter->race}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Ethnicity :</b> {{$detail->submitter->ethnicity}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Gender Identity :</b> {{$detail->submitter->gender_identity}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Gender Expression :</b> {{$detail->submitter->gender_expression}}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Sexual Identity :</b> {{$detail->submitter->sexual_identity}}</h6>
                     </div>
                  </div>
                  <div class = "row">
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>Disablity :</b> {{$detail->submitter->disability ?? 'No' }}</h6>
                     </div>
                     <div class = "col-md-4">
                        <h6 class = "card-title"><b>LGBTQ :</b> {{$detail->submitter->LGBTQ}}</h6>
                     </div>
                  </div>
               </div>
            </div>
            @else
            <h4 class = "card-title text-center">No Data Found</h4>
            @endif
         </div>
      </div>
   </div>
</div>
@endsection
<script>
function copyToClipboard(element) {
  var $temp = $("<input>");
 
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
  $(element).next().text('Copied');
}
</script>