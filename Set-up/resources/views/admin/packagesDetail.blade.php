@extends('admin.mainlayout')
@section('title', 'Admin package')
@section('content')
@section('heading','Packages')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Packages')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href= "{{url('add-package')}}" class = "btn btn-primary float-right mento" >Add Package</a>
      <h5 class="card-title">Packages List</h5>
     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered">
         <?php //echo "<pre>";print_r($packages);exit;?>
            <thead>
               <tr>
                  <th>Sr No.</th>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Duration</th>
                  <th>Detail</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
            @forelse($packages as $row)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$row['package_name']}}</td>
              <td>{{$row['package_price']}}</td>
              <td>{{$row['duration_value']}} {{$row['duration_time']}}</td>
              <td>{{$row['detail']}}</td>
              <td><a href = "{{url('modify-status/package_detail/'.$row['package_id'])}}">{{$row['status'] == 1 ? 'Active':'Inactive'}}</a></td>
              <td><a href = "{{url('edit-package/'.$row['package_id'])}}"><i class = "fa fa-edit"></i></a> | 
                  <a href = "{{url('delete-package/'.$row['package_id'])}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr>No data found<tr>
            @endforelse 

            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection