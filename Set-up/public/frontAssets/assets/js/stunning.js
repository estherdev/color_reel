// Draft Submission js 

$(document).ready(function(){

  $('#filmtype').change(function(){
    var values = $(this).val();
    if(values == 'Other'){
      $('#other').show();
    }
    else{
      $('#other').hide();
    }
  });
});

$(document).ready(function() {
   
  //here first get the contents of the div with name class copy-fields and add it to after "after-add-more" div class.
   var  hours = '';
  for(var j=0;j<100;j++) {
     hours += '<option>'+j+'</option>';
     
  }
   var minute = '';
   for(var k= 0;k< 60 ; k++)
   {
    minute += '<option>'+k+'</option>';
   }
  var i= 2; $(".add-more").click(function(){ 
      if(i<11){
     var field='<div class="control-group col-md-12">';
     field+= '<div class="form-group">';
     field+= '<label>Episode Length</label>';
     field+= '<div class="row">';
     field+= '<div class="col-md-2">';
     field+= '<input type = "text" name = "episode_title[]" class = "form-control">';
     field+= '</div>';
     field+= '<div class="col-md-3">';
     field+= '<select class="form-control" name = "episode_hour[]">';
     field+= '<option>Hrs.</option>';
     field+= hours;
     field+= '</select>';
     field+= '</div>';
     field+= '<div class="col-md-3">';
     field+= '<select class="form-control" name = "episode_minute[]">';
     field+= '<option>Min.</option>';
     field+= minute;
     field+= '</select>';
     field+= '</div>';
     field+= '<div class="col-md-3">';
     field+= '<select class="form-control" name = "episode_sec[]">';
     field+= '<option>Sec.</option>';
     field+= minute;
     field+= '</select>';
     field+= '</div>';
     field+= '<div class="col-md-1">';
     field+= '<a href="javascript:void(0)" class="remove"><i class="fas fa-minus"></i></a>';
     field+= '</div>';
     field+= '</div>';
     field+= '</div>';
     field+= '</div>';
      i++;
      $(".after-add-more").append(field);
   }
   });
  //here it will remove the current value of the remove button which has been pressed
   $("body").on("click",".remove",function(){ 
       $(this).parents(".control-group").remove();
   });
  
  });

  //

  // Form Elements 

  $('#chooselng').change(function(){
    
    var lang = $( 'input[name=other_lang]:checked' ).val()
    if(lang == 'yes')
    {
      $('#filmlng').show();
    }else{
      $('#filmlng').hide();
    }
  })

  $('#subtitle').change(function(){
    
    var sublang = $( 'input[name=subtitle_lang]:checked' ).val()
    
    if(sublang == 'english')
    {
      $('#othersubtitle').hide();
    }else{
      $('#othersubtitle').show();
    }
  })
  $('.ownership').hide();
  $('#checkowner').change(function(){
    var checkowner = $( 'input[name=checkowner]:checked').val()
    //alert(checkowner);
    if(checkowner == 'yes')
    {
      $('.ownership').hide();
    }else{
      $('.ownership').show();
    }
  })
  var i= 2; $(".add-new").click(function(){ 
    var newadd = '<div class = "">';
    newadd += '<div class= "row ownership">';
    newadd += '<div class="col-md-4 ">';
    newadd += '<div class="form-group">';
    newadd += '<label>Other Owner`s Name:</label>';
    newadd += '<input type="text" class="form-control" name = "owner_name[]">';
    newadd += '</div>';
    newadd += '</div>';
    newadd += '<div class="col-md-4 ">';
    newadd += '<div class="form-group">';
    newadd += '<label>Other Owner`s Email:</label>';
    newadd += '<input type="text" class="form-control" name = "email[]">';
    newadd += '</div>';
    newadd += '</div>';
    newadd += '<div class="col-md-4 ">';
    newadd += '<div class="form-group">';
    newadd += '<label>Other Owner`s Phone:</label>';
    newadd += '<input type="text" class="form-control" name = "phone[]">';
    newadd += '</div>';
    newadd += '</div>';
    newadd += '<div class="col-md-12 ">';
    newadd += '<div class="form-group">';
    newadd += '<label>Other Owner`s Address:</label>';
    newadd += '<textarea class="form-control" rows="4" name = "mailing_address[]"></textarea>';
    newadd += '</div>';
    newadd += '</div>';
    newadd += '<div class = "col-md-12 text-right">';
    newadd += '<button class = "btn btn-danger pull-right remove-new" type = "button">Remove</button>';
    newadd += '</div>';
    newadd += '<hr>';
    newadd += '</div> ';
    newadd += '</div>';
    i++;
    $(".after-add-new").append(newadd);
  });
  $("body").on("click",".remove-new",function(){ 
    $(this).parents(".ownership").remove();
});

//Distributer
$('.distributor').hide();
$('#checkdistributor').change(function(){
  var checkowner = $( 'input[name=checkdistributor]:checked').val()
  //alert(checkowner);
  if(checkowner == 'yes')
  {
    $('.distributor').hide();
  }else{
    $('.distributor').show();
  }
})

var i= 2; $(".add-new-one").click(function(){ 
  var newadd = '<div class = "">';
  newadd += '<div class= "row distributor">';
  newadd += '<div class="col-md-4 ">';
  newadd += '<div class="form-group">';
  newadd += '<label> Distributor’s Name:</label>';
  newadd += '<input type="text" class="form-control" name = "distributor_name[]">';
  newadd += '</div>';
  newadd += '</div>';
  newadd += '<div class="col-md-4 ">';
  newadd += '<div class="form-group">';
  newadd += '<label> Distributor’s Email:</label>';
  newadd += '<input type="text" class="form-control" name = "distributor_email[]">';
  newadd += '</div>';
  newadd += '</div>';
  newadd += '<div class="col-md-4 ">';
  newadd += '<div class="form-group">';
  newadd += '<label> Distributor’s Phone:</label>';
  newadd += '<input type="text" class="form-control" name = "phone[]">';
  newadd += '</div>';
  newadd += '</div>';
  newadd += '<div class="col-md-12 ">';
  newadd += '<div class="form-group">';
  newadd += '<label> Distributor’s Address:</label>';
  newadd += '<textarea class="form-control" rows="4" name = "distributor_mailing_address[]"></textarea>';
  newadd += '</div>';
  newadd += '</div>';
  newadd += '<div class = "col-md-12 text-right">';
  newadd += '<button class = "btn btn-danger pull-right remove-new-one" type = "button">Remove</button>';
  newadd += '</div>';
  newadd += '<hr>';
  newadd += '</div> ';
  newadd += '</div>';
  i++;
  $(".after-add-new-one").append(newadd);
});
$("body").on("click",".remove-new-one",function(){ 
  $(this).parents(".distributor").remove();
});

// Date Picker

$( function() {
  $( "#flim_complition_date" ).datepicker({
    changeMonth: true,
    changeYear: true
  });
} );

$( function() {
  $( "#dob" ).datepicker({
    changeMonth: true,
    changeYear: true,
    maxDate:0
  });
} );

// Non English Title
$('.noneng').hide();
$('#nonengtitle').change(function(){

 var nontitle = $('input[name=nontitle]:checked').val()
 if(nontitle == 'yes'){
  $('.noneng').show();
 }
 else{
  $('.noneng').hide();
 }
});

var i= 2; $(".add-new-more").click(function(){ 
  var newone = '<div class = "">';
  newone += '<div class= "row noneng">';
  newone += '<div class="col-md-6">';
  newone += '<div class="form-group">';
  newone += '<label>Non-English Title</label>';
  newone += '<input type="text" class="form-control" name = "non_eng_title[]">';
  newone += '</div>';
  newone += '</div>';
  newone += '<div class="col-md-6">';
  newone += '<div class="form-group">';
  newone += '<label>Non-English Synopsis</label>';
  newone += '<input type="text" class="form-control" name = "non_eng_description[]">';
  newone += '</div>';
  newone += '</div>';
  newone += '<button class = "btn btn-danger pull-right remove-new-one-more" type = "button">Remove</button>';
  newone += '</div>';
  newone += '</div>';
  i++;
  $(".after-add-new-one-more").append(newone);
});
$("body").on("click",".remove-new-one-more",function(){ 
  $(this).parents(".noneng").remove();
});





