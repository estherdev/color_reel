<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilmInfo extends Model
{  
    public $table = "film_info"; protected $primaryKey = 'flim_id';
    protected $fillable = ['film_type','film_genre','project_title','project_synopsis','total_run_time','country_origin','country_filming','flim_complition_date','trailer_link','full_movie_link','website','screenings_and_awards','has_disrtibuter','ownership'];
    public function episodes()
    {
        return $this
            ->hasMany('App\submitterModel\Episode','flim_id_fk');
            
    }
    
    public function submitter()
    {
        return $this
            ->hasOne('App\submitterModel\SubmitterInfo','film_id_fk');
            
    }

    public function language()
    {
        return $this
            ->hasMany('App\submitterModel\FilmLanguage','film_id_fk');
            
    }

    public function distributer()
    {
        return $this
            ->hasMany('App\submitterModel\Distributors','film_id');
            
    }

    public function ownerships()
    {
        return $this
            ->hasMany('App\submitterModel\Ownership','flim_id');
            
    }

    public function subtitle()
    {
        return $this
            ->hasMany('App\submitterModel\Subtitle','flim_id_fk');
            
    }

    public function sociallink()
    {
        return $this
            ->hasMany('App\submitterModel\SocialLinks','film_id_fk');
            
    }
    
}
