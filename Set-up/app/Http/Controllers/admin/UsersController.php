<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserModel;
use App\Package;
use App\Role;
use Redirect;
class UsersController extends Controller
{
    /**
     * Add User and role by admin
     * @method addUser
     * @param null
     */
    public function addUser(Request $request)
    {  
        $package = Package::all();
        return view('admin.adduser',compact('package'));
    }

    /**
     * Save user detail 
     * @method submitDetail
     * @param null
     */
    public function submitDetail(Request $request ,$id = null )
    {
        $validatedData = $request->validate([
            'name'   => 'required|string',
            'email'  => 'required|unique:users,email,'.$id.',id',
            'phone_number'  =>'required|numeric|regex:/[0-9]{10}/',
            'password'  =>'required|confirmed',
            'package' =>'required'
        ]); 
        
        
       $user = User::updateOrCreate(['id' => $id,],$request->except(['_token']));
       
       $data = $request->except(['_token','password_confirmation']);
       $data['user_id'] = $user->id;
       UserModel::updateOrCreate(['user_id' => $id,],$data);
       $role = Role::where('name','ROLE_USER')->first();
       $user->roles()->attach($role->id);
        
       return redirect('users-list')->with('success','User added successfully');
        
    }

    /**
     * fetch all user detail
     * @method viewallUser
     * @param null
     */
    public function viewallUser(Request $request)
    {
        $users = UserModel::with('userpackage')->get()->toArray();
        return view('admin.viewallUser',compact('users'));
    }
    /**
     * Edit Package list
     * @method editPackage
     * @param package id 
     */
    public function editUser(Request $request,$id = null)
    {   
        
        $package = Package::all();
        $user = UserModel::find($id);
        return view('admin.adduser',compact('package','user'));
    }
    /**
     * Delete user 
     * @method deleteData
     * @param id
     */
    public function deleteData(Request $request,$id = null)
    {
        User::destroy($id);
        return redirect::back()->with('success','Delete Successfully');
    }
}
