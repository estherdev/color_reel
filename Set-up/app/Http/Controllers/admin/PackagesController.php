<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Package;
use Redirect;
class PackagesController extends Controller
{
    
    /**
     * view packges load 
     * @method viewPackages 
     * @param null
     */
    public function viewPackages(Request $request)
    {
        $packages = Package::all();
        return view('admin.packagesDetail',compact('packages'));
    }

    /**
     * Load Add Packages detail Page
     * @method addPackage
     * @param null
     */
    public function addPackage(Request $request)
    {
        return view('admin.addpackagesDetail');
    }
    /**
     * Submit Packes detail
     * @method submitDetail
     * @param $request
     */
    public function submitDetail(Request $request ,$id = null )
    {
        
        $validatedData = $request->validate([
            'package_name'   => 'required',
            'package_price'  => 'required|numeric',
            'detail'        =>'required'
        ]); 
       
        $inserted = Package::updateOrCreate(['package_id' => $id],$request->except(['_token']));
        if($id){
            return redirect('packages-list')->with('success','Packges updated Successfully');
        }
        else{
            return redirect('packages-list')->with('success','Packges Creted Successfully');
        }
        
    }
    /**
     * Edit Package list
     * @method editPackage
     * @param package id 
     */
    public function editPackage(Request $request,$id = null)
    {  
        $package = Package::find($id);
        
        return view('admin.addpackagesDetail',compact('package'));
    }
    
    /**
     * Delete data 
     * @method deleteData
     * @param id
     */
    public function deleteData(Request $request,$id = null)
    {
        Package::destroy($id);
        return redirect('packages-list')->with('success','Packges delete Successfully');
    }

    /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$primary_key = null)
    {
        $package = new Package;
        $changed = $package->updatemoduleStatus($table_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }

    }

   
}
