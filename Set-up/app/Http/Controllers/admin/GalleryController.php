<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Gallery;
class GalleryController extends Controller
{
    //
    /**
     * load Gallery Page
     * @method addGallery
     * @param null
     */
    public function addGallery(Request $request)
    {
        return view('admin.addImages');
    }
    /**
     * Save Image 
     * @method saveImage
     * @param null
     */
    public function saveImage(Request $request,$id = null)
    {
        
        $image = $request->file('file_name') != '' ? $request->file('file_name') : $request->file('video') ;
       
    
        if(!empty($request->file('file_name'))){
            $validation = Validator::make($request->all(), [
                'file_name' => 'image|mimes:jpeg,png,jpg,gif|max:1024'
               ]);
            } 
            else if(!empty($request->file('video'))) 
            {
                $validation = Validator::make($request->all(), [
                    'file_name' => 'mimes:mp4,mov,ogg,qt|max:20000'
                   ]);
            } 
            else{
                $validation = Validator::make($request->all(), [
                    'url' => 'required'
                   ]);
            }  
          
               if($validation->passes()){
               if(!empty($image)){
                    $new_name = rand() . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('gallery'), $new_name);
                    $data = $request->all();
                    $data['file_name'] = $new_name;
               }
               else{
                   $message = $this->convertVimeo($request->post('url'));
                   $data = $request->all();
                   $data['file_name'] = $message;
                  
               }
               
                Gallery::updateOrCreate(['gallery_id' => $id],array_filter($data));

                return response()->json([
                'message'   => 'file Upload Successfully',
                'class_name'  => 'alert-success',
                'status' => 200
                ]);
            }

            else{
                return response()->json([
                'message'   => $validation->errors()->all(),
                'uploaded_image' => '',
                'class_name'  => 'alert-danger',
                'status' => 401
        ]);

        }
    }
 
    public function convertVimeo($text = null)
    {
        $text = preg_replace('#https?://(www\.)?vimeo\.com/(\d+)#',
           '<iframe class="videoFrame" src="//player.vimeo.com/video/$2" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>',
           $text);
        return $text;
        
    
    }
     /**
     * fetch gallery
     * @method fetchGallery
     * @param null
     */
    public function viewGallery(Request $request)
    {
         $files = Gallery::all();
        return view('admin.gallery',compact('files'));

    }
}
