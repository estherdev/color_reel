<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CMSModel extends Model
{
    public $table = "page_list"; protected $primaryKey = 'page_id';
    protected $fillable = ['page_title','page_description','page_image','meta_title','meta_description','meta_keywords'];
}
