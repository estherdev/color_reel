<?php

namespace App\submitterModel;

use Illuminate\Database\Eloquent\Model;

class Subtitle extends Model
{
    public $table = "subtitle"; protected $primaryKey = 'sub_title_id';
    protected $fillable = ['flim_id_fk','name'];
    public function filmdetail()
    {
        return $this
            ->belongsTo('App\FilmInfo');
           
    }
}
