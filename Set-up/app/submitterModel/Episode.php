<?php


namespace App\submitterModel;
use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    public $table = "episode"; protected $primaryKey = 'episode_id';
    protected $fillable = ['episode_title','flim_id_fk','episode_length'];
    public function filmdetail()
    {
        return $this
            ->belongsTo('App\FilmInfo');
           
    }
}
