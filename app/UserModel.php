<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class UserModel extends Model
{
    protected $guarded = ['token'];
    public $table = "user_info"; 
    protected $primaryKey = 'user_id';
 
    public function userpackage()
    {
        return $this
            ->belongsTo('App\Package','package');
           
    }

}
