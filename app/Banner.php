<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Banner extends Model
{
    public $table = "banner_list"; protected $primaryKey = 'banner_id';
    protected $fillable = ['heading','sub_heading','url','banner_image'];
}
