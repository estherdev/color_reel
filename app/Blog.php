<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Blog extends Model
{
    public $table = "blog_list"; protected $primaryKey = 'blog_id';
    protected $fillable = ['title','description','image'];

     public function fetchdatabloglatest(){
        return DB::table('blog_list') 
                   ->where('status', 1)
                   ->orderBy('blog_id','DESC')
                   ->take(1)
                   ->get();
    }
     public function fetchdata(){
        return DB::table('blog_list') 
                 ->where('status', 1)
                 ->orderBy('blog_id','DESC')
                 ->skip(1)
                 ->take(3)
                 ->get();
    }

    public function fetchBlogWhere($where){
        return DB::table('blog_list')
                         ->select('*')
                         ->where('status', 1)
                         ->where('blog_id', $where)
                         ->orderBy('blog_id','DESC')
                         ->first();
    }
}
