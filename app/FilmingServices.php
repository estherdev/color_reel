<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class FilmingServices extends Model
{
	protected $guarded = ['token']; public $table = "filming_services"; protected $primaryKey = 'id';
    protected $fillable = ['title','description','image'];

    /**
     * Update Status 
     * @method updatemoduleStatus
     * @param $table_name,$primary_key
     */
    public function updatemoduleStatus($table_name = null,$primary_key = null){
        $column_name=  'id';       
        $status = DB::table($table_name)->where($column_name,$primary_key)->first('status');
        if($status->status == 1 ){
          return DB::table($table_name)->where($column_name,$primary_key)->update(array('status'=>0));
        }else{
          return  DB::table($table_name)->where($column_name,$primary_key)->update(array('status'=>1));
        }
    }

//->get()->toArray();
    public function fetchdata(){
      return $filming_services = DB::table('filming_services')
                                           ->select('*')
                                           ->where('status', 1)
                                           ->orderBy('id','DESC')
                                           ->get();
    }

}
