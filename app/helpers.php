<?php 
if (! function_exists('sitelogo')) {
  function sitelogo($user_id = null) {
    $data = \DB::table('users')
        ->select('*')
        ->where('id', $user_id)
        ->first();

    return $data;
  }
}

if (! function_exists('draftprice')) {
  function draftprice() {
    $data = \DB::table('site_setting')
        ->select('*')
        ->first();

    return $data;
  }
}
if (! function_exists('category')) {
  function category() {
        return $users = DB::table('categories')->where('status', 1)->get()->toArray();
  }
}

if (! function_exists('updatemoduleStatus')) {


 function updatemoduleStatus($table_name = null,$column_name = null,$primary_key = null)
    {
      //$column_name=  'id';       
      $status = DB::table($table_name)->where($column_name,$primary_key)->first('status');
      if($status->status == 1 ){
        return DB::table($table_name)->where($column_name,$primary_key)->update(array('status'=>0));
      }else{
       return  DB::table($table_name)->where($column_name,$primary_key)->update(array('status'=>1));
      }
    }


    function pr($d, $echo = TRUE){
          if($echo){
              echo '<pre>'.print_r($d, true).'</pre>';
          }else{
             return '<pre>'.print_r($d, true).'</pre>';
          } 
       }

       function prd($d){   
          pr($d);     
          die; 
       }

       function vr($d , $echo = TRUE){ 
          if($echo){
              echo '<pre>';var_dump($d, true);
          }else{
             return '<pre>';var_dump($d, true);
          } 
       }

       function vrd($d){
          vr($d);
          die; 
       }




if (! function_exists('DDMMYY')) {
    function DDMMYY($date){
      if($date!=''){
        $return= date('d M Y',strtotime($date));
      }else{
        $return="";
      }
      return $return;
    }
  }



if (! function_exists('getTime')) {
  function getTime($date){
    if($date!=''){
      $return= date('m:i:s',strtotime($date));
    }else{
      $return="";
    }
    return $return;
  }
  }

if (! function_exists('getDateTime')) {
  function getDateTime($date){
    if($date!=''){
      $return= date('l, M d Y',strtotime($date));
    }else{
      $return="";
    }
    return $return;
  }
  }

  if (! function_exists('getMonth')) {
    function getMonth($date){
      if($date!=''){
        $return= date('d M',strtotime($date));
      }else{
        $return="";
      }
      return $return;
    }
    }


    if (! function_exists('slugToId')) {
      function slugToId($table_name,$slug){       

        return DB::table($table_name)
        ->select('id')
        ->where('slug', $slug)
        ->first();

      }
  }
  



  if (! function_exists('makeSlug')) {
    function makeSlug($str){
      if(!empty($str)){
        $str = preg_replace('~[^\pL\d]+~u', '-', $str);

        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);

        $str = preg_replace('~[^-\w]+~', '', $str);

        $str = trim($str, '-');

        $str = preg_replace('~-+~', '-', $str);

        $str = strtolower($str);

        if (empty($str)) {
          return 'n-a';
        }
      }else{
        $str='n-a';;
      }
      return $str;
    }
  }


}

