<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class EventDetail extends Model
{
    //
    public $table = "event_price_variant"; protected $primaryKey = 'id';
    protected $fillable = ['price','available','variant_id','event_id'];
    public function eventinfo()
    {
        return $this
        ->belongsTo('App\Event');
    }
    public function fetcheventPrice($variant_id,$event_id){
        return DB::table('event_price_variant')
        ->select('*')
        ->where('variant_id', $variant_id)
        ->where('event_id', $event_id)
        ->first();
      }
}
