<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Categories extends Model
{
    public $table = "categories"; protected $primaryKey = 'id';
    protected $fillable = ['name','status','created_at','updated_at'];
}
