<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserModel;
use App\Package;
use App\Role;
use Redirect;
use App\Categories;

class CategoriesController extends Controller
{
    /**
     * Add Category List 
     * @method addCategories
     * @param null
     */
    public function addCategories(Request $request){
        return view('admin.category.add_categories');
    }

    
    /**
     * Save Category detail 
     * @method submitDetail
     * @param null
     */
    public function submitDetail(Request $request ,$id = null ){
        /**
         * ^ beginning of the string
         * [\pL\-]+one or more times any letter (also special characters) or a dash
         * \s? an optional space
         * [\pL\-]*zero or more times any letter (also special characters) or a dash
         * $ end of the string *  'required|string|min:3|max:30|regex:/^[\pL\s\-]+$/u|unique:users',
         */
        $validatedData = $request->validate([
            'name'   => 'required|string|min:3|max:30|regex:/^[\pL\s\-]+$/u|unique:categories'
        ]); 

        $categories = Categories::updateOrCreate(['id' => $id,],$request->except(['_token']));
               
       return redirect('admin-categories-list')->with('success','Category added successfully');
    }

     /**
     * fetch all Category detail
     * @method viewallCategories
     * @param null
     */
    public function viewallCategories(Request $request){
        $categories = Categories::all();
        return view('admin.category.view_all_categories',compact('categories'));
    }

     /**
     * Edit Categories Detail 
     * @method editCategories
     * @param id 
     */
    public function editCategories(Request $request , $id = null){  
        $categories = Categories::find($id);
        return view('admin.category.add_categories', compact('categories'));
    }

     /**
     * Delete Category Detail
     * @method deleteData
     * @param id
     */
    public function deleteData(Request $request ,$id = null){       
        $categories = Categories::destroy($id);
        return redirect('admin-categories-list')->with('success','Category  deleted successfully');
    }
     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null)
    {
        //$filmingservices = new FilmingServices;
        $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }

    }


}
