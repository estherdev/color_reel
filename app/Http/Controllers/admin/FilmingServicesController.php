<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use App\FilmingServices;

class FilmingServicesController extends Controller
{
    /**
     * Add Filming Services List 
     * @method addFilmingServices
     * @param null
     */
    public function addFilmingServices(Request $request){
       
        return view('admin.filmingservices.add_filmingservices');
    }

    
    /**
     * Save Filming Services detail 
     * @method submitDetail
     * @param null
     */
    public function submitDetail(Request $request ,$id = null ){
        /**
         * ^ beginning of the string
         * [\pL\-]+one or more times any letter (also special characters) or a dash
         * \s? an optional space
         * [\pL\-]*zero or more times any letter (also special characters) or a dash
         * $ end of the string *  'required|string|min:3|max:30|regex:/^[\pL\s\-]+$/u|unique:users',
         */

        $validatedData = $request->validate([
            'title'   => 'required|string',
            'description' =>'required'
           
        ]); 
        if($id ==''){
        $validatedData = $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]); 
        }
        $image_name = '';
        if($request->file('image') != ''){
            if(FilmingServices::find($id)){
                $old_image = FilmingServices::find($id)->image;
                $filepath = ('uploads/filmingservices/').$old_image;
                unlink($filepath);
            }
        
        $image = $request->file('image');
        $image_name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'uploads/filmingservices';
        $image->move($destinationPath, $image_name);
        }else{
            $old_image = FilmingServices::find($id)->image;
        }
        $data = $request->all();
        $image_name = !empty($image_name)  ? $image_name : $old_image;
      
        $data['image'] = $image_name;
        FilmingServices::updateOrCreate(['id' => $id],$data);
        return redirect('admin-filming-services-list')->with('success','Filming Services added successfully');


    }

     /**
     * fetch all Filming Services detail
     * @method viewallFilmingServices
     * @param null
     */
    public function viewallFilmingServices(Request $request){
        $filmingservices = FilmingServices::all();
        return view('admin.filmingservices.view_filmingservices',compact('filmingservices'));
    } 

    /**
     * Edit Filming Services Detail 
     * @method editFilmingServices
     * @param banner id 
     */
    public function editFilmingServices(Request $request , $id = null){  
        $filmingservices = FilmingServices::find($id);
        return view('admin.filmingservices.add_filmingservices', compact('filmingservices'));
    }
    /**
     * Delete Filming Services Detail
     * @method deleteData
     * @param id
     */
    public function deleteData(Request $request ,$id = null){       
        $old_image = FilmingServices::find($id)->image;
        $filepath = ('uploads/filmingservices/').$old_image;
        unlink($filepath);
        $filmingservices = FilmingServices::destroy($id);

        return redirect('admin-filming-services-list')->with('success','Filming Services  deleted successfully');
    }

     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null)
    {
        //$filmingservices = new FilmingServices;
        $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }

    }


}
