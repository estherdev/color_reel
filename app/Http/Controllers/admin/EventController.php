<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserModel;
use App\Package;
use App\Role;
use App\Event;
use Redirect;
use App\Categories;
use Excel;
use App\EventDetail;

class EventController extends Controller{
    /**
     * Add Event List 
     * @method addEvent
     * @param null
     */
    public function addEvents(Request $request){
       
        return view('admin.event.add_events');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadExcel($type)
    {
        $data = Event::get();
   
       /* return Event::create('event', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);*/
    }

    
    /**
     * Save Event detail 
     * @method submitDetail
     * @param null
     */
    public function submitDetail(Request $request ,$id = null ){
        /**
         * ^ beginning of the string
         * [\pL\-]+one or more times any letter (also special characters) or a dash
         * \s? an optional space
         * [\pL\-]*zero or more times any letter (also special characters) or a dash
         * $ end of the string *  'required|string|min:3|max:30|regex:/^[\pL\s\-]+$/u|unique:users',
         */
        //prd($request->all());
        $validatedData = $request->validate([
            'name' => 'required|string|min:3|max:30|regex:/^[\pL\s\-]+$/u',
            //'websiteurl' => 'active_url',
            //'mobile' => 'required|regex:/[0-9]{9}/',
            'email' => 'required|email',
            'category_id' => 'required ',
            
        ]); 
        
        //'price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
       
        if($id==''){
            $validatedData = $request->validate([
                'page_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ]); 
        }
        $image_name = '';
        if($request->file('page_image') != ''){
            if(Event::find($id)){
                $old_image = Event::find($id)->page_image;
                $filepath = public_path('uploads/events/').$old_image;
                unlink($filepath);                
            }        
        $image = $request->file('page_image');
        $image_name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'uploads/events';
        $image->move($destinationPath, $image_name);
        }else{
           $old_image = Event::find($id)->image;
        }        
        $data = $request->except(['variant_id','price','qty']); 
        $image_name = !empty($image_name)  ? $image_name : $old_image;
        $data['image'] = $image_name;
        $data['slug'] = makeSlug($request->input('name').'-'.time());         
        $event = Event::updateOrCreate(['id' => $id],$data);      
        $detail=array();
        
        for($i = 0 ;$i<count($request->post('price'));$i++){
            $detail = array('variant_id'=>$request->post('variant_id')[$i],
                            'price'=>$request->post('price')[$i],
                            'available' => $request->post('qty')[$i],
                            'event_id' => $event->id);
            EventDetail::updateOrCreate($detail);
        }
        //print_r($detail); exit;
        if($id){
            return redirect('admin-view-all-events-list')->with('success','Event updated Successfully');
        }else{
            return redirect('admin-view-all-events-list')->with('success','Event Creted Successfully');
        }
               
    }
    /**
     * Edit Event list
     * @method editEvents 
     * @param id 
     */
    public function editEvents (Request $request,$id = null)    {  
        $event = Event::find($id);
        
        return view('admin.event.add_events',compact('event'));
    }
 
     /**
     * fetch all Event detail
     * @method viewEvents
     * @param null
     */
    public function viewEvents(Request $request,$event_id = null){
        //$event = Event::find($event_id);
        $data=array();
        $data['event']= Event::with('fetchEventDetailBySlug')->first()->toArray();          
        $eventprice = new Event;
        $data['event']['event_variant'] = $eventprice->event_price_variant($event_id); 
        //prd($data);
        return view('admin.event.view_events',$data);
    }
 
     /**
     * fetch all Event detail
     * @method viewallEvent
     * @param null
     */
    public function viewallEvents(Request $request){
        //$slug='zumba-fitness-training-1567514217';
        $data=array();
         //$data['event']= Event::where('status',1)->with('fetchEventDetailBySlug')->first()->toArray(); 
         //$dataid=slugToId($table_name='event',$slug);         
         //$id=$dataid->id;
         //$eventprice = new Event;
         //$data['event']['event_variant'] = $eventprice->event_price_variant($id);        
         $data['event'] = Event::all();
         
         //$eventprice = new Event;
         //$data['event']= $eventprice->viewAlleventList();
         //prd($data);        
        return view('admin.event.view_all_events',$data);
    }
    /**
     * Delete Event Detail
     * @method deleteData
     * @param id
     */
    public function deleteData(Request $request ,$id = null){       
        $event = Event::destroy($id);
        return redirect('admin-view-all-events-list')->with('success','Event deleted successfully');
    }

     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null){
         $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }
    }

}
