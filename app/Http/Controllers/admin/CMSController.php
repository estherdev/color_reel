<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CMSModel;
use Redirect;
class CMSController extends Controller
{
    /**
     * Add CMS page
     * @method addPage
     * @param null
     */
    public function addPage(Request $request)
    {
        return view('admin.cms.add_page_detail');
    }
    /**
     * Submit page detail
     *  @method submitPageDetail
     *  @param null
     */
    public function submitDetail(Request $request,$id = null)
    {
        $validatedData = $request->validate([
            'page_title'       => 'required',
            'page_description' => 'required',
           
        ]); 
        
        if($id ==''){
        $validatedData = $request->validate([
            'page_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]); 
        }
        $image_name = '';
        if($request->file('page_image') != ''){
            if(CMSModel::find($id)){
                $old_image = CMSModel::find($id)->page_image;
                $filepath = public_path('uploads/').$old_image;
                unlink($filepath);
            }
        
        $image = $request->file('page_image');
        $image_name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'uploads';
        $image->move($destinationPath, $image_name);
        }else{
            $old_image = CMSModel::find($id)->page_image;
        }
        $data = $request->all();
        
       
        
        $image_name = !empty($image_name)  ? $image_name : $old_image;
        //echo "<pre>";print_r($data); exit;
        $data['page_image'] = $image_name;
        CMSModel::updateOrCreate(['page_id' => $id],$data);
        return redirect('cms/view-page')->with('success','Page added successfully');
    }
    /**
     * view cms Page
     * @method viewPage
     * @param null
     */
    public function viewPage(Request $request)
    {   
        $pages = CMSModel::all();
        return view('admin.cms.view_page_detail',compact('pages'));
    }
    /**
     * Edit Page content
     * @method EditPage
     * @param page_id
     */
    public function EditPage(Request $request, $id = null)
    {
        $page = CMSModel::find($id);
        return view('admin.cms.add_page_detail', compact('page'));
    }
    /**
     * Delete page 
     * @method deletePage
     * @param page_id
     */
    public function deletePage(Request $request, $id = null)
    {   
        $old_image = CMSModel::find($id)->page_image;
        $filepath = public_path('uploads/').$old_image;
        unlink($filepath);
        $banner = CMSModel::destroy($id);
        return redirect('cms/view-page')->with('success','Page deleted successfully');
    }

     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null){
        //$filmingservices = new FilmingServices;
        $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }
    }


}
