<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use Redirect;
class BlogController extends Controller
{
    /**
     * load add blog page 
     * @method addBlog
     * @param null
     * 
     */
    public function addBlog(Request $request)
    {
        return view('admin.addBlogDetail');
    }
    /**
     * Submit blog detail 
     * @method submitDetail
     * @param null
     */
    public function submitDetail(Request $request ,$id = null )
    {
        $validatedData = $request->validate([
            'title'   => 'required|string',
            'description' =>'required'
           
        ]); 
        if($id ==''){
        $validatedData = $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]); 
        }
        $image_name = '';
        if($request->file('image') != ''){
            if(Blog::find($id)){
                $old_image = Blog::find($id)->image;
                $filepath = ('uploads/').$old_image;
                unlink($filepath);
            }
        
        $image = $request->file('image');
        $image_name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'uploads';
        $image->move($destinationPath, $image_name);
        }else{
            $old_image = Blog::find($id)->image;
        }
        $data = $request->all();
        $image_name = !empty($image_name)  ? $image_name : $old_image;
      
        $data['image'] = $image_name;
        Blog::updateOrCreate(['blog_id' => $id],$data);
        return redirect('blog/blog-list')->with('success','Blog added successfully');
    }
    /**
     * view all blog list
     * @method viewBloglist
     * @param null
     */
    public function viewBloglist(Request $request)
    {
       $blogs = Blog::all();
       return view('admin.viewBlog',compact('blogs'));
    }
    /**
     * Edit blog detail
     * @method editBlog
     * @param blog _id
     */
    public function editBlog(Request $request ,$id = null)
    {   
        $blog = Blog::find($id);
        return view('admin.addBlogDetail',compact('blog'));
    }
    /**
     * Delete Blog Detail
     * @method deleteBlog
     * @param blog id
     */
    public function deleteBlog(Request $request ,$id = null)
    {
        $old_image = Blog::find($id)->image;
        $filepath = ('uploads/').$old_image;
        unlink($filepath);
        $blog = Blog::destroy($id);
        return redirect('blog/blog-list')->with('success','Blog  deleted successfully');
    }

     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null){
        //$filmingservices = new FilmingServices;
        $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }

    }
}
