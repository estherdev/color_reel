<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use Redirect;
class BannersController extends Controller
{
    //
    /**
     * Load Add banner image
     * @method addBanner
     * @param null
     */
    public function addBanner(Request $request){
       return view('admin.addBanner');
    }
    /**
     * Submit banner data 
     * @method submitDetail
     * @param null
     */
    public function submitDetail(Request $request ,$id = null){
        $validatedData = $request->validate([
            'heading'      => 'required',
           
        ]); 
        if($id ==''){
        $validatedData = $request->validate([
            'banner_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]); 
        }
        $image_name = '';
        if($request->file('banner_image') != ''){
        $image = $request->file('banner_image');
        $image_name = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = 'uploads';
        $image->move($destinationPath, $image_name);
        }else{
            $old_image = Banner::find($id)->banner_image;
        }
        $data = $request->all();
        
       
        
        $image_name = !empty($image_name)  ? $image_name : $old_image;
      
        $data['banner_image'] = $image_name;
        Banner::updateOrCreate(['banner_id' => $id],$data);
        return redirect('banners')->with('success','Banner added successfully');
    }
    /**
     * Get banners detail
     * @method viewBanners
     * @param null
     */
    public function viewBanners(Request $request){    
        $banners = Banner::all();
        return view('admin.view_banners',compact('banners'));
    }
    /**
     * Edit Banner Detail 
     * @method editbanner
     * @param banner id 
     */
    public function editbanner(Request $request , $id = null){  
        $banner = Banner::find($id);
        return view('admin.addBanner', compact('banner'));
    }
        /**
     * Delete Banner
     * @method deleteBanner
     * @param banner_id
     */
    public function deleteBanner(Request $request , $id = null){  
        $old_image = Banner::find($id)->banner_image;
        $filepath = public_path('uploads/').$old_image;
        unlink($filepath);
        $banner = Banner::destroy($id);
        return redirect('banners')->with('success','Banner deleted successfully');
    }
     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null){
        //$filmingservices = new FilmingServices;
        $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }

    }
}
