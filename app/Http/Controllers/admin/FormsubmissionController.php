<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FilmInfo;
use App\submitterModel\Episode;
use App\submitterModel\SubmitterInfo;
class FormsubmissionController extends Controller
{
    /**
     * load draft submission page
     * @method draftSubmission
     * @param null
     */
    public function draftSubmission(Request $request)
    {
        $detail = FilmInfo::with('episodes','submitter','language')->orderBy('flim_id', 'desc')->get()->toArray();
        return view('admin.draft_submission',compact('detail'));
    }
    /**
     * Draft submission detail of single user
     * @method draftSubmissionDetail
     * @param film id  
     */
    public function draftSubmissionDetail(Request $request,$film_id = null)
    {  
        $detail = FilmInfo::with('episodes','submitter','language','distributer','ownerships','subtitle','sociallink')->find($film_id);
        return view('admin.draft_submission_detail',compact('detail'));
    }
}
