<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Countries;
use Redirect;
class CountriesController extends Controller
{
    /**
     * fetch all Countries detail
     * @method viewallCountries
     * @param null
     */
    public function viewallCountries(Request $request){
        $countries = Countries::all();

        return view('admin.countries.view_all_countries',compact('countries'));
    }

      /**
     * Delete Countries Detail
     * @method deleteData
     * @param id
     */
    public function deleteData(Request $request ,$id = null){       
        $countries = Countries::destroy($id);
        return redirect('admin-view-all-countries-list')->with('success','Countries  deleted successfully');
    }
     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null)
    {
         $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }

    }


}
