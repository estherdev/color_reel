<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FaqModel;
use Redirect;
class FaqController extends Controller
{



    /**
     * Add Faq
     * @method addFaq
     * @param null
     */
    function addFaq(){
        return view('admin.faq.add_faq');
    }


     /**
     * Submit page detail
     *  @method submitPageDetail
     *  @param null
     */
    public function submitDetail(Request $request,$id = null)
    {
        $validatedData = $request->validate([
            'que' => 'required',
            'ans' => 'required',           
        ]); 
        
        $data = $request->all();
        $data['ans']=strip_tags($request->post('ans'));
        FaqModel::updateOrCreate(['id' => $id],$data);
        return redirect('admin-view-all-faq-list')->with('success','Faq added successfully');
    }



    /**
     * view All Faq
     * @method viewallFaq
     * @param null
     */
    public function viewallFaq(Request $request)
    {   
        $faq = FaqModel::all();
        //prd($faq);
        return view('admin.faq.view_all_faq',compact('faq'));
    }
    /**
     * Edit Faq content
     * @method EditFaq
     * @param id
     */
    public function EditFaq(Request $request, $id = null)
    {
        $faq = FaqModel::find($id);
        //prd($faq);
        return view('admin.faq.add_faq', compact('faq'));
    }
    /**
     * Delete Faq 
     * @method deleteFaq
     * @param id
     */
    public function deleteData(Request $request, $id = null)
    {  
        $banner = FaqModel::destroy($id);
       
        return redirect('admin-view-all-faq-list')->with('success','Faq deleted successfully');
    }

     /**
     * Modify status
     * @method modifyStatus
     * @param $table_name,$column_name,$primary_key
     */
    public function modifyStatus(Request $request,$table_name = null,$column_name = null,$primary_key = null){
        //$filmingservices = new FilmingServices;
        $changed = updatemoduleStatus($table_name,$column_name,$primary_key);
        if($changed){
            return redirect::back()->with('success','Status updated Successfully');
        }
        else{
            return redirect::back()->with('error','Something went wrong');
        }
    }

    
}
