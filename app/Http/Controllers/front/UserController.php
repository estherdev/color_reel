<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
     /**
     *User Registration
     * @method userRegister
     * @param null
     */
    public function userRegister(Request $request){
        return view('frontend.pages.signup');
    }
}
