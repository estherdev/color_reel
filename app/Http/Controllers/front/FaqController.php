<?php

namespace App\Http\Controllers\front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FaqModel;
use Session;
class FaqController extends Controller
{


    
     /**
     * fetch all Faq detail
     * @method viewFaq
     * @param faq_id
     */
    public function viewFaq(Request $request){
        $data=array();            
        $faq = new FaqModel;
        $data['faq'] = $faq->fetchdata();        
       // prd($data);
        return view('frontend.pages.faq',$data);
    }
 

}
