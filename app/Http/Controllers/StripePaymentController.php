<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Session;
use Stripe;
use App\FilmInfo;
use Redirect;
class StripePaymentController extends Controller
{
    //
    public function __construct()
    {
        /** PayPal api context **/
                /*$paypal_conf = \Config::get('paypal');
                $this->_api_context = new ApiContext(new OAuthTokenCredential(
                    $paypal_conf['client_id'],
                    $paypal_conf['secret'])
                );
                $this->_api_context->setConfig($paypal_conf['settings']);*/
    }



    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {   
        if(Session::get('filmid')>0){
            
            return view('stripe');
        }
        else{
           return Redirect::to('/'); 
        }
        
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
    */
    public function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $response = Stripe\Charge::create ([
                "amount" => 100 * draftprice()->draft_amount,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Draft Submission Payment" 
        ]);
         if($response){
         FilmInfo::where('flim_id',Session::get('filmid'))
                 ->update(array('payment_status' => $response->status,
                                'transaction_id' => $response->balance_transaction,
                                 'amount' => ($response->amount)/100)
                                );
        Session::flash('success', 'Payment successful!');
          
        return Redirect::to('/thankyou');
         }
    }
  
}
