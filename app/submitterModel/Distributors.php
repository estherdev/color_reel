<?php

namespace App\submitterModel;

use Illuminate\Database\Eloquent\Model;

class Distributors extends Model
{
    public $table = "distributors"; protected $primaryKey = 'distributer_id';
    protected $fillable = ['film_id','distributor_name','distributor_email','mailing_address','phone'];
    public function filmdetail()
    {
        return $this
            ->belongsTo('App\FilmInfo');
           
    }
}
