<?php

namespace App\submitterModel;

use Illuminate\Database\Eloquent\Model;

class FilmLanguage extends Model
{
    public $table = "film_language"; protected $primaryKey = 'lang_id';
    protected $fillable = ['film_id_fk','language'];
    public function filmdetail()
    {
        return $this
            ->belongsTo('App\FilmInfo');
           
    }
}
