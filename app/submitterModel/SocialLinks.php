<?php

namespace App\submitterModel;

use Illuminate\Database\Eloquent\Model;

class SocialLinks extends Model
{
    public $table = "social_links"; protected $primaryKey = 'id';
    protected $fillable = ['social_media_name','social_links','film_id_fk'];
    public function filmdetail()
    {
        return $this
            ->belongsTo('App\FilmInfo');
           
    }
}
