<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
    public $table = "gallery"; protected $primaryKey = 'gallery_id';
    protected $fillable = ['title','file_type','upload_type','file_name'];
}
