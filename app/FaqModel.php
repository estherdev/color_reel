<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class FaqModel extends Model
{
    public $table = "faq"; protected $primaryKey = 'id';
    protected $fillable = ['que','ans','created_at','updated_at'];

    public function fetchdata(){
      return DB::table('faq')
                       ->select('*')
                       ->where('status',1)
                       ->orderBy('id','DESC')
                       ->get()->toArray();
    }
}
