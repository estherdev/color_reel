<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Countries extends Model
{
    public $table = "countries"; protected $primaryKey = 'id';
    protected $fillable = ['name','sortname','phonecode','status'];
}
