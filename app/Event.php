<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Event extends Model
{
    public $table = "event"; protected $primaryKey = 'id';
    protected $fillable = ['name','category_id','slug','price','mobile','image','address','description','websiteurl','email','meta_title','meta_keywords','meta_description','created_at','updated_at','event_start_date','event_end_date','event_start_time','event_end_time'];

    public function fetchdata(){
      return DB::table('event')
                       ->select('*')
                       ->where('status', 1)
                       ->orderBy('id','DESC')
                       ->get();
    }

    public function fetchEventDetailBySlug(){

       return $this
        ->hasMany('App\EventDetail','event_id');

        // return $event= DB::table('event')
        //                 ->select('*')
        //                 ->where('status', 1)
        //                 ->where('slug', $slug)
        //                 ->orderBy('id','DESC')
        //                 ->first();

      }

      public function event_price_variant($id){
      return DB::table('event_price_variant as epv')
                      ->join('event_variant', 'epv.variant_id', '=', 'event_variant.id')
                      ->join('event as e', 'epv.event_id', '=', 'e.id')
                      ->where('epv.event_id', $id)
                       ->select('event_variant.*','epv.id as event_price_var_id','epv.price','epv.available','epv.variant_id','epv.event_id' )
                      
                      ->get()->toArray();
      }

      /**
       * Admin View All Event List
       * @method viewAlleventList
       * @param null
       */
      public function viewAlleventList(){
        return DB::table('event as e')
                        ->join('event_price_variant as epv', 'epv.event_id', '=', 'e.id')
                        ->join('event_variant as ev', 'epv.variant_id', '=', 'ev.id')
                        ->select('e.*','epv.id as event_price_var_id','epv.price as epv_price ','epv.available as epv_available','epv.variant_id as epv_variant_id','epv.event_id as epv_event_id')                       
                        ->get();
        }
  

     
}
