<?php 
return [
    'siteTitle' => 'ColorReel',
    'seeallevent' =>'See All Events',
    'seeallnews' =>'See All News',
    'wtla'=>'Watch the live awardshow',
    'event' =>'Events',
    'free' =>'Free',
    'newsfeed' =>'News Feed',
    'newsletter'=>'Newsletter',
    'connectwith'=>'Connect with',
	'pagination' => 10,
	'nodata' => 'No data found'
];

?>