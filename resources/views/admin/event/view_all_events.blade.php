@extends('admin.mainlayout')
@section('title', 'Admin| Event')
@section('content')
@section('heading','Event')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Event')  

<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href="{{url('admin-manage-events')}}" class="btn btn-primary float-right mento" >Add Event</a>
   <a href="{{ url('downloadExcel/xls') }}"><button class="btn btn-success">Download Excel xls</button></a>
	<a href="{{ url('downloadExcel/xlsx') }}"><button class="btn btn-success">Download Excel xlsx</button></a>
	<a href="{{ url('downloadExcel/csv') }}"><button class="btn btn-success">Download CSV</button></a>
 
      <h5 class="card-title">Event List</h5>
     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered table-hover">
         <?php //echo "<pre>";print_r($packages);exit;?>
            <thead>
               <tr class="text-nowrap">
                  <th>S No.</th>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Url</th>
                  <!--<th>Email</th>
                   <th>Date</th>
                  <th>Time</th> -->
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
              
            @forelse($event as $row)
            <tr class="text-center text-nowrap" >
              <td>{{$loop->iteration}}</td>
              <td>{{ucfirst($row->name)}}</td>
              <td> <img class="rounded" src="{{ asset('uploads/events/'.$row->image)}}" width="50" height="50"></td>
              <td> <a href="{{$row->websiteurl}}" target="_blank">{{$row->websiteurl}}</a></td>
           <!--       <td>{{$row->email}}</td>
           <td>{{$row->event_start_date.' To '.$row->event_end_date }} </td>
              <td>{{$row->event_start_time.' To '.$row->event_end_time }} </td> -->
              @if($row->status == 1)              
                <td class="text-center"><a href="{{url('admin-modify-events-status/event/id/'.$row->id)}}"> <i class="fa fa-unlock"></i></a></td>
              @else
                <td class="text-center"><a href="{{url('admin-modify-events-status/event/id/'.$row->id)}}"> <i class = "fa fa-lock"></i></a></td>
              @endif
              <td>
              	<a href="{{url('admin-view-event/'.$row->id)}}"><i class="fa fa-eye"></i></a> | 
              	<a href="{{url('admin-edit-events/'.$row->id)}}"><i class="fa fa-edit"></i></a> | 
              	<a href="{{url('admin-delete-events/'.$row->id)}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr><td colspan ="11" class = "text-center">{{ config('global.nodata') }}</td><tr>
            @endforelse 

            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection