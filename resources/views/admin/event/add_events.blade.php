@extends('admin.mainlayout')
@section('title', 'Admin add Event')
@section('content')
@section('heading','Event')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','add Event')  

<div class="row">
   <div class="col-md-12">
      <div class="card">
      <?php //echo "<pre/>";print_r($event)?>
         @if(@$event !='')
         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-events-detail/'.$event->id)}}" enctype = "multipart/form-data">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-events-detail/')}}" enctype = "multipart/form-data">
         @endif         
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Event Detail</h4>
               <div class="form-group row adevent">
                  <label for="fname" class="col-sm-2 text-right control-label col-form-label">Name <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" required value = "{{old('name') != '' ? old('name') : @$event->name }}">
                     <span style="color:red">{{ $errors->first('name') }}</span>
                  </div>
                    <label class="col-sm-2 text-right control-label col-form-label">Image <span class="mandatory">*</span></label>
                   <div class="col-sm-4">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name ="page_image" id="validatedCustomFile">
                           <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                           <span style = "color:red">{{ $errors->first('image') }}</span>
                        </div>
                  </div>
            
                  <label for="websiteurl" class="col-sm-2 text-right control-label col-form-label">Website url <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="text" name="websiteurl" class="form-control" id="websiteurl" placeholder="Enter Website url" required value = "{{old('websiteurl') != '' ? old('websiteurl') : @$event->websiteurl }}">
                     <span style="color:red">{{ $errors->first('websiteurl') }}</span>
                  </div>
                  <label for="category" class="col-sm-2 text-right control-label col-form-label">Category <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <?php  $category=category(); ?>
                     <select name="category_id" class="category_id form-control" id="category_id" >
                        <option value="">Select Category</option>
                        @foreach($category as $key=> $category)
                         <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{ucfirst($category->name)}}</option>
                        @endforeach
                     </select>
                     <span style="color:red">{{ $errors->first('category_id') }}</span>
                  </div>
                  <label for="mobile" class="col-sm-2 text-right control-label col-form-label">Phone  <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="text" name="mobile" class="form-control" id="mobile" placeholder="Enter Phone" required value = "{{old('mobile') != '' ? old('mobile') : @$event->mobile }}">
                     <span style="color:red">{{ $errors->first('mobile') }}</span>
                  </div>
                  <label for="email" class="col-sm-2 text-right control-label col-form-label">Email  <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="email" name="email" class="form-control" id="email" placeholder="Enter Email" required value = "{{old('email') != '' ? old('email') : @$event->email }}">
                     <span style="color:red">{{ $errors->first('email') }}</span>
                  </div> 

                  <label for="description" class="col-sm-2 text-right control-label col-form-label">Start Date <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="date" name="event_start_date" class="form-control" id="event_start_date" placeholder="Enter start date"  value="{{old('event_start_date') != '' ? old('event_start_date') : @$event->event_start_date }}">
                     <span style="color:red">{{ $errors->first('event_start_date') }}</span>
                  </div>
                  
                  <label for="description" class="col-sm-2 text-right control-label col-form-label">End Date  <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="date" name="event_end_date" class="form-control" id="event_end_date" placeholder="Enter end date" value="{{old('event_end_date') != '' ? old('event_end_date') : @$event->event_end_date }}" >
                     <span style="color:red">{{ $errors->first('event_end_date') }}</span>
                  </div> 

                  <label for="description" class="col-sm-2 text-right control-label col-form-label">Start Time <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="text" name="event_start_time" class="form-control" id="event_start_time" placeholder="Enter start date"  value="{{old('event_start_time') != '' ? old('event_start_time') : @$event->event_start_time }}">
                     <span style="color:red">{{ $errors->first('event_start_time') }}</span>
                  </div>
                  
                  <label for="description" class="col-sm-2 text-right control-label col-form-label">End Time  <span class="mandatory">*</span></label>
                  <div class="col-sm-4">
                     <input type="text" name="event_end_time" class="form-control" id="event_end_time" placeholder="Enter end date" value="{{old('event_end_time') != '' ? old('event_end_time') : @$event->event_end_time }}" >
                     <span style="color:red">{{ $errors->first('event_end_time') }}</span>
                  </div>

                  <label for="address" class="col-sm-2 text-right control-label col-form-label">Address  <span class="mandatory">*</span></label>
                  <div class="col-sm-10">
                     <textarea type="text" name="address" class="form-control" id="address" placeholder="Enter Address">{{old('address') != '' ? old('address') : @$event->address }} </textarea>
                     <span style="color:red">{{ $errors->first('address') }}</span>
                  </div>   
                                 
                  
                  <label for="description" class="col-sm-2 text-right control-label col-form-label">Description  <span class="mandatory">*</span></label>
                  <div class="col-sm-10">
                     <textarea type="text" name="description" class="form-control" id="editor1" placeholder="Enter description" >{{old('description') != '' ? old('description') : @$event->description }}</textarea>
                     <span style="color:red">{{ $errors->first('description') }}</span>
                  </div>

               </div>
               <h4 class="card-title">Price</h4>
               <div class="form-group row adevent">
                     
                   <label for="gold_price" class="col-sm-2 text-right control-label col-form-label">Gold</label>
                  <div class="col-sm-5">
                     <input type = "hidden" value = "1" name = "variant_id[]">
                     <input type="text" name="price[]" class="form-control" id="gold_price" placeholder="Enter Price"  value = "{{old('gold_price') != '' ? old('gold_price') : @$event->gold_price }}">
                  </div> 
                  <div class="col-sm-5">
                    <input type="text" name="qty[]" class="form-control" id="gold_qty" placeholder="Enter Qty Available"  value = "{{old('gold_qty') != '' ? old('gold_qty') : @$event->gold_qty }}">
                 </div> 

                  <label for="meta_keywords" class="col-sm-2 text-right control-label col-form-label">Platinum</label>
                  <div class="col-sm-5">
                    <input type = "hidden" value = "2"  name = "variant_id[]">
                    <input type="text" name="price[]" class="form-control" id="platinum_price" placeholder="Enter Price"  value = "{{old('platinum_price') != '' ? old('platinum_price') : @$event->platinum_price }}">
                 </div> 
                 <div class="col-sm-5">
                  <input type="text" name="qty[]" class="form-control" id="platinum_qty" placeholder="Enter Qty Available"  value = "{{old('platinum_qty') != '' ? old('platinum_qty') : @$event->platinum_qty }}">
               </div> 

                  <label for="meta_description" class="col-sm-2 text-right control-label col-form-label">Standard</label>
                  <div class="col-sm-5">
                      <input type = "hidden" value = "3" name = "variant_id[]">
                    <input type="text" name="price[]" class="form-control" id="standard_price" placeholder="Enter Price"  value = "{{old('standard_price') != '' ? old('standard_price') : @$event->standard_price }}">
                 </div> 
                 <div class="col-sm-5">
                  <input type="text" name="qty[]" class="form-control" id="standard_qty" placeholder="Enter Qty Available"  value = "{{old('standard_qty') != '' ? old('standard_qty') : @$event->standard_qty }}">
               </div> 
               </div>
               <h4 class="card-title">Seo Detail</h4>
               <div class="form-group row adevent">
                     
                   <label for="meta_title" class="col-sm-2 text-right control-label col-form-label">Title</label>
                  <div class="col-sm-10">
                     <input type="text" name="meta_title" class="form-control onlyinteger" id="meta_title" placeholder="Enter Meta Title"  value = "{{old('meta_title') != '' ? old('meta_title') : @$event->meta_title }}">
                  </div> 

                  <label for="meta_keywords" class="col-sm-2 text-right control-label col-form-label">Keywords</label>
                  <div class="col-sm-10">
                    <textarea class = "form-control" id="meta_keywords" name="meta_keywords" placeholder = "Enter Meta keywords">{{old('meta_keywords') != '' ? old('meta_keywords') : @$event->meta_keywords }}</textarea>
                     
                  </div>

                  <label for="meta_description" class="col-sm-2 text-right control-label col-form-label">Description</label>
                  <div class="col-sm-10">
                     <textarea class = "form-control" id="meta_description" name="meta_description" placeholder = "Enter Meta keywords">{{old('meta_description') != '' ? old('meta_description') : @$event->meta_description }}</textarea>
                     
                  </div>
               </div>
            </div>
            <div class="border-top">
               <div class="card-body text-center">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <!-- <a herf="{{url('/admin-manage-events')}}" class="btn btn-primary">Back</a> -->
               </div>
            </div>
         </form>
      </div>      
   </div>
</div>
@endsection

