@extends('admin.mainlayout')
@section('title', 'Admin Draft Submission')
@section('content')
@section('heading','Draft Submission')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Draft Submission')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
      <h5 class="card-title">Submission List</h5>
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered">
         <?php //echo "<pre>";print_r($detail);?>
            <thead>
               <tr>
                  <th>Sr No.</th>
                  <th>Submitter Name</th>
                  <th>Email</th>
                  <th>Flim Name</th>
                  <th>Amount</th>
                  <th>Payment Status</th>
                  <th>Submission Date</th>
                 
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
             @forelse($detail as $row)
              <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$row['submitter']['name']}}</td>
              <td>{{$row['submitter']['email']}}</td>
              <td>{{$row['project_title']}}</td>
              <td>{{$row['amount']}}</td>
              <td>{{$row['payment_status']}}</td>
              <td>{{date( "d-M-Y", strtotime($row['created_at']))}}</td>
              <!--<td><a href = "{{url('update-status/film_info/'.$row['flim_id'])}}">{{$row['status'] == 1 ? 'Active':'Inactive'}}</a></td>-->
              <td><a href = "{{ url('user-draft-submission-detail/'.$row['flim_id']) }}"><i class="m-r-10 mdi mdi-eye"></i></a></td>
              </tr>
              
              @empty
              <tr><td colspan = "8" class = "text-center">No data found</td><tr>
              @endforelse 
            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection