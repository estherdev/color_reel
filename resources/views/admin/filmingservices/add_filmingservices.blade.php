@extends('admin.mainlayout')
@section('title', 'Admin | Filming Services')
@section('content')
@section('heading',' Filming Services')  
@section('breadcrumb_menu',' Filming Services')  
@section('breadcrumb_submenu','Filming Services')  
<div class="row">
   <div class="col-md-12">
      <div class="card">
          @if(@$filmingservices !='')
         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-filming-services-detail/'.$filmingservices->id)}}" enctype = "multipart/form-data">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-filming-services-detail')}}" enctype = "multipart/form-data">
         @endif
         
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Filming Services Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title</label>
                  <div class="col-sm-9">
                     <input type="text" name = "title" class="form-control" id="name" placeholder="Enter Filming Services Heading" required value = "{{old('title') != '' ? old('title') : @$filmingservices->title }}">
                     <span style = "color:red">{{ $errors->first('title') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Description</label>
                  <div class="col-sm-9">
                  <textarea class = "form-control" id="editor1" name = "description" placeholder = "Enter Filming Services Description" required>{{old('description') != '' ? old('description') : @$filmingservices->description }}</textarea>
                  <span style = "color:red">{{ $errors->first('description') }}</span> 
                  </div>
               </div>
               
               <div class="form-group row">
                  <label class="col-md-3 text-right control-label col-form-label">Image</label>
                  <div class="col-md-9">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name = "image" id="validatedCustomFile">
                           <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                           <span style = "color:red">{{ $errors->first('image') }}</span>
                        </div>
                  </div>
               </div>
               
              
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
      
      
   </div>
</div>
@endsection