@extends('admin.mainlayout')
@section('title', 'Admin|Filming Services')
@section('content')
@section('heading','Filming Services')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Filming Services')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href= "{{url('admin-manage-filming-services-list')}}" class = "btn btn-primary float-right mento" >Add New</a>
      <h5 class="card-title">Filming Services List</h5>
     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered table-hover ">
             <thead>
               <tr>
                  <th>Sr No.</th>
                  <th>Heading</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody> 
            @forelse($filmingservices as $row)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{ucfirst(strtolower($row['title']))}}</td>
             
              <td><img src = "{{asset('uploads/filmingservices').'/'.$row['image']}}" class = "img-thumbnail" width = "50px" height = "50px"> </td>
              @if($row['status'] == 1)
              
              <td class="text-center"><a href = "{{url('admin-modify-filming-services-status/filming_services/id/'.$row['id'])}}"> <i class="fa fa-unlock"></i></a></td>
            @else
                <td class="text-center"><a href = "{{url('admin-modify-filming-services-status/filming_services/id/'.$row['id'])}}"> <i class = "fa fa-lock"></i></a></td>
            @endif
              <td><a href = "{{url('admin-edit-filming-services/'.$row['id'])}}"><i class = "fa fa-edit"></i></a> | 
                  <a href = "{{url('admin-delete-filming-services/'.$row['id'])}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr><td colspan = "6" class = "text-center">No data found</td><tr>
            @endforelse 

            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection