@extends('admin.mainlayout')
@section('title', 'Admin|Gallery')
@section('content')
@section('heading','Gallery')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','banners')  
<?php //print_r($files);?>
<a href= "{{url('add-gallery')}}" class = "btn btn-primary float-right mento" >Add New</a>
<hr>
<div class="row el-element-overlay"> 

   @forelse ($files as $file)
   <div class="col-lg-3 col-md-6">
      <div class="card">
         <div class="el-card-item">
            <div class="el-card-avatar el-overlay-1">
             @if ($file->file_type == 'image')
              <img src="{{ asset('gallery/'.$file->file_name) }}" alt="color Reel" />
              <div class="el-overlay">
                  <ul class="list-style-none el-info">
                     <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="{{ asset('gallery/'.$file->file_name) }}"><i class="mdi mdi-magnify-plus"></i></a></li>
                     <li class="el-item"><a class="btn default btn-outline el-link" href="javascript:void(0);"><i class="mdi mdi-link"></i></a></li>
                  </ul>
               </div>
             
              @elseif($file->upload_type == 'url')
              <?php echo $file->file_name;?>
             
             
              @else 
              <video width="202" height="135" controls>
                      <source src="{{ asset('gallery/'.$file->file_name) }}" type="video/mp4">
                      <source src="{{ asset('gallery/'.$file->file_name) }}" type="video/ogg">
                      Your browser does not support the video tag.
                </video>
                
              @endif      
               
            </div>
            <div class="el-card-content">
               <h4 class="m-b-0">{{$file->title}}</h4>
            </div>
         </div>
      </div>
   </div>
   @empty 
   <p>No data found</p>
   @endforelse
</div>
@endsection