@extends('admin.mainlayout')
@section('title', 'Admin add movies Category')
@section('content')
@section('heading','Movies category')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Add movies category')  
<div class="row">
   <div class="col-md-12">
      <div class="card">
      <?php //echo "<pre/>";print_r($categories)?>
         @if(@$categories !='')
         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-categories-detail/'.$categories->id)}}">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-categories-detail')}}">
         @endif
         
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Movies Category Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                  <div class="col-sm-8">
                     <input type="text" name = "name" class="form-control" id="name" placeholder="Enter Category Name" required value = "{{old('name') != '' ? old('name') : @$categories->name }}">
                     <span style = "color:red">{{ $errors->first('name') }}</span>
                  </div>
               </div>
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
      
      
   </div>
</div>
@endsection