@extends('admin.mainlayout')
@section('title', 'Admin|blogs')
@section('content')
@section('heading','blogs')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','blogs')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href= "{{url('blog/add-blog')}}" class = "btn btn-primary float-right mento" >Add New</a>
      <h5 class="card-title">blogs List</h5>
     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered">
         <?php //echo "<pre>";print_r($packages);exit;?>
            <thead>
               <tr>
                  <th>Sr No.</th>
                  <th>Heading</th>
                  <th>Image</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
            @forelse($blogs as $row)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$row['title']}}</td>
             
              <td><img src = "{{asset('uploads').'/'.$row['image']}}" class = "img-thumbnail" width = "100px" height = "100px"> </td>
             
             <!--  <td><a href = "{{url('modify-status/blog_list/'.$row['blog_id'])}}">{{$row['status'] == 1 ? 'Active':'Inactive'}}</a></td> -->
              @if($row['status'] == 1)
              
              <td class="text-center"><a href = "{{url('admin-modify-blog-status/blog_list/blog_id/'.$row['blog_id'])}}"> <i class="fa fa-unlock"></i></a></td>
            @else
                <td class="text-center"><a href = "{{url('admin-modify-blog-status/blog_list/blog_id/'.$row['blog_id'])}}"> <i class = "fa fa-lock"></i></a></td>
            @endif
              <td><a href = "{{url('blog/edit-blog/'.$row['blog_id'])}}"><i class = "fa fa-edit"></i></a> | 
                  <a href = "{{url('delete-blog/'.$row['blog_id'])}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr><td colspan = "6" class = "text-center">No data found</td><tr>
            @endforelse 

            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection