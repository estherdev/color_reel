@extends('admin.mainlayout')
@section('title', 'Admin packages')
@section('content')
@section('heading','Packages')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Packages')  
<div class="row">
   <div class="col-md-12">
      <div class="card">
         @if(@$package !='')
         <form class="form-horizontal" method = "post" action = "{{url('/package-submit-detail/'.$package->package_id)}}">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/package-submit-detail')}}">
         @endif
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Package Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                  <div class="col-sm-9">
                     <input type="text" name = "package_name" class="form-control" id="fname" placeholder="Package Name Here" required value = "{{old('package_name') != '' ? old('package_name') : @$package->package_name }}">
                     <span style = "color:red">{{ $errors->first('package_name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Price</label>
                  <div class="col-sm-9">
                     <input type="text" name  = "package_price" class="form-control" id="lname" placeholder="Package Price Here" required value = "{{old('package_price') != '' ? old('package_price') : @$package->package_price }}">
                     <span style = "color:red">{{ $errors->first('package_price') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Duration</label>
                  <div class="col-sm-9">
                 
                        <div class="input-group">
                            <input type="text" name = "duration_value" class="form-control" placeholder="Enter Value" aria-label="Recipient 's username" aria-describedby="basic-addon2" value = "{{old('duration_value') != '' ? old('duration_value') : @$package->duration_value }}" required>
                            <div class="input-group-append">
                            <select class = "form-control" name = "duration_time" required value = "@$package->duration_time " >
                                <option value = "days" {{@$package->duration_time == 'days' ? 'selected' : ''}}> Day </option>
                                <option value = "months" {{@$package->duration_time == 'months' ? 'selected' : ''}}> Month </option>
                                <option value = "years" {{@$package->duration_time == 'years' ? 'selected' : ''}}> Year </option>
                            </select>
                            </div>
                        </div>
                                   
                     <span style = "color:red">{{ $errors->first('duration_value') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Detail</label>
                  <div class="col-sm-9">
                     <textarea class="form-control"  name = "detail" required> {{old('detail') != '' ? old('detail') : @$package->detail }}</textarea>
                     <span style = "color:red">{{ $errors->first('detail') }}</span>
                  </div>
               </div>
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
      
      
   </div>
</div>
@endsection