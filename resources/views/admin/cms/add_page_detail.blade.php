@extends('admin.mainlayout')
@section('title', 'Admin | add Page')
@section('content')
@section('heading','CMS')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','add-Page') 
<div class="row">
   <div class="col-md-12">
      <div class="card">
      <?php //print_r($page)?>
         @if(@$page !='')
         <form class="form-horizontal" method = "post" action = "{{url('/submit-page-detail/'.$page->page_id)}}" enctype = "multipart/form-data">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/submit-page-detail')}}" enctype = "multipart/form-data">
         @endif
         
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Page Title</label>
                  <div class="col-sm-9">
                     <input type="text" name = "page_title" class="form-control" id="fname" placeholder="Enter Page Title" required value = "{{old('page_title') != '' ? old('page_title') : @$page->page_title }}">
                     <span style = "color:red">{{ $errors->first('page_title') }}</span>
                  </div>
               </div>
              
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Page Description</label>
                  <div class="col-sm-9">
                     <!-- Create the editor container -->
                      <textarea class = "form-control" id="editor1" name = "page_description">{{old('page_description') != '' ? old('page_description') : @$page->page_description }} </textarea>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-md-3 text-right control-label col-form-label">Page Image</label>
                  <div class="col-md-9">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name = "page_image" id="validatedCustomFile">
                           <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                           <span style = "color:red">{{ $errors->first('banner_image') }}</span>
                        </div>
                  </div>
               </div>
               <hr>
               <h4 class="card-title">Seo Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title</label>
                  <div class="col-sm-9">
                     <input type="text" name = "meta_title" class="form-control" id="fname" placeholder="Enter Meta Title"  value = "{{old('meta_title') != '' ? old('meta_title') : @$page->meta_title }}">
                  </div>
               </div>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Keywords</label>
                  <div class="col-sm-9">
                    <textarea class = "form-control" id="editor1" name = "meta_keywords" placeholder = "Enter Meta keywords">{{old('meta_keywords') != '' ? old('meta_keywords') : @$page->meta_keywords }}</textarea>
                     
                  </div>
               </div>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Description</label>
                  <div class="col-sm-9">
                     <textarea class = "form-control" id="editor1" name = "meta_description" placeholder = "Enter Meta keywords">{{old('meta_description') != '' ? old('meta_description') : @$page->meta_description }}</textarea>
                     
                  </div>
               </div>
              
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
@endsection