@extends('admin.mainlayout')
@section('title', 'Admin Pages')
@section('content')
@section('heading','Pages')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Pages')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href= "{{url('cms/add-page')}}" class = "btn btn-primary float-right mento" >Add page</a>
      <h5 class="card-title">pages List</h5>
     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered">
         <?php //echo "<pre>";print_r($pages);exit;?>
            <thead>
               <tr>
                  <th>Sr No.</th>
                  <th>Page</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
            @forelse($pages as $row)
            <tr>
              <td>{{$loop->iteration}}</td>
              <td>{{$row['page_title']}}</td>
              
              <!-- <td><a href = "{{url('modify-status/page_list/'.$row['page_id'])}}">{{$row['status'] == 1 ? 'Active':'Inactive'}}</a></td> -->
             
               @if($row['status'] == 1)              
              <td class="text-center"><a href = "{{url('admin-modify-cms-status/page_list/page_id/'.$row['page_id'])}}"> <i class="fa fa-unlock"></i></a></td>
            @else
                <td class="text-center"><a href = "{{url('admin-modify-cms-status/page_list/page_id/'.$row['page_id'])}}"> <i class = "fa fa-lock"></i></a></td>
            @endif
              <td><a href = "{{url('cms/edit-page/'.$row['page_id'])}}"><i class = "fa fa-edit"></i></a> | 
                  <a href = "{{url('cms/delete-page/'.$row['page_id'])}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr><td colspan= "3">No data found<td><tr>
            @endforelse 

            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection