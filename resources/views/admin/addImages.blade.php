@extends('admin.mainlayout')
@section('title', 'Admin | add Gallery')
@section('content')
@section('heading','Gallery')  
@section('breadcrumb_menu','Gallery')  
@section('breadcrumb_submenu','add-Gallery')  
<div class="row">
   <div class="col-md-12">
      <div class="card">
      <?php //print_r($user)?>
      <div class="alert" id="message" style="display: none"></div>
         @if(@$blog !='')
         <form class="form-horizontal" method = "post" id = "upload_form" action = "{{url('/save-images/'.$blog->blog_id)}}" enctype = "multipart/form-data">
         @else
         <form class="form-horizontal" method = "post" id = "upload_form" action = "{{url('/save-images')}}" enctype = "multipart/form-data">
         @endif
         
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Title</label>
                  <div class="col-sm-9">
                     <input type="text" name = "title" class="form-control" id="fname" placeholder="Enter Image Title"  value = "{{old('title') != '' ? old('title') : @$blog->title }}">
                     <span style = "color:red">{{ $errors->first('title') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-md-3 text-right control-label col-form-label">Upload Type</label>
                  <div class="col-md-9" id = "uploadtype">
                        <div class="custom-control custom-radio" >
                           <input type="radio" class="custom-control-input" id="customControlValidation1" name="file_type" value = "image" required >
                           <label class="custom-control-label" for="customControlValidation1">Image</label>
                        </div>
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" id="customControlValidation2" name="file_type" value = "video" >
                           <label class="custom-control-label" for="customControlValidation2">Video</label>
                        </div>
                        
                  </div>
               </div>
               <div class="form-group row" id = "videotag" style = "display:none">
                  <label class="col-md-3 text-right control-label col-form-label">Browse</label>
                  <div class="col-md-9" id = "browsetype">
                        <div class="custom-control custom-radio" >
                           <input type="radio" class="custom-control-input" id="customControlValidation11" name="upload_type" value = "url">
                           <label class="custom-control-label" for="customControlValidation11">URL</label>
                        </div>
                        <div class="custom-control custom-radio">
                           <input type="radio" class="custom-control-input" id="customControlValidation22" name="upload_type" value="custom">
                           <label class="custom-control-label" for="customControlValidation22">Custom</label>
                        </div>
                        
                  </div>
               </div>
               <div class="form-group row" id = "imagetag" style = "display:none">
                  <label class="col-md-3 text-right control-label col-form-label">Browse Image</label>
                  <div class="col-md-9">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name = "file_name" id="validatedCustomFile">
                           <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                           <span style = "color:red">{{ $errors->first('file_name') }}</span>
                        </div>
                  </div>
               </div>
               <div class="form-group row" id = "customvideo" style = "display:none">
                  <label class="col-md-3 text-right control-label col-form-label">Broser Video</label>
                  <div class="col-md-9">
                        <div class="custom-file">
                           <input type="file" class="custom-file-input" name = "video" id="validatedCustomvideoFile">
                           <label class="custom-file-label" for="validatedCustomvideoFile">Choose file...</label>
                           <span style = "color:red">{{ $errors->first('video') }}</span>
                        </div>
                  </div>
               </div>
               <div class="form-group row" id = "urltag" style = "display:none">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">URL</label>
                  <div class="col-sm-9">
                     <input type="text" name = "url" class="form-control" id="urls" placeholder="Enter video URL" value = "">
                     <span style = "color:red">{{ $errors->first('url') }}</span>
                  </div>
               </div>
               <img id="blah" src="#" alt="your image" style = "display:none" width = "250px" height = "150px"/>  
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
      
      
   </div>
</div>

@endsection
