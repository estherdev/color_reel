@extends('admin.mainlayout')
@section('title', 'Admin| Countries')
@section('content')
@section('heading','Countries')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Countries')  
<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
       <h5 class="card-title">Countries List</h5>     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered table-hover">
         <?php //echo "<pre>";print_r($packages);exit;?>
            <thead>
               <tr class="text-nowrap text-center">
                  <th>Sr No.</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
            @forelse($countries as $row)
            <tr class="text-nowrap text-center">
              <td>{{$loop->iteration}}</td>
              <td>{{$row['name']}}</td>
                @if($row['status'] == 1)              
              <td><a href = "{{url('admin-modify-countries-status/countries/id/'.$row['id'])}}"> <i class="fa fa-unlock"></i></a></td>
            @else
                <td><a href = "{{url('admin-modify-countries-status/countries/id/'.$row['id'])}}"> <i class = "fa fa-lock"></i></a></td>
            @endif
              <td> <a href = "{{url('admin-delete-countries/'.$row['id'])}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr><td colspan = "4" class = "text-center">{{ config('global.nodata') }}</td><tr>
            @endforelse 
            </tbody>           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection