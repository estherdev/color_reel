@extends('admin.mainlayout')
@section('title', 'Admin Film Makers')
@section('content')
@section('heading','Filmmaker')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','add-Filmmakers')  
<div class="row">
   <div class="col-md-12">
      <div class="card">
      <?php //print_r($user)?>
         @if(@$user !='')
         <form class="form-horizontal" method = "post" action = "{{url('/filmmaker-submit-detail/'.$user->user_id)}}">
         @else
         <form class="form-horizontal" method = "post" action = "{{url('/filmmaker-submit-detail')}}">
         @endif
        
         {!! csrf_field() !!}
            <div class="card-body">
               <h4 class="card-title">Add  Detail</h4>
               <div class="form-group row">
                  <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                  <div class="col-sm-9">
                     <input type="text" name = "name" class="form-control" id="fname" placeholder="Enter User Name" required value = "{{old('name') != '' ? old('name') : @$user->name }}">
                     <span style = "color:red">{{ $errors->first('name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Email</label>
                  <div class="col-sm-9">
                     <input type="email" name  = "email" class="form-control" id="" placeholder="Enter Email id" required value = "{{old('email') != '' ? old('email') : @$user->email }}">
                     <span style = "color:red">{{ $errors->first('email') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Password</label>
                  <div class="col-sm-9">
                     <input type="password" name  = "password" class="form-control" id="" placeholder="Enter Password "  value = "{{old('password') != '' ? old('password') : @$user->password}}">
                     <span style = "color:red">{{ $errors->first('password') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Confirm Password</label>
                  <div class="col-sm-9">
                     <input type="password" name  = "password_confirmation" class="form-control" id="" placeholder="Confirm Password "  value = "{{old('password') != '' ? old('password') : @$user->password}}">
                     <span style = "color:red">{{ $errors->first('password_confirmation') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label for="lname" class="col-sm-3 text-right control-label col-form-label">Phone Number</label>
                  <div class="col-sm-9">
                     <input type="text" name  = "phone_number" class="form-control" id="" placeholder="Enter 10 digit mobile number" required value = "{{old('phone_number') != '' ? old('phone_number') : @$user->phone_number }}">
                     <span style = "color:red">{{ $errors->first('phone_number') }}</span>
                  </div>
               </div>
               
            </div>
            <div class="border-top">
               <div class="card-body">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </div>
         </form>
      </div>
      
      
   </div>
</div>
@endsection