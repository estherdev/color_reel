@extends('admin.mainlayout')
@section('title', 'Admin| Event Detail')
@section('content')
@section('heading','Event Detail')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Event Detail') 

<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href="{{url('admin-manage-faq')}}" class="btn btn-primary float-right mento" >Add Event</a>
      <h5 class="card-title">Event Detail</h5>
       @if(!empty($event))
      <div class="row viewEvent">
        <div class="col-md-6">
          <table class="table">
            <tr>
              <th>Name</th>
              <td>{{ucfirst($event['name'])}}</td>
            </tr>
            
            <tr>
              <th>Website</th>
               <td> <a href="{{$event['websiteurl']}}" target="_blank">{{$event['websiteurl']}}</a></td>
            </tr>
             <tr>
              <th>Start</th>
              <td>{{$event['event_start_date'] .' '.$event['event_start_time'] }}</td>
            </tr>
            <tr>
              <th>Email</th>
              <td>{{$event['email']}}</td>
            </tr>            
            <tr>
              <th>Phone</th>
              <td>{{$event['mobile']}}</td>
            </tr>
            <?php pr($event['event_variant']);?>

          </table>
        </div>
        <div class="col-md-6">
          <table class="table">
           <tr>
              <th>Image</th>
              <td><img src="{{ asset('uploads/events/'.$event['image'])}}"></td>
            </tr>
             <tr>
              <th>End</th>
              <td>{{$event['event_end_date'] .' '. $event['event_end_time'] }}</td>
            </tr>        
             <tr>
              <th>Address</th>
              <td>{{ucfirst($event['address'])}}</td>
            </tr>
          </table>
        </div>
      </div>
      @else
      <h4 class = "card-title text-center">{{ config('global.nodata') }}</h4>
      @endif
   </div>
</div>
</div>
</div>
@endsection