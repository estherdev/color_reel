@extends('admin.mainlayout')
@section('title', 'Admin add Faq')
@section('content')
@section('heading','Faq')
@section('breadcrumb_menu','Home')
@section('breadcrumb_submenu','add Faq')
<div class="row">
	<div class="col-md-12">
		<div class="card">			
	         @if(@$faq !='')
	         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-faq-detail/'.$faq->id)}}" >
		         @else
		         <form class="form-horizontal" method = "post" action = "{{url('/admin-submit-faq-detail/')}}" >
			         @endif         
			         {!! csrf_field() !!}
		            <div class="card-body">
			               <h4 class="card-title">Add Faq Detail</h4>
			               <div class="form-group row adevent">
				                  <label for="fname" class="col-sm-2 text-right control-label col-form-label">Que <span class="mandatory">*</span></label>
				                  <div class="col-sm-10">
					                     <input type="text" name="que" class="form-control" id="que" placeholder="Enter Que" required value = "{{old('que') != '' ? old('que') : @$faq->que }}">
					                     <span style="color:red">{{ $errors->first('que') }}</span>
				                  </div>
				            
				                  <label for="ans" class="col-sm-2 text-right control-label col-form-label">Ans<span class="mandatory">*</span></label>
				                  <div class="col-sm-10">
					                     <textarea type="text" name="ans" class="form-control" id="editor1" placeholder="Enter Ans" required>{{old('ans') != '' ? old('ans') : @$faq->ans }} </textarea>
					                     <span style="color:red">{{ $errors->first('ans') }}</span>
				                  </div>
			                   </div>  
		               </div>		               
	            </div>
	            <div class="border-top">
	               <div class="card-body text-center">
		                  <button type="submit" class="btn btn-primary">Submit</button>
	               </div>
	            </div>
          </form>
	      </div>      
	   </div>
	</div>
	@endsection