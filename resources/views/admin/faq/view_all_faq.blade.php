@extends('admin.mainlayout')
@section('title', 'Admin| Faq')
@section('content')
@section('heading','Faq')  
@section('breadcrumb_menu','Home')  
@section('breadcrumb_submenu','Faq')  

<div class="row">
   <!-- /.col -->
   <div class="col-md-12">
   @include('admin.message')
<div class="card">
   <div class="card-body">
   <a href="{{url('admin-manage-faq')}}" class="btn btn-primary float-right mento" >Add Faq</a>
 
      <h5 class="card-title">Faq List</h5>
     
      <div class="table-responsive">
         <table id="zero_config" class="table table-striped table-bordered table-hover">
         <?php //echo "<pre>";print_r($faq);exit;?>
            <thead>
               <tr class="text-nowrap">
                  <th>S No.</th>
                  <th>Que</th>
                  <th>Ans</th>
                  <th>Status</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
              
            @forelse($faq as $row)
            <tr class="" >
              <td>{{$loop->iteration}}</td>
              <td>{{ucfirst($row->que)}}</td>            
              <td>{{$row->ans}}</td>
              @if($row->status == 1)              
                <td class="text-center"><a href="{{url('admin-modify-faq-status/faq/id/'.$row->id)}}"> <i class="fa fa-unlock"></i></a></td>
              @else
                <td class="text-center"><a href="{{url('admin-modify-faq-status/faq/id/'.$row->id)}}"> <i class = "fa fa-lock"></i></a></td>
              @endif
              <td class="text-nowrap">
              	<a href="{{url('admin-view-faq/'.$row->id)}}"><i class="fa fa-eye"></i></a> | 
              	<a href="{{url('admin-edit-faq/'.$row->id)}}"><i class="fa fa-edit"></i></a> | 
              	<a href="{{url('admin-delete-faq/'.$row->id)}}" onclick="return confirm('Are you sure you want to delete ?');"><i class = "fa fa-trash"></i></a></td>
              </tr>
            @empty
              <tr><td colspan ="11" class = "text-center">{{ config('global.nodata') }}</td><tr>
            @endforelse 

            </tbody>
           
         </table>
      </div>
   </div>
</div>
</div>
</div>
@endsection