<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Site information -->
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Color Reel</title>
		<meta name="description" content="Bigshow is a template to jump start campaigning your various kind of movie or tv show">
		<!-- External CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/owl.transitions.css">
		<link rel="stylesheet" href="assets/css/magnific-popup.css">
		<!-- Custom CSS -->
		<link rel="stylesheet" href="assets/css/style.css">
		<link rel="stylesheet" href="assets/css/responsive.css">
		<!-- Google Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
		<!-- Favicon -->
		<link rel="icon" href="images/template/favicon.png">
		<link rel="apple-touch-icon" href="images/template/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="images/template/icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="images/template/icon-114x114.png">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- Preloader -->
		<div class="preloader" id="preloader">
			<div class="lds-css ng-scope">
				<div class="lds-ripple">
					<div></div>
					<div></div>
				</div>
			</div>
		</div>
		<!-- Preloader End -->
		<!-- Top Header -->
		<header class="topbar text-white" id="topbar">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-8">
						<p class="topbar-intro">+1 818-808-1234</p>
					</div>
					<div class="col-lg-6 col-sm-4">
						<div class="topbar-right-btns">
							<a class="btn" href="login.html">Register as user</a>
							<a class="btn" href="signup.html">Register as film maker</a>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- Top Header End -->
		<!-- Main Header -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav-collapse" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index-2.html">
						<img src="images/template/logo.png" alt="Site Logo">
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="main-nav-collapse">
					<!-- <form class="navbar-form navbar-left">
								<select name="type">
											<option value="movie" selected>Movies</option>
											<option value="tv">TV</option>
								</select>
								<input type="search" name="searchinput" required>
								<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
					</form> -->
					<ul class="nav navbar-nav navbar-right">
						<li><a href="index.html">Home</a></li>
						<li><a href="#">Film Festival</a></li>
						<li><a href="#">Award Ceremony</a></li>
						
						<li class="dropdown">
							<a href="#">Events</a>
							<ul class="dropdown-menu">
								<li><a href="#">Past Events</a></li>
								<li><a href="#">Upcoming Events</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#">Browse Movies</a>
							<ul class="dropdown-menu">
								<li><a href="#">Genres</a></li>
								<li><a href="#">Region</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- Main Header End -->
		<!-- Page Header -->
		<div class="page-header">
			<div class="page-header-overlay">
				<div class="container">
					<h2 class="page-title">Event Detail</h2>
				</div>
			</div>
		</div>
		
		<!-- Page Header End -->
		<div class="main-wrap">
			<div class="section section-padding video-single-section">
				<div class="container">
					<div class="video-single">
						<div class="row">
							<div class="col-md-5">
								<div class="event-img">
									<img src="images/events/demo.jpg">
								</div>
							</div>
							<div class="col-md-7 event-detail-main">
								<h2 class="event-title">Pellentesque habitant morbi</h2>
								<div class="event-info">
									<div class="date-info row">
										<div class="col-md-6 event-startdate">
											
											<div class="date-start">
												<span class="date-title">Start</span>
												<div class="date-value">Friday, August 30, 2019 12:30 PM</div>
											</div>
											
										</div>
										<div class="col-md-6 event-startdate">
											
											<div class="date-start">
												<span class="date-title">End</span>
												<div class="date-value">Saturday, August 31, 2019 3:00 AM</div>
											</div>
											
										</div>
									</div>
									<div class="row location-info">
										<div class="col-md-6">
											
											<div class="event-address">
												<span class="address-title">Address</span>
												<div class="address-value">
													38440 Wolfsburg, Germany&nbsp;&nbsp;
													<a href="#" target="_blank" class="map-link" data-slimstat="5">View map <i class="fa fa-map-marker"></i></a>
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											
											<div class="event-phone">
												<span class="phone-title">Phone</span>
												<div class="phone-value">1234567890</div>
											</div>
											
										</div>
									</div>
									
									<div class="row more-info">
										<div class="col-md-6">
											
											<div class="event-email">
												<span class="email-title">Email</span>
												<div class="email-value">
													<a href="mailto:info@example.com">info@example.com</a>
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											
											<div class="event-website">
												<span class="website-title">Website</span>
												<div class="website-value">
													<a href="#" target="_blank">www.example.com</a>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<div class="event-price">
									<p class="price"><i class="fa fa-usd" aria-hidden="true"></i> 59.00</p>
									<p>Nulla fringilla a dui in placerat. Proin facilisis, augue non ornare ullamcorper, sem turpis rutrum risus, eget blandit magna sem sed urna.</p>
								</div>
								<div class="event-total">
									<p class="total"><span>Total : </span> <i class="fa fa-usd" aria-hidden="true"></i> 59.00</p>
									<div class="ticket-price">
										<form>
											<div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
											<input type="number" id="number" value="0" />
											<div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
											<select>
												<option>Select Category</option>
												<option>Gold</option>
												<option>Platinum</option>
												<option>Standard</option>
											</select>
											<button class="book-event">Book Event</button>
											<a class="book-event">Book Event</a>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="row status">
							<div class="col-md-7">
								<div class="event-status">
									<h3 class="status-title">Status</h3>
									<p id="demo"></p>
									<div class="about-ticket">
										<p><i class="fa fa-ticket"></i> 169 Qty Available</p>
										<p><i class="fa fa-user"></i> 625 Has Sold</p>
									</div>
									<ul class="social-share">
										<li class="facebook"><a class=""  href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li class="twitter"><a class=""  href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li class="linkedin"><a class=""  href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
										<li class="tumblr"><a class=""  href="#" target="_blank"><i class="fa fa-tumblr"></i></a></li>
										<li class="google-plus"><a class=""  href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
										<li class="pinterest"><a class=""  href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
										<li class="vk"><a class=""  href="#" target="_blank"><i class="fa fa-vk"></i></a></li>
										<li class="email"><a class=""  href="#" target="_blank"><i class="fa fa-envelope"></i></a></li>
										<li class="whatsapp"><a class=""  href="#" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-5">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.056887220717!2d77.3739389145595!3d28.628057091035387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce58785d5e2ff%3A0xd23c99820f0663ad!2sPan%20India%20Internet%20Private%20Limited!5e0!3m2!1sen!2sin!4v1566799993518!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							</div>
						</div>
						<div class="events-tabs">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#home">Description</a></li>
								<!--   <li><a data-toggle="tab" href="#review">Reviews (1)</a></li> -->
								
							</ul>
							<div class="tab-content">
								<div id="home" class="tab-pane fade in active">
									<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
									<p>Nam vitae tortor in lectus pretium posuere id fermentum urna. Aliquam vel lectus vitae augue placerat tristique ut in odio. Duis arcu diam, sodales vitae sem cursus, varius placerat ante. Praesent gravida fringilla lacus nec sodales. Suspendisse vel finibus nibh. Nulla fermentum cursus dapibus. Phasellus hendrerit tortor non est interdum, sit amet luctus massa pretium. Etiam ut neque non mi laoreet auctor. Suspendisse porta est eu</p>
								</div>
								<!-- <div id="review" class="tab-pane fade">
									<h3>Menu 1</h3>
									<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								</div> -->
								
							</div>
						</div>
						 <div id="event-video-slider" class="owl-carousel banner-slider">
                <div class="banner-item banner-item-1">
                    <div class="overlay-70">
                        <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                        
                    </div>
                </div>
                <div class="banner-item banner-item-2">
                    <div class="overlay-70">
                        <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                       
                    </div>
                </div>
                <div class="banner-item banner-item-3">
                    <div class="overlay-70">
                        <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                       
                    </div>
                </div>
            </div>
					</div>
				</div>
			</div>
			<!-- Latest Movies -->
			<div class="section section-padding top-padding-normal related-events">
				<div class="container">
					<div class="related-section">
						<div class="row">
						<div class="col-sm-8">
							<div class="section-header">
								<h2 class="section-title">Related Events</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="owl-carousel col-md-12" id="related-carousel">
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="images/events/related.jpg" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
									
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="images/events/related1.jpg" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="images/events/related2.jpg" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="images/events/related.jpg" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="images/events/related1.jpg" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- Latest Movies End -->
		</div>
		<!-- Footer Start -->
		<footer class="text-white">
			<div class="footer-widget-area">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-xs-12 sm-bottom-40">
							<div class="widget about-widget">
								<div class="widget-inner">
									<a class="footer-logo" href="index-2.html">
										<img src="images/template/logo-footer.png" alt="Footer logo">
									</a>
									<p class="about-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque, ducimus, atque. Praesentium suscipit provident explicabo dignissimos nostrum numquam deserunt earum accusantium et fugit.</p>
									<div class="footer-social">
										<h5 class="footer-social-title">Connect with</h5>
										<div class="socials">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
											<a href="#"><i class="fa fa-google-plus"></i></a>
											<a href="#"><i class="fa fa-youtube-play"></i></a>
											<a href="#"><i class="fa fa-instagram"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="widget category-widget">
								<h3 class="widget-title">Information</h3>
								<div class="widget-inner">
									<ul class="widget-cat">
										<li class="cat"><a href="#">About Us</a></li>
										<li class="cat"><a href="#">ColorReel News</a></li>
										<li class="cat"><a href="#">FAQ</a></li>
										<li class="cat"><a href="#">Gallery</a></li>
										<li class="cat"><a href="#">Contact Us</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="widget newsletter-widget">
								<h3 class="widget-title">Newsletter</h3>
								<div class="widget-inner">
									<p>Sign up for our mailling list to get latest updates videos and upcomming Movie</p>
									<form id="subscriptionForm" class="subscription" action="#" method="post">
										<input type="email" name="email" placeholder="Email Address" required>
										<button type="submit" name="emailsubmit"><i class="fa fa-arrow-circle-right"></i></button>
										<p class="newsletter-success"></p>
										<p class="newsletter-error"></p>
									</form>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="copyright-footer">
				<div class="container">
					<div class="row">
						
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
							<p class="copyright-text">&copy; 2019 <a href="index-2.html">ColorReel</a>. All Rights Reserved</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- Footer End -->
		<!-- Script -->
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/owl.carousel.js"></script>
		<script src="assets/js/jquery.magnific-popup.min.js"></script>
		<script src="assets/js/jquery.ajaxchimp.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmiJjq5DIg_K9fv6RE72OY__p9jz0YTMI"></script>
		<script src="assets/js/map.js"></script>
		<script src="assets/js/custom.js"></script>
	</body>
</html>