@extends('frontend.layout.mainlayout')
@section('title', 'Color Reel')
@section('content')   


    <!-- Page Header -->
    <div class="page-header">
        <div class="page-header-overlay">
            <div class="container">
                <h2 class="page-title">News Feed</h2>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="main-wrap">
        <div class="section section-padding news-list-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="news-list">

                            @forelse($news as $row)
                           
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-md-4 col-xs-12">
                                        <a class="news-thumb" href="{{URL::to('news-deatail/'.$row->blog_id)}}">
                                            <img src="{{asset('uploads').'/'.$row->image}}" alt="News Thumb">
                                        </a>
                                    </div>
                                    <div class="col-md-8 col-xs-12">
                                        <div class="news-content">
                                            <p class="news-metas">
                                                <span class="news-meta date-meta">{{DDMMYY($row->created_at)}}</span> | <span class="news-meta author-meta">By - <a href="#">Admin</a></span>
                                            </p>
                                            <h3 class="news-title"><a href="{{URL::to('news-deatail/'.$row->blog_id)}}">Combined with a handful of model</a></h3>
                                            <p class="news-excerpt">{{strip_tags($row->description)}}</p>
                                            <a class="news-link-btn" href="{{URL::to('news-deatail/'.$row->blog_id)}}">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @empty
                            {{ config('global.nodata') }}                 
                           @endforelse
    

                            <!-- Post Pagination -->
                            <nav class="navigation pagination" role="navigation">
                                <div class="nav-links">
                                    <span class="current-page">Page 2 of 14</span>
                                    <a class="prev page-numbers" href="#"><i class="fa fa-caret-left"></i></a>
                                    <a class="page-numbers" href="#">1</a>
                                    <span class="page-numbers current">2</span>
                                    <a class="page-numbers" href="#">3</a>
                                    <a class="page-numbers" href="#">4</a>
                                    <a class="next page-numbers" href="#"><i class="fa fa-caret-right"></i></a>
                                </div>
                            </nav>
                            <!-- Post Pagination End -->
                        </div>
                    </div>                   
                </div>
            </div>
        </div>
    </div>

  @endsection