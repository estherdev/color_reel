@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel| Packages')
@section('content')

    <!-- Page Header -->
    <div class="page-header">
        <div class="page-header-overlay">
            <div class="container">
                <p class="header-metas">12 June, 2017 | By - Admin </p>
                <h2 class="page-title">Wow, It’s going to be awesome</h2>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="main-wrap">
        <div class="section section-padding news-single-section">
            <div class="container">
                <div class="row">
                        <div class="news-single">
                            <div class="news-single-main">
                                <div class="single-thumb">
                                    <img class="img-responsive" src="images/news/single-thumb.png" alt="Single News Thumb">
                                </div>
                                <div class="news-entry">
                                    <p>Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure<br/>Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                                    <blockquote>Nunc blandit tincidunt consequat. Duis diam metus, suscipit in pulvinar eget, egestas id arcu. Duis a enim vel mauris ultrices. Nullam aliquet velit ac velit tempus in semper neque auctor Contrary to popular belief</blockquote>
                                    <p>Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over</p>
                                </div>
                            </div>

                            <!-- News Footer -->
                            <div class="single-footer">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="news-share">
                                            <label>Share: </label>
                                            <div class="share-social">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 col-xs-12">
                                        <div class="news-tag">
                                            <label>Tags: </label>
                                            <a href="#">Action</a>,
                                            <a href="#">Movie</a>,
                                            <a href="#">Drama</a>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <!-- News Footer -->

                            <!-- Given Comment -->
                            <!-- <div class="given-comment">
                                <h3 class="given-comment-title">Comments (03)</h3>
                                <ul class="comments">
                                    <li class="comment">
                                        <div class="comment-wrap">
                                            <div class="commenter-thumb">
                                                <img src="images/news/avatar1.png" alt="commenter Photo">
                                            </div>
                                            <div class="comment-body">
                                                <h6 class="comment-title">
                                                    <span class="commenter-name">Manaxi Kandy</span>
                                                    <span class="comment-date">/ 24 April 2017</span>
                                                </h6>
                                                <div class="comment-content">
                                                    <p>Dolore magnam aliquam quaerat voluptatem. dit aut fugit, sed quia consequuntur</p>
                                                </div>
                                                <a class="comment-reply" href="#">Reply</a>
                                            </div>
                                        </div>
                                        <ul class="comments child-comments">
                                            <li class="comment">
                                                <div class="comment-wrap">
                                                    <div class="commenter-thumb">
                                                        <img src="images/news/avatar2.png" alt="commenter Photo">
                                                    </div>
                                                    <div class="comment-body">
                                                        <h6 class="comment-title">
                                                            <span class="commenter-name">Manaxi Kandy</span>
                                                            <span class="comment-date">/ 24 April 2017</span>
                                                        </h6>
                                                        <div class="comment-content">
                                                            <p>Dolore magnam aliquam quaerat voluptatem. dit aut fugit, sed quia consequuntur</p>
                                                        </div>
                                                        <a class="comment-reply" href="#">Reply</a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div> -->
                            <!-- Given Comment -->

                            <!-- Commenting form -->
                            <!-- <div class="commenting-wrap">
                                <h4 class="comment-form-title">Leave Your Comments</h4>
                                <p>Your email address will not be published. Required fields are marked *</p>
                                <form id="comment-form" class="comment-form" action="#" method="post">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" name="name" placeholder="Full Name *" required="">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="email" name="email" placeholder="Email *" required="">
                                        </div>
                                    </div>
                                    <textarea name="comment" placeholder="Comment" rows="4"></textarea>
                                    <button type="submit" name="comment-submit">Submit</button>
                                </form>
                            </div> -->
                            <!-- Commenting form end -->

                        </div>
                    </div>
                    <!-- <div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-5">
                        <div class="sidebar">
                            <div class="widget search-widget">
                                <div class="widget-inner">
                                    <form id="sidebarSearch" class="searchform" action="#" method="post">
                                        <input type="search" name="searchinput" placeholder="Search..." required>
                                        <button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div class="widget category-widget">
                                <h3 class="widget-title">Category</h3>
                                <div class="widget-inner">
                                    <ul class="widget-cat">
                                        <li class="cat"><a href="#">TV Show<span>23</span></a></li>
                                        <li class="cat"><a href="#">Drama<span>13</span></a></li>
                                        <li class="cat"><a href="#">Action Movies<span>33</span></a></li>
                                        <li class="cat"><a href="#">Horror Movie<span>25</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="widget thumb-post-widget">
                                <h3 class="widget-title">Latest Posts</h3>
                                <div class="widget-inner">
                                    <div class="widget-posts">
                                        <div class="widget-post">
                                            <a class="widget-thumb" href="news-detail.html">
                                                <img src="images/news/s1.png" alt="Sidebar Post thumb">
                                            </a>
                                            <div class="widget-post-content">
                                                <a class="widget-post-title" href="news-detail.html">Molit anim id est labor</a>
                                                <p class="widget-post-date">15 May, 2017</p>
                                            </div>
                                        </div>
                                        <div class="widget-post">
                                            <a class="widget-thumb" href="news-detail.html">
                                                <img src="images/news/s2.png" alt="Sidebar Post thumb">
                                            </a>
                                            <div class="widget-post-content">
                                                <a class="widget-post-title" href="news-detail.html">Molit anim id est labor</a>
                                                <p class="widget-post-date">15 May, 2017</p>
                                            </div>
                                        </div>
                                        <div class="widget-post">
                                            <a class="widget-thumb" href="news-detail.html">
                                                <img src="images/news/s3.png" alt="Sidebar Post thumb">
                                            </a>
                                            <div class="widget-post-content">
                                                <a class="widget-post-title" href="news-detail.html">Molit anim id est labor</a>
                                                <p class="widget-post-date">15 May, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget tag-widget">
                                <h3 class="widget-title">Post Tag</h3>
                                <div class="widget-inner">
                                    <div class="tags">
                                        <a href="#" class="tag">Creative</a>
                                        <a href="#" class="tag">Design</a>
                                        <a href="#" class="tag">Skill</a>
                                        <a href="#" class="tag">Template</a>
                                        <a href="#" class="tag">Landing</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

    @endsection