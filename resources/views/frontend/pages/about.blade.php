@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel| Packages')
@section('content')

    <div class="page-header">
        <div class="page-header-overlay">
            <div class="container">
                <h2 class="page-title">About Us</h2>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="main-wrap about">
        <div class="section section-padding news-list-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-12">
                        <div class="about-img">
                        	<img src="assets/img/about-img.jpg">
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12">
						<div class="about-content">
							<h3>The Team Here To Create a Completely New Experience For You.</h3>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
							<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
							<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
						</div>                       
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsecttion 