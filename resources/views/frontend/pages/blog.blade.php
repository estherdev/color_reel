@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel| Packages')
@section('content')

    <!-- Page Header -->
    <div class="page-header">
        <div class="page-header-overlay">
            <div class="container">
                <h2 class="page-title">Blog</h2>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="main-wrap">
        <div class="section section-padding news-list-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="news-list">
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <a class="news-thumb" href="news-detail.html">
                                            <img src="images/news/l1.png" alt="News Thumb">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="news-content">
                                            <p class="news-metas">
                                                <span class="news-meta date-meta">12 June, 2017</span> | <span class="news-meta author-meta">By - <a href="#">Admin</a></span>
                                            </p>
                                            <h3 class="news-title"><a href="news-detail.html">Wow, It's going to be awesome</a></h3>
                                            <p class="news-excerpt">Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam</p>
                                            <a class="news-link-btn" href="news-detail.html">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <a class="news-thumb" href="news-detail.html">
                                            <img src="images/news/l2.png" alt="News Thumb">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="news-content">
                                            <p class="news-metas">
                                                <span class="news-meta date-meta">12 June, 2017</span> | <span class="news-meta author-meta">By - <a href="#">Admin</a></span>
                                            </p>
                                            <h3 class="news-title"><a href="news-detail.html">Combined with a handful of model</a></h3>
                                            <p class="news-excerpt">Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam</p>
                                            <a class="news-link-btn" href="news-detail.html">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <a class="news-thumb" href="news-detail.html">
                                            <img src="images/news/l3.png" alt="News Thumb">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="news-content">
                                            <p class="news-metas">
                                                <span class="news-meta date-meta">12 June, 2017</span> | <span class="news-meta author-meta">By - <a href="#">Admin</a></span>
                                            </p>
                                            <h3 class="news-title"><a href="news-detail.html">Wow, It's going to be awesome</a></h3>
                                            <p class="news-excerpt">Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam</p>
                                            <a class="news-link-btn" href="news-detail.html">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <a class="news-thumb" href="news-detail.html">
                                            <img src="images/news/l4.png" alt="News Thumb">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="news-content">
                                            <p class="news-metas">
                                                <span class="news-meta date-meta">12 June, 2017</span> | <span class="news-meta author-meta">By - <a href="#">Admin</a></span>
                                            </p>
                                            <h3 class="news-title"><a href="news-detail.html">Want more photos than available</a></h3>
                                            <p class="news-excerpt">Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam</p>
                                            <a class="news-link-btn" href="news-detail.html">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <a class="news-thumb" href="news-detail.html">
                                            <img src="images/news/l5.png" alt="News Thumb">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="news-content">
                                            <p class="news-metas">
                                                <span class="news-meta date-meta">12 June, 2017</span> | <span class="news-meta author-meta">By - <a href="#">Admin</a></span>
                                            </p>
                                            <h3 class="news-title"><a href="news-detail.html">Richard McClintock, a Latin professor</a></h3>
                                            <p class="news-excerpt">Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam</p>
                                            <a class="news-link-btn" href="news-detail.html">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <a class="news-thumb" href="news-detail.html">
                                            <img src="images/news/l6.png" alt="News Thumb">
                                        </a>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="news-content">
                                            <p class="news-metas">
                                                <span class="news-meta date-meta">12 June, 2017</span> | <span class="news-meta author-meta">By - <a href="#">Admin</a></span>
                                            </p>
                                            <h3 class="news-title"><a href="news-detail.html">If you are going to use a passage </a></h3>
                                            <p class="news-excerpt">Aliquam rhoncus risus nibh, sed laoreet lacus auctor mollis. Sed sed laoreet lorem, eget aliquam magna. Vivamus finibus diam</p>
                                            <a class="news-link-btn" href="news-detail.html">More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Post Pagination -->
                            <nav class="navigation pagination" role="navigation">
                                <div class="nav-links">
                                    <span class="current-page">Page 2 of 14</span>
                                    <a class="prev page-numbers" href="#"><i class="fa fa-caret-left"></i></a>
                                    <a class="page-numbers" href="#">1</a>
                                    <span class="page-numbers current">2</span>
                                    <a class="page-numbers" href="#">3</a>
                                    <a class="page-numbers" href="#">4</a>
                                    <a class="next page-numbers" href="#"><i class="fa fa-caret-right"></i></a>
                                </div>
                            </nav>
                            <!-- Post Pagination End -->
                        </div>
                    </div>
                    <div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-5">
                        <div class="sidebar">
                            <div class="widget search-widget">
                                <div class="widget-inner">
                                    <form id="sidebarSearch" class="searchform" action="#" method="post">
                                        <input type="search" name="searchinput" placeholder="Search..." required>
                                        <button type="submit" name="searchsubmit"><i class="fa fa-search"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div class="widget category-widget">
                                <h3 class="widget-title">Category</h3>
                                <div class="widget-inner">
                                    <ul class="widget-cat">
                                        <li class="cat"><a href="#">TV Show<span>23</span></a></li>
                                        <li class="cat"><a href="#">Drama<span>13</span></a></li>
                                        <li class="cat"><a href="#">Action Movies<span>33</span></a></li>
                                        <li class="cat"><a href="#">Horror Movie<span>25</span></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="widget thumb-post-widget">
                                <h3 class="widget-title">Latest Posts</h3>
                                <div class="widget-inner">
                                    <div class="widget-posts">
                                        <div class="widget-post">
                                            <a class="widget-thumb" href="news-detail.html">
                                                <img src="images/news/s1.png" alt="Sidebar Post thumb">
                                            </a>
                                            <div class="widget-post-content">
                                                <a class="widget-post-title" href="news-detail.html">Molit anim id est labor</a>
                                                <p class="widget-post-date">15 May, 2017</p>
                                            </div>
                                        </div>
                                        <div class="widget-post">
                                            <a class="widget-thumb" href="news-detail.html">
                                                <img src="images/news/s2.png" alt="Sidebar Post thumb">
                                            </a>
                                            <div class="widget-post-content">
                                                <a class="widget-post-title" href="news-detail.html">Molit anim id est labor</a>
                                                <p class="widget-post-date">15 May, 2017</p>
                                            </div>
                                        </div>
                                        <div class="widget-post">
                                            <a class="widget-thumb" href="news-detail.html">
                                                <img src="images/news/s3.png" alt="Sidebar Post thumb">
                                            </a>
                                            <div class="widget-post-content">
                                                <a class="widget-post-title" href="news-detail.html">Molit anim id est labor</a>
                                                <p class="widget-post-date">15 May, 2017</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="widget tag-widget">
                                <h3 class="widget-title">Post Tag</h3>
                                <div class="widget-inner">
                                    <div class="tags">
                                        <a href="#" class="tag">Creative</a>
                                        <a href="#" class="tag">Design</a>
                                        <a href="#" class="tag">Skill</a>
                                        <a href="#" class="tag">Template</a>
                                        <a href="#" class="tag">Landing</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>

@endsection
