@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel| Packages')
@section('content')


    <!-- Page Header -->
    <div class="page-header">
        <div class="page-header-overlay">
            <div class="container">
                <h2 class="page-title">Contact Us</h2>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="main-wrap">
        <div class="section section-padding news-list-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-12">
                        <div class="contactpage-form">
                            <h3>Please fill the form below:</h3>
                            <form>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label>Your Name (required)</label>
                                        <input type="text" class="form-control form-input">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Your Email (required)</label>
                                        <input type="text" class="form-control form-input">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Subject</label>
                                        <input type="text" class="form-control form-input">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label>Your Message</label>
                                        <textarea class="form-control form-input" rows="5"></textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <button class="send-btn">Send</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-5">
                        <div class="contact-info">
                            <h3>Contact Information</h3>
                            <ul>
                                <li><i class="fa fa-home" aria-hidden="true"></i> 184 Main street west victoria 887897, Australia</li>
                                <li><i class="fa fa-phone" aria-hidden="true"></i> 1800 222 222</li>
                                <li><i class="fa fa-envelope" aria-hidden="true"></i> info@exampe.com</li>
                            </ul>
                        </div>
                        <div class="social-info">
                            <h3>Social Media</h3>
                            <ul>
                                <li><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
<div class="contact-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.0570470681655!2d77.3761274!3d28.6280523!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce58785d5e2ff%3A0xd23c99820f0663ad!2sPan%20India%20Internet%20Private%20Limited!5e0!3m2!1sen!2sin!4v1566995000321!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
@endsection