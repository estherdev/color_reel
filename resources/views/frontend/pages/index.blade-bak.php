@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel')
@section('content')
<section class="banners" style="background-image: url('{{ asset('frontAssets/assets/images/banner.jpg')}}');">
      <div class="bannerContent">
        <h2>ColorReel will be open for submissions to our online film platform on</h2>
        <h3>June 1, 2019</h3>
        <a href="#" class="submitNow">Submit Now</a>
      </div>
    </section>
    <section class="ongoing">
      <div class="container-fluid">
        <div class="ongoingContent">
          <h3>Submissions will be ongoing throughout the year. However we have submission deadlines for your project to be considered for the:</h3>
          <h4>ColorReel Film Festival (August, 2019)</h4>
          <ul>
            <li>Eligible submissions turned in before July 20 (11:59 Central Standard Time) will be included in the ColorReel Film Festival. </li>
          </ul>
          <h4>ColorReel Awards (January 2020)</h4>
          <ul>
            <li>All submissions within the 2019 calendar year submitted by December 31, 2019 (11:59 Central Standard Time) are eligible to be considered in the annual ColorReel Awards.</li>
          </ul>
        </div>
      </div>
    </section>
    <section class="filmMakers" style="background-image: url('{{ asset('frontAssets/assets/images/filmmakers.jpg')}}');">
      <div class="container-fluid">
        <div class="title"><h2>ColorReel for Filmmakers</h2></div>
        <div class="row">
          <div class="col-md-6">
            <div class="makersContent">
              <p><span>ColorReel</span> is a film streaming platform built to support and empower filmmakers whose stories and experiences are underrepresented in the film industry. We are looking to stream quality independent films by filmmakers who are:</p>
              <ul>
                <li><span>People of Color</span></li>
                <li><span>Women</span></li>
                <li><span>LGBTQ+ persons</span></li>
                <li><span>People with Disabilities</span></li>
                <li><span>Citizens of developing nations (for a list of developing countries see here: <a href="#">https://icqi.org/developing-countries-list/</a>)</span></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6">
            <div class="makersContent">
              <p>We recognize that categories aren’t perfect, people do not always fit perfectly in boxes, and that any one person may identify with more than one of these groups. You don’t have to identify as only one – intersectionality is real! By creating these eligibility categories we are doing our best to prioritize the stories of those who have been historically underrepresented in the <span>Western film industry.</span></p>
              <p>The intention of <span>ColorReel</span> is to create a collaborative, diverse and inclusive incubator community for Filmmakers from around the world while providing a platform for viewers to see amazing films. Filmmakers will have the potential to crowdfund through their profiles on the website as well as to be paid residuals as <span>our subscriber</span> base grows.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="submissionDetail">
      <div class="container-fluid">
        <div class="subTitle">
          <h2>Submissions Details</h2>
        </div>
        <div class="row">
          <div class="col-md-5">
            <div class="submissinFeees">
              <div class="submissinFeee">
                <h3>Submissions Fee</h3>
                <p><i class="fas fa-dollar-sign"></i> <span>75</span>USD</p>
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <div class="feeContent">
              <h3>What this Covers</h3>
              <ul>
                <li>1 Year <span>ColorReel</span> Filmmaker Membership: This membership holds even if your submission has not been selected for streaming on the platform.</li>
                <li>1 annual viewer subscription to the <span>ColorReel</span> Platform</li>
                <li>A total of 2 submissions per year (During the period of your Filmmaker membership)</li>
                <li><span>BONUS!</span> Filmmakers who submit their films before July 31, 2019 will get an additional 6 months <span>Free membership</span> in 2020!</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="submissionMain">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 eligiblity">
            <div class="title"><h2>Submission Eligiblity:</h2></div>
            <div class="eligiblityContent"><p>You are eligible to submit a film and become a <span>ColorReel</span> Filmmaker Member if you fit into one or more of the following categories:</p>
            <ul>
              <li><span>People of Color</span></li>
              <li><span>Women</span></li>
              <li><span>LGBTQ+ persons</span></li>
              <li><span>People with Disabilities</span></li>
              <li><span>Citizens of developing nations (for a list of developing countries see here: <a href="#">https://icqi.org/developing-countries-list/)</a></span></li></ul>
            </div>
          </div>
          <div class="col-md-6 category">
            <div class="title"><h2>Submission Categories:</h2></div>
            <div class="categoryContent"><p>Our submission categories are: </p>
            <ul>
              <li><span>Short</span></li>
              <li><span>Documentary</span></li>
              <li><span>Music/Poetry Video Short</span></li>
              <li><span>Series</span></li>
              <li><span>Comedy</span></li>
              <li><span>Drama</span></li>
              <li><span>Action</span></li>
              <li><span>Thriller/Horror</span></li>
              <li><span>Animation</span></li>
              <li><span>Family</span></li>
              <li><span>Other</span></li>
            </ul>
            <p>We are accepting shorts running from about 3 minutes <span>(Music / Poetry video shorts)</span> to full feature-length films as well as series.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    @endsection