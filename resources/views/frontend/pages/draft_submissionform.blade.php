@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel')
@section('content')
<section class="filmInfo">
      <div class="container-fluid">
        
        <form id="regForm" action="{{url('/submit-draft-from-data')}}" method = "post">
        {{ csrf_field() }}
         <div class="tab">
          <div class="pageTitle">
          <h3>Film Information</h3>
        </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Type of Film</label>
                <select class="form-control" name = "film_type[]" id = "filmtype" multiselect required>
                  <option value = "short">Short</option>
                  <option value = "documentry">Documentry</option>
                  <option value = "Music-Poetry-Video Short">Music/Poetry Video Short</option>
                  <option value = "series">Series</option>
                  <option value = "Comedy">Comedy</option>
                  <option value = "Drama">Drama</option>
                  <option value = "Action">Action</option>
                  <option value = "thriller-horror">Thriller/Horror</option>
                  <option value = "Animation">Animation</option>
                  <option value = "Family">Family</option>
                  <option value = "Other">Other</option>
                </select>
                <input type = "text" class = "form-control" name = "film_type[]"  placeholder = "fill other" style = "display:none" id = "other">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Film Genre:</label>
                <input type="text" class="form-control" name="film_genre" id = "film_genre" required>
                
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Project Title:</label>
                <input type="text" class="form-control" name = "project_title" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Brief Synopsis:</label>
                <textarea class="form-control" rows="4" name = "project_synopsis" required></textarea>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Runtime</label>
                <div class="row">
                  <div class="col-md-4">
                    <select class="form-control" name = "hour" required>
                      <option value = "">Hrs.</option>
                     @for ($i = 0; $i < 100; $i++)
                     <option value = "{{$i}}">{{$i}}</option>
                    @endfor
                    </select>
                  </div>
                  <div class="col-md-4">
                    <select class="form-control" name = "minute" required>
                      <option value = "">Min.</option>
                      @for ($i = 0; $i < 60; $i++)
                     <option value = "{{$i}}">{{$i}}</option>
                    @endfor
                    </select>
                  </div>
                  <div class="col-md-4">
                    <select class="form-control" name= "second" required>
                      <option value = "">Sec.</option>
                      @for ($i = 0; $i < 60; $i++)
                     <option value = "{{$i}}">{{$i}}</option>
                    @endfor
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 after-add-more">
              <div class="form-group">
                <label>Episode Length</label>
                <div class="row">
                  <div class="col-md-2">
                    <input type = "text" name = "episode_title[]" class = "form-control" placeholder = "Episode Title" >
                  </div>
                  <div class="col-md-3">
                  <select class="form-control" name = "episode_hour[]">
                      <option value = "">Hrs.</option>
                     @for ($i = 0; $i < 100; $i++)
                     <option value = "{{$i}}">{{$i}}</option>
                    @endfor
                    </select>
                  </div>
                  <div class="col-md-3">
                  <select class="form-control" name = "episode_minute[]">
                      <option value = "">Min.</option>
                      @for ($i = 0; $i < 60; $i++)
                     <option value = "{{$i}}">{{$i}}</option>
                    @endfor
                    </select>
                  </div>
                  <div class="col-md-3">
                  <select class="form-control" name= "episode_sec[]">
                      <option value = "">Sec.</option>
                      @for ($i = 0; $i < 60; $i++)
                     <option value = "{{$i}}">{{$i}}</option>
                    @endfor
                    </select>
                  </div>
                  <div class="col-md-1">
                    <a href="javaScript:void(0)" class="add add-more"><i class="fas fa-plus"></i></a>
                  </div>
                  
                </div>
              </div>
            </div>
          
                
                
               <div class="copy-fields hide">
                  <!-- Add more field  -->
                     
               </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Country of Origin:</label>
                <input type="text" class="form-control" name = "country_origin" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Country of Filming:</label>
                <input type="text" class="form-control" name = "country_filming" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Film Completion Date:</label>
                <input type="text" class="form-control" name = "flim_complition_date" id = "flim_complition_date" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>This film is in English language:</label>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="yes" name="language[]" value = "english">
                  <label class="custom-control-label" for="yes">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="no" name="language[]" value = "">
                  <label class="custom-control-label" for="no">No</label>
                </div>
              </div>
            </div>
            <div class="col-md-8">
              <div class="form-group" id = "chooselng">
                <label>This film is in language(s) other than English</label>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="yes1" name="other_lang" value = "yes">
                  <label class="custom-control-label" for="yes1">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="no1" name="other_lang" value = "no">
                  <label class="custom-control-label" for="no1">No</label>
                </div>
              </div>
            </div>
            <div class="col-md-4" id = "filmlng" style = "display:none">
              <div class="form-group">
                <label>Languages in this film:</label>
                <input type="text" class="form-control" name = "language[]"  >
              </div>
            </div>
            <div class="col-md-4" >
              <div class="form-group" id = "subtitle">
                <label>English subtitles available</label>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="yes2" name="subtitle_lang[]" value = "english">
                  <label class="custom-control-label" for="yes2">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="no2" name="subtitle_lang" value = "">
                  <label class="custom-control-label" for="no2">No</label>
                </div>
              </div>
            </div>
            <div class="col-md-4" id = "othersubtitle"style = "display:none">
              <div class="form-group">
                <label>Other subtitle languages available:</label>
                <input type="text" class="form-control" name = "subtitle_lang[]" >
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>Audio description for the visually impaired</label>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="yes3" name="example1">
                  <label class="custom-control-label" for="yes3">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="no3" name="example1">
                  <label class="custom-control-label" for="no3">No</label>
                </div>
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group" id = "nonengtitle">
                <label>This film also has a non-English Title & synopsis (checkbox) (If yes)</label>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="yes83" name="nontitle" value = "yes">
                  <label class="custom-control-label" for="yes83">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="no83" name="nontitle" value = "no">
                  <label class="custom-control-label" for="no83">No</label>
                </div>
              </div>
            </div>
            <div class = "col-md-12">
            <div class= "row noneng">
            <div class="col-md-6">
              <div class="form-group">
                <label>Non-English Title</label>
                <input type="text" class="form-control" name = "non_eng_title[]">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Non-English Synopsis</label>
                <input type="text" class="form-control" name = "non_eng_description[]">
              </div>
            </div>
            <button class = "btn btn-primary pull-right add-new-more" type = "button">Add More</button>
      </div>
</div>
<div class = "col-md-12 after-add-new-one-more"></div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Facebook</label>
                <input type="text" class="form-control" name = "social_links[]">
                <input type = "hidden" name = "social_media_name[]" value = "Facebook">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Instagram</label>
                <input type="text" class="form-control" name = "social_links[]" >
                <input type = "hidden" name = "social_media_name[]" value = "Instagram">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Linkedin</label>
                <input type="text" class="form-control" name = "social_links[]">
                <input type = "hidden" name = "social_media_name[]" value = "Linkedin">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Youtube</label>
                <input type="text" class="form-control" name = "social_links[]">
                <input type = "hidden" name = "social_media_name[]" value = "Youtube">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Twitter</label>
                <input type="text" class="form-control" name = "social_links[]">
                <input type = "hidden" name = "social_media_name[]" value = "Twitter">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Other</label>
                <input type="text" class="form-control" name = "social_links[]">
                <input type = "hidden" name = "social_media_name[]" value = "other">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Link to Trailer:</label>
                <input type="text" class="form-control" name = "trailer_link" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Link to Film:</label>
                <input type="text" class="form-control" name = "full_movie_link" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Website:</label>
                <input type="text" class="form-control" name = "website">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Screenings and Awards:</label>
                <input type="text" class="form-control" name = "screenings_and_awards" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Does this Film have A Distributor?</label>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="yes4" name="has_disrtibuter" value = "yes" checked>
                  <label class="custom-control-label" for="yes4">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="no4" name="has_disrtibuter" value = "no">
                  <label class="custom-control-label" for="no4">No</label>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Who has ownership and distribution rights to this film?</label>
                <input type="text" class="form-control" name = "ownership" required>
              </div>
            </div>
            
          </div>
        </div>
          <div class="tab"> 
          <div class="pageTitle">
          <h3>Submitter Information</h3>
        </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>Name:</label>
                <input type="text"  class="form-control" name = "name" required>
                  
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Email:</label>
                <input type="text" class="form-control" name = "email" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Phone:</label>
                <input type="text" class="form-control" name = "phone_number" required>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Mailing Address:</label>
                <textarea class="form-control" rows="4" name = "mailing_address" required></textarea>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Role on film (Check All That Apply)</label>
                <div class="row">
                  <div class="col-md-2">
                    <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="writer" name="role_on_film[]" value = "writer">
                  <label class="custom-control-label" for="writer">Writer</label>
                </div>
                  </div>
                  <div class="col-md-2">
                    <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="producer" name="role_on_film[]" value = "producer">
                  <label class="custom-control-label" for="producer">Producer</label>
                </div>
                  </div>
                  <div class="col-md-2">
                    <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="director" name="role_on_film[]" value = "director">
                  <label class="custom-control-label" for="director">Director</label>
                </div>
                  </div>
                  <div class="col-md-2">
                    <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="others" >
                  <label class="custom-control-label" for="others">Other</label>
                </div>
                  </div>
                  <div class="col-md-4">
              <div class="form-group">
                <input type="text" class="form-control" name = "role_on_film[]">
              </div>
            </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="note"><p><b>Eligibility Categories:</b> These are used to determine eligibility for the ColorReel Platform. This information will be kept private. Filmmakers can create their own profiles and decide what information they wish to put on there.</p></div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Country of Origin:</label>
                <input type="text" class="form-control" name = "user_country_origin" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Birth Date:</label>
                <input type="text" class="form-control" name = "dob" id = "dob" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Race:</label>
                <input type="text" class="form-control" name = "race" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Ethnicity:</label>
                <input type="text" class="form-control" name = "ethnicity" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Gender Identity:</label>
                <input type="text" class="form-control" name = "gender_identity" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Gender Expression:</label>
                <input type="text" class="form-control" name = "gender_expression" required>
              </div>
            </div>

            <div class="col-md-4">
                   <div class="form-group">
                <label>LGBTQ+ Identity</label>
                  <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="iyes" name="LGBTQ" value = "yes">
                  <label class="custom-control-label" for="iyes">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="ino" name="LGBTQ" value = "no">
                  <label class="custom-control-label" for="ino">No</label>
                </div>
              </div>
                  </div>
                  <div class="col-md-4">
              <div class="form-group">
                <label>Sexual Identity (optional):</label>
                <input type="text" class="form-control" name = "sexual_identity">
              </div>
            </div>
            <div class="col-md-4">
                   <div class="form-group">
                <label>Do you have a Disability</label>
                  <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="dyes" name="disability" value = "yes">
                  <label class="custom-control-label" for="dyes">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="dno" name="disability" >
                  <label class="custom-control-label" for="dno">No</label>
                </div>
              </div>
                  </div>
                   <div class="col-md-4">
              <div class="form-group">
                <label>If Yes Please specify:</label>
                <input type="text" class="form-control" name = "disability">
              </div>
            </div>
            <div class="col-md-4">
                   <div class="form-group" id = "checkowner">
                <label>Do you have full ownership of this film?</label>
                  <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="oyes" name="checkowner" value = "yes">
                  <label class="custom-control-label" for="oyes">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="ono" name="checkowner" value = "no">
                  <label class="custom-control-label" for="ono">No</label>
                </div>
              </div>
                  </div>
                  <div class="col-md-4">
              <div class="form-group">
                
              </div>
            </div>
            <div class = "col-md-12">
            <div class= "row ownership">
            <div class="col-md-4 ">
              <div class="form-group">
                <label>Other Owner's Name:</label>
                <input type="text" class="form-control" name = "owner_name[]">
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="form-group">
                <label>Other Owner's Email:</label>
                <input type="text" class="form-control" name = "owner_email[]">
              </div>
            </div>
            <div class="col-md-4 ">
              <div class="form-group">
                <label>Other Owner's Phone:</label>
                <input type="text" class="form-control" name = "owner_phone[]">
              </div>
            </div>
            <div class="col-md-12 ">
              <div class="form-group">
                <label>Other Owner's Address:</label>
                <textarea class="form-control" rows="4" name = "owner_mailing_address[]"></textarea>
              </div>
            </div>
            <div class = "col-md-12 text-right">
          <button class = "btn btn-primary pull-right add-new" type = "button">Add More</button>
</div>
          <hr>
          </div> 
          
</div>
  <div class = "col-md-12 after-add-new"></div>

            <div class="col-md-6">
                
                   <div class="form-group" id = "checkdistributor">
                <label>Do you have full distribution rights to this film?</label>
                  <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="ryes" name="checkdistributor" value = "yes">
                  <label class="custom-control-label" for="ryes">Yes</label>
                </div>
                <div class="custom-control custom-radio">
                  <input type="radio" class="custom-control-input" id="rno" name="checkdistributor"  value = "no">
                  <label class="custom-control-label" for="rno">No</label>
                </div>
              </div>
                  </div>
                  <div class="col-md-6">
              <div class="form-group">
                
              </div>
            </div>
            <div class = "col-md-12">
            <div class= "row distributor">
            <div class="col-md-4">
              <div class="form-group">
                <label>Other Distributor’s Name:</label>
                <input type="text" class="form-control" name = "distributor_name[]">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Other Distributor’s Email:</label>
                <input type="text" class="form-control" name = "distributor_email[]">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Other Distributor’s Phone:</label>
                <input type="text" class="form-control" name = "phone[]">
              </div>
            </div>
           <div class="col-md-12">
              <div class="form-group">
                <label>Other Distributor’s Address</label>
                <textarea class="form-control" rows="4" name = "distributor_mailing_address[]"></textarea>
              </div>
            </div>
            <div class = "col-md-12 text-right">
          <button class = "btn btn-primary pull-right add-new-one" type = "button">Add More</button>
</div>
          <hr>
</div>
</div>
<div class = "col-md-12 after-add-new-one"></div>
           <div class="col-md-12">
              <div class="note"><p><b>Acknowledgement of Rights or Permissions</b></p>
<p>I have full rights/permission to submit this film to the ColorReel Platform to potentially be exclusively
streamed for a minimum 2-year period.</p></div>
            </div>
            
           
            <div class="col-md-12">
              <div class="form-group">
                <label>Signature:</label>
                <input type="file" class="form-control">
              </div>
            </div>
            
          </div>
        </div>
        <div class="text-right form-btn">
    <div>
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
   <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
    <span class="step"></span>
    
  </div>
        </form>
      </div>
    </section>
@endsection