@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel| Packages')
@section('content')


    <!-- Page Header -->
    <div class="page-header single-celebrity-header">
        <div class="page-header-overlay">
            <div class="container">
                <h2 class="page-title">Cynthia W. Capra</h2>
            </div>
        </div>
    </div>
    <!-- Page Header End -->

    <div class="main-wrap">
        <div class="section section-padding celebrity-single-section">
            <div class="container">
                <div class="celebrity-single">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="content-wrap">
                                <div class="celebrity-thumb">
                                    <img src="images/celebrity/9.png" alt="Actress Pic">
                                </div>
                                <div class="celebrity-details">
                                    <div class="single-section">
                                        <div class="fimmaker-title">
                                            <h3 class="celebrity-name">Cynthia W. Capra</h3>
                                            <button class="donate"><i class="fa fa-money" aria-hidden="true"></i> Donate Now</button>
                                            <button class="like"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Like</button>
                                        </div>
                                        <p class="celebrity-profession">Actor</p>
                                        <div class="celebrity-infos">
                                            <p class="birthname"><label>Birth Name:</label> Cynthia W. Capra</p>
                                            <p class="birthdate"><label>Date of Birth</label> 03, Oct 1988</p>
                                            <p class="residence"><label>Residence:</label> USA</p>
                                            <p class="country"><label>Country: </label> USA</p>
                                            <p class="gender"><label>Gender: </label> Female</p>
                                            <p class="language"><label>Language:</label> English</p>
                                            <p class="height"><label>Height:</label> 5.7”</p>
                                        </div>
                                        <div class="share-on">
                                            <label>Share: </label>
                                            <div class="share-social">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="single-section bio-entry">
                                        <h3 class="single-section-title">Biography</h3>
                                        <div class="section-content">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                            <p>Pri quas audiam virtute ut, case utamur fuisset eam ut, iisque accommodare an eam. Reque blandit qui eu, cu vix nonumy volumus. Legendos intellegam id usu, vide oporteat vix eu, id illud principes has.</p>
                                        </div>
                                    </div>
                                    <div class="single-section">
                                        <!-- <h3 class="single-section-title">Photo Gallery</h3> -->
                                        <div class="upcomming-featured">
                                <img class="img-responsive" src="images/slider/single-3.png" alt="Single Slider Image">
                                <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                
                            </div>
                            <!-- <div class="section-content">
                                            <div id="single-gallery-1" class="owl-carousel single-gallery-slider">
                                                <img class="img-responsive" src="images/slider/single-3.png" alt="Single Slider Image">
                                                <img class="img-responsive" src="images/slider/single-2.png" alt="Single Slider Image">
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="single-section">
                                        <h3 class="single-section-title">Filmography</h3>
                                        <div class="section-content">
                                            <table class="filmography-table">
                                                <tr class="head-tr">
                                                    <th colspan="2">Movie Name</th>
                                                    <th>Release Date</th>
                                                </tr>
                                                <tr>
                                                    <td class="film-poster">
                                                        <a class="film-thumb" href="movie-detail.html"><img src="images/movies/s1.png" alt="City Truck"></a>
                                                    </td>
                                                    <td class="film-details">
                                                        <div class="film-info">
                                                            <h4 class="film-title"><a href="movie-detail.html">City Truck</a></h4>
                                                            <p class="film-category">Action, Drama</p>
                                                        </div>
                                                    </td>
                                                    <td class="film-release">May 2, 2017</td>
                                                </tr>
                                                <tr>
                                                    <td class="film-poster">
                                                        <a class="film-thumb" href="movie-detail.html"><img src="images/movies/s2.png" alt="Dark Night - 2"></a>
                                                    </td>
                                                    <td class="film-details">
                                                        <div class="film-info">
                                                            <h4 class="film-title"><a href="movie-detail.html">Dark Night - 2</a></h4>
                                                            <p class="film-category">Action, Drama</p>
                                                        </div>
                                                    </td>
                                                    <td class="film-release">January 25, 2017</td>
                                                </tr>
                                                <tr>
                                                    <td class="film-poster">
                                                        <a class="film-thumb" href="movie-detail.html"><img src="images/movies/s3.png" alt="Basket Ball"></a>
                                                    </td>
                                                    <td class="film-details">
                                                        <div class="film-info">
                                                            <h4 class="film-title"><a href="movie-detail.html">Basket Ball</a></h4>
                                                            <p class="film-category">Action</p>
                                                        </div>
                                                    </td>
                                                    <td class="film-release">16 April, 2017</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection