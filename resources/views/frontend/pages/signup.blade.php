@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel| Sign up')
@section('content')

    <div class="account-wrap">

        <a class="site-logo" href="{{'/'}}">
            <img src="{{asset('frontAssets/images/template/logo-white.png')}}" alt="Site Logo">
        </a>

        <form class="accountform signupform">
            <h3>Sign up, it's free..</h3>
            <div class="basic-field">
                <label>Username <br/>
                    <p>
                        <input type="text" name="username" required>
                    </p>
                </label>
                <label>E-mail address <br/>
                    <p>
                        <input type="email" name="email" required>
                    </p>
                </label>
                <label>Password <br/>
                    <p>
                        <input type="password" name="password" required>
                    </p>
                </label>
                <div class="row">
                    <div class="col-sm-6">
                        <label>First Name <br/>
                            <p>
                                <input type="text" name="fname" required>
                            </p>
                        </label>
                    </div>
                    <div class="col-sm-6">
                        <label>Last Name <br/>
                            <p>
                                <input type="text" name="lname" required>
                            </p>
                        </label>
                    </div>
                </div>
            </div>
            <button type="submit">Login</button>
            <p class="signup-recover">Do you already have an account? <a href="login.html">Login here</a></p>
        </form>
    </div>

    @endsection
