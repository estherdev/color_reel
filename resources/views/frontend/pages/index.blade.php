@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel')
@section('content')
<div class="banner-slider-area text-white">
            <div id="banner-slider" class="owl-carousel banner-slider">
                 @forelse($banner as $row)
                <div class="banner-item banner-item-{{$loop->iteration}}" style="background-image:url('uploads/{{$row['banner_image']}}')">
                    <div class="overlay-70">
                        <a class="play-video" href="{{$row['url']}}"><i class="fa fa-play"></i></a>
                        <div class="banner-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 col-md-6">
                                        <h1 class="banner-title"><a href="javascript:void(0)">{{$row['heading']}}</a></h1>
                                       <!--  <p class="banner-date-meta">20 Jun, 2017</p> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 @empty
                    {{ config('global.nodata') }}                 
                 @endforelse
            </div>
        </div>
        <!-- Banne Slider End -->
        <div class="main-wrap">
            <div class="section pt-14 ">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-center">
                                <h3 class="sub-title primary-color"> Filmming Services</h3>
                                <h2 class="section-title mb-2">Subscribe Now</h2>
                                <p class="mb-4">Doloremque, ad. Vero veniam dicta aliquid blanditiis ullam quibusdam, accusamus, <br>eius quia molestias nihil temporibus nam praesentium odit repellat.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         @forelse($filmingservices as $row)
                        <div class="col-md-4"> 
                            <div class="icon-boxes icon-boxes-bg text-center">
                                <div class="icon-boxes-icon" data-bg-image="{{ asset('frontAssets/assets/img/icon_bg_1.png')}}" style="background-image: url('{{ asset('frontAssets/assets/img/icon_bg_1.png')}}');">
                                    <i class="fa fa-film" aria-hidden="true"></i>
                                </div>
                                <div class="icon-boxes-inner">
                                    <h5 class="icon-boxes-title">{{$row->title}}</h5>
                                    <div class="icon-boxes-content">
                                       {{strip_tags($row->description)}}
                                    </div>
                                    <div class="icon-boxes-link">
                                        <a href="javascript:void(0)"><span class="fa fa-arrow-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       @empty
                        {{ config('global.nodata') }}                 
                       @endforelse
                </div>
            </div>
        </div>
        <!-- Movie Section -->
        <div class="section section-padding movie-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-xs-6">
                        <div class="section-header">
                            <h2 class="section-title">Latest Movies</h2>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <a class="all-link" href="javascript:void(0)">See All Movies</a>
                    </div>
                </div>
                <div class="row">
                    <div class="owl-carousel video-carousel" id="video-carousel">
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/1.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">Wonder Women</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/2.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">Captain America</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/3.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">The City Truck</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/4.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">Dark Night 2</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/1.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">Wonder Women</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/2.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">Captain America</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/3.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">The City Truck</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                        <div class="video-item">
                            <div class="thumb-wrap">
                                <img src="{{ asset('frontAssets/images/movies/4.png')}}" alt="Movie Thumb">
                                <span class="rating">9.2</span>
                                <div class="thumb-hover">
                                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                                </div>
                            </div>
                            <div class="video-details">
                                <h4 class="video-title"><a href="movie-detail.html">Dark Night 2</a></h4>
                                <p class="video-release-on">22 Jun 2017</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Movie Section End -->
        <!-- Upcoming Movie Section -->
        <div class="section section-padding bg-image upcomming-section text-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="section-header">
                            <h2 class="section-title">Popular Movie</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="upcomming-featured">
                            <img class="img-responsive" src="{{ asset('frontAssets/images/movies/upcomming-featured.png')}}" alt="Upcomming Featured">
                            <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                            <div class="upcomming-details">
                                <h4 class="video-title"><a href="movie-detail.html">Fright Night (2017)</a></h4>
                                <p class="video-release-on">12 Jul 2017</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12 sm-top-30">
                        <div class="upcomming-item">
                            <img class="img-responsive" src="{{ asset('frontAssets/images/movies/upcomming-1.png')}}" alt="Upcomming featured">
                            <div class="upcomming-details">
                                <h4 class="video-title"><a href="movie-detail.html">Basketball</a></h4>
                                <p class="video-release-on">26 Jul 2017</p>
                            </div>
                            <div class="upcomming-hover">
                                <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                        <div class="upcomming-item">
                            <img class="img-responsive" src="{{ asset('frontAssets/images/movies/upcomming-2.png')}}" alt="Upcomming">
                            <div class="upcomming-details">
                                <h4 class="video-title"><a href="movie-detail.html">Expandable</a></h4>
                                <p class="video-release-on">27 Jul 2017</p>
                            </div>
                            <div class="upcomming-hover">
                                <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                        <div class="upcomming-item">
                            <img class="img-responsive" src="{{ asset('frontAssets/images/movies/upcomming-3.png')}}" alt="upcomming">
                            <div class="upcomming-details">
                                <h4 class="video-title"><a href="movie-detail.html">Devine Girl</a></h4>
                                <p class="video-release-on">28 Jul 2017</p>
                            </div>
                            <div class="upcomming-hover">
                                <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Upcoming Movie Section End -->
        <!-- TV Show Section -->
        <div class="section section-padding tvshow-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-xs-6">
                        <div class="section-header">
                            <h2 class="section-title">{{ config('global.event') }}</h2>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <a class="all-link" href="javascript:void(0)">{{ config('global.seeallevent') }}</a>
                    </div>
                </div>
                <div class="row">
                 
                    <div class="owl-carousel tvshow-carousel" id="tvshow-carousel">
                  @forelse($event as $row)
                        <div class="video-item">
                            <div class="eventBox">
                                <div class="eventImg"> 
                                    <a class="imageclasstag" href="{{URL::to('event-detail/'.$row->slug)}}"><img src="{{ asset('uploads/events/'.$row->image)}}" alt="image"></a>

                                    <div class="eventWish"><!-- event-detail -->
                                       <span>{{ '10'}}/{{ '100'}}</span>
                                    </div>
                                    <a href="javascript:void(0)" class="eventPrice">
                                            @if (1  == 2)
                                            <strong>{{ ucfirst($row->name) }}</strong>
                                            @else
                                            <strong>{{ config('global.free')}}</strong>
                                            @endif
                                    </a>
                                    <a href="javascript:void(0)" class="eventType">
                                        <span>{{ ucfirst($row->name) }}</span>
                                    </a>

                                </div> <div class="eventContent">
                                    <div class="d-flex">
                                        <div class="eventDate eventDate2">
                                            <strong>{{ getMonth($row->event_start_date) }}</strong>
                                            <span>to</span>
                                            <strong>{{ getMonth($row->event_end_date) }}</strong>
                                        </div>
                                        <div class="eventText">
                                            <h6 class="aaTooltip">
                                                <a href="{{URL::to('event-detail/'.$row->slug)}}" class="multiline-ellipsis" style="overflow: hidden; text-overflow: ellipsis; -webkit-box-orient: vertical; display: -webkit-box; -webkit-line-clamp: 1;">{{ ucfirst($row->name) }}</a>
                                                <span class="aaTooltiptext">{{ ucfirst($row->name) }}</span>
                                            </h6>
                                            <div class="d-flex align-items-baseline">
                                                <i class="fa fa-calendar"></i>
                                                <span><span>{{ DDMMYY($row->event_start_date) }}</span>&nbsp;-&nbsp;<span>{{ DDMMYY($row->event_end_date) }}</span></span>
                                            </div>
                                            <div class="d-flex align-items-baseline">
                                                <i class="fa fa-clock-o"></i>
                                                <span><span>{{ $row->event_start_time}}</span>&nbsp;-&nbsp;<span>{{ $row->event_end_time}}</span></span>
                                            </div>
                                            <div class="aaTooltip d-flex align-items-baseline">
                                                <i class="fa fa-map-marker"></i>
                                                <span class="multiline-ellipsis" style="overflow: hidden; text-overflow: ellipsis; -webkit-box-orient: vertical; display: -webkit-box; -webkit-line-clamp: 1;">Adliya, Manama, Bahrain</span>
                                                <span class="aaTooltiptext ">Adliya, Manama, Bahrain</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                        {{ config('global.nodata') }}              
                      
                       @endforelse
                       

                    </div>
                   
                </div>
            </div>
        </div>
        <!-- TV Show Section End -->
        <!-- News -->
        <div class="section section-padding top-padding-normal news-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="section-header">
                            <h2 class="section-title">{{ config('global.newsfeed') }}</h2>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <a class="all-link" href="javascript:void(0)">{{ config('global.seeallnews') }}</a>
                    </div>
                </div>
                <div class="row">
                     @forelse($bloglatest as $row)
                      <div class="col-md-6">
                        <a class="news-featured" href="{{URL::to('news-deatail/'.$row->blog_id)}}">
                            <img class="img-responsive" src="{{'uploads/'.$row->image}}" alt="Featured news thumb">
                            <span class="news-date-meta">{{DDMMYY($row->created_at)}}</span>
                            <div class="news-content">
                                <h4 class="news-title">{{$row->title}}</h4>
                            </div>
                        </a>
                    </div>
                       @empty
                        {{ config('global.nodata') }}                 
                       @endforelse
                   

                    <div class="col-md-6 sm-top-30">
                          @forelse($allbloglatest as $row)
                        <div class="news-item">
                            <a class="news-thumb" href="{{URL::to('news-deatail/'.$row->blog_id)}}">
                                <img src="{{'uploads/'.$row->image}}" alt="News thumb">
                            </a>
                            <div class="news-content">
                                <span class="news-date-meta">{{DDMMYY($row->created_at)}}</span>
                                <h4 class="news-title"><a href="{{URL::to('news-deatail/'.$row->blog_id)}}">{{$row->title}}</a></h4>
                                <p class="news-excerpt">{{strip_tags($row->description)}}</p>
                            </div>
                        </div>
                        @empty
                        {{ config('global.nodata') }}                 
                       @endforelse

                    </div>
                </div>
            </div>
        </div>
        <!-- News End -->
        <div class="liveAward">
            <div class="col-md-offset-2 col-md-8">
                <div class="upcomming-featured">
                    <div class="section-header">
                        <h2 class="section-title">{{ config('global.wtla') }}</h2>
                    </div>
                    <img class="img-responsive" src="{{ asset('frontAssets/assets/img/film_1.jpg')}}" alt="Upcomming Featured">
                    <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                </div>
            </div>
        </div>
        @endsection