@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel')
@section('content')

		<!-- Main Header End -->
		<!-- Page Header -->
		<div class="page-header">
			<div class="page-header-overlay">
				<div class="container">
					<h2 class="page-title">Event Detail</h2>
				</div>
			</div>
		</div>
		@if(!empty($event))
 		<!-- Page Header End -->
		<div class="main-wrap">
			<div class="section section-padding video-single-section">
				<div class="container">
					<div class="video-single">
						<div class="row">
							<div class="col-md-5">
								<div class="event-img">
									<img src="{{ asset('uploads/events/'.$event['image'])}}">
								</div>
							</div>
							<input type="hidden" name="event_id" id="event_id" class="event_id" value="{{$event['id']}}">
							<div class="col-md-7 event-detail-main">
								<h2 class="event-title">{{ucfirst($event['name'])}}</h2>
								<div class="event-info">
									<div class="date-info row">
										<div class="col-md-6 event-startdate">
											
											<div class="date-start">
												<span class="date-title">Start</span>
												<div class="date-value">{{getDateTime($event['event_start_date']) .', '.$event['event_start_time'] }}</div>
											</div>
											
										</div>
										<div class="col-md-6 event-startdate">
											
											<div class="date-start">
												<span class="date-title">End</span>
												<div class="date-value">{{getDateTime($event['event_end_date']) .','. $event['event_end_time'] }}</div>
											</div>
											
										</div>
									</div>
									<div class="row location-info">
										<div class="col-md-6">
											
											<div class="event-address">
												<span class="address-title">Address</span>
												<div class="address-value">
														{{$event['address']}}
													<a href="#" target="_blank" class="map-link" data-slimstat="5">View map <i class="fa fa-map-marker"></i></a>
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											
											<div class="event-phone">
												<span class="phone-title">Phone</span>
												<div class="phone-value">{{$event['mobile']}}</div>
											</div>
											
										</div>
									</div>
									
									<div class="row more-info">
										<div class="col-md-6">
											
											<div class="event-email">
												<span class="email-title">Email</span>
												<div class="email-value">
													<a href="mailto:{{$event['email']}}">{{$event['email']}}</a>
												</div>
											</div>
											
										</div>
										<div class="col-md-6">
											
											<div class="event-website">
												<span class="website-title">Website</span>
												<div class="website-value">
													<a href="{{$event['websiteurl']}}" target="_blank">{{$event['websiteurl']}}</a>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<div class="event-price">
									<p class="price getprice">
										<i class="fa fa-usd" aria-hidden="true"></i>
										   {{$event['event_variant'][0]->price}}
									</p>
									<p>{{strip_tags($event['description'])}}</p>
								</div>
								<div class="event-total">
									<p class="total getpricetotal">
										<span class="">Total : </span> 
										<i class="fa fa-usd" aria-hidden="true"></i> 
										{{$event['event_variant'][0]->price}}
									</p>
									<div class="ticket-price">
										<form>
											<div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
											<input type="number" id="number" value="0" />
											<div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
											<select name="variant_id" class="variant_id" id="variant_id">
												<option>Select Category</option>
												<!-- <?php //pr($event['event_variant']);?>-->
												@foreach($event['event_variant'] as $row)
												<option value="{{$row->id}}">{{$row->name}}</option>
												@endforeach
											</select>
											<button class="book-event">Book Event</button>
											<a class="book-event">Book Event</a>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="row status">
							<div class="col-md-7">
								<div class="event-status">
									<h3 class="status-title">Status</h3>
									<p id="demo"></p>
									<div class="tickets">
											<div class="row">										
												@forelse($event['fetch_event_detail_by_slug'] as $row)
												<div class="col-md-4 allcattickets">
													<div class="ticket-inner">
														<p><b>Gold {{$row['variant_id']}}</b></p>
														<p><i class="fa fa-usd" aria-hidden="true"></i> {{$row['price']}} </p>
														<p>{{$row['available']}}  Seats Available</p>
														<p>{{$row['sold']}}  Seats Sold</p>
													</div>
												</div>
												@empty
												<p class = "text-center">{{ config('global.nodata') }}><p>
											  @endforelse 
											</div>
										</div>
								
									<ul class="social-share">
										<li class="facebook"><a class=""  href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li class="twitter"><a class=""  href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li class="linkedin"><a class=""  href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
      								</ul>
								</div>
							</div>
							<div class="col-md-5">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.056887220717!2d77.3739389145595!3d28.628057091035387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce58785d5e2ff%3A0xd23c99820f0663ad!2sPan%20India%20Internet%20Private%20Limited!5e0!3m2!1sen!2sin!4v1566799993518!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
							</div>
						</div>
						<div class="events-tabs">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#home">Description</a></li>								
							</ul>
							<div class="tab-content">
								<div id="home" class="tab-pane fade in active">
									{{strip_tags($event['description'])}}
								</div>								
							</div>
						</div>
						 <div id="event-video-slider" class="owl-carousel banner-slider">
                <div class="banner-item banner-item-1">
                    <div class="overlay-70">
                        <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                   </div>
                </div>
                <div class="banner-item banner-item-2">
                    <div class="overlay-70">
                        <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>
                    </div>
                </div>
                <div class="banner-item banner-item-3">
                    <div class="overlay-70">
                        <a class="play-video" href="https://www.youtube.com/watch?v=5cY5PHE4x_g"><i class="fa fa-play"></i></a>                    
                    </div>
                </div>
            </div>
					</div>
				</div>
			</div>
			<!-- Latest Movies -->
			<div class="section section-padding top-padding-normal related-events">
				<div class="container">
					<div class="related-section">
						<div class="row">
						<div class="col-sm-8">
							<div class="section-header">
								<h2 class="section-title">Related Events</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="owl-carousel col-md-12" id="related-carousel">
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="{{ asset('frontAssets/images/events/related1.jpg')}}" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
									
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="{{ asset('frontAssets/images/events/related1.jpg')}}" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="{{ asset('frontAssets/images/events/related.jpg')}}" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="{{ asset('frontAssets/images/events/related1.jpg')}}" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							<div class="video-item">
								<div class="thumb-wrap">
									<img src="{{ asset('frontAssets/images/events/related2.jpg')}}" alt="Movie Thumb">
									<div class="related-date">
										<span class="day">13</span>
										<span class="month">Sep</span>
									</div>
									
								</div>
								<div class="video-details">
									<div class="related-meta">
										<span><i class="fa fa-calendar"></i>Sep 4, 2019</span>
										<span><i class="fa fa-shopping-basket"></i><span class="related-amount"><i class="fa fa-usd" aria-hidden="true"></i> 15.00</span></span>
										<span><i class="fa fa-ticket"></i> 44 Qty Available</span>
									</div>
									<div class="event-name"><h3>Event color</h3></div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- Latest Movies End -->
		</div>
		@else
      <h4 class = "card-title text-center">{{ config('global.nodata') }}</h4>
      @endif
		<!-- Footer Start -->
@endsection