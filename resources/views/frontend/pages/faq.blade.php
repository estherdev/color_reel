@extends('frontend.layout.mainlayout')
@section('title', 'ColorReel| Faq')
@section('content')

        <!-- Page Header -->
        <div class="page-header">
            <div class="page-header-overlay">
                <div class="container">
                    <h2 class="page-title">FAQ</h2>
                </div>
            </div>
        </div>
        <!-- Page Header End -->
        <div class="main-content">
            <div class="container">
                <div class="page-content faq">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        @foreach($faq as $key => $row)
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading{{$key}}">
                                <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">{{ucfirst($row->que)}}<i class="fa fa-plus"></i>
                                    <i class="fa fa-minus" style="display: none"></i>
                                </a>
                                </h4>
                            </div>
                            <div id="collapse{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$key}}">
                                <div class="panel-body">{{ucfirst($row->ans)}}</div>
                            </div>
                        </div>
                      
                      @endforeach 

                      
                    </div>
                </div>
            </div>
        </div>
     @endsection